package com.uthwork.letbereal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.UserRepository;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LetberealApplicationTests {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;


	@Test
	public void contextLoads() {
		assertNotNull(userRepository);
	}

	/*@Test
	public void saveUser(){
		User user=new User();
		user.setEmail("testMail@gmail.com");
		user.setFirstName("alex");
		user.setLastName("alex");
		user.setPassword(passwordEncoder.encode("login1-2"));
		user=userRepository.save(user);
		assertNotNull(user);
		assertNotNull(user.getUserId());
	}*/

	/*@Test
	public void findUser(){
		User user=userRepository.findByEmail("testMail@gmail.com");
		assertNotNull(user);
		assertNotNull(user.getUserId());
	}*/


}
