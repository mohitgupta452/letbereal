package com.uthwork.letbereal.post.domain.response;

import java.util.Date;

public class PostCommentResponseById {
	
	private String comment;
	private Date createdTime;
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

}
