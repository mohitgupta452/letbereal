package com.uthwork.letbereal.post.domain.response;

import java.util.ArrayList;
import java.util.List;

public class PostLoadResponseById {

	private Long userId;
	boolean isPostIdValid;
	private Long postId;
	private String imageUrl;
	private String description;
	private List<PostCommentResponseById> commentResponse= new ArrayList<PostCommentResponseById>();
	private Float avgRating;
	
	public Long getPostId() {
		return postId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public String getDescription() {
		return description;
	}
	public List<PostCommentResponseById> getCommentResponse() {
		return commentResponse;
	}
	public void setPostId(Long postId) {
		this.postId = postId;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setCommentResponse(List<PostCommentResponseById> commentResponse) {
		this.commentResponse = commentResponse;
	}
	public boolean isPostIdValid() {
		return isPostIdValid;
	}
	public void setPostIdValid(boolean isPostIdValid) {
		this.isPostIdValid = isPostIdValid;
	}

	public Float getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Float avgRating) {
		this.avgRating = avgRating;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
