package com.uthwork.letbereal.post.domain.response;

import java.util.ArrayList;
import java.util.List;

public class ProfileLoadResponse {

	boolean isProfileIdValid;
	private Long profileId;
	private String firstName;
	private String lastName;
	private String email;
	private List<ProfileLoadCommentResponse> commentResponse= new ArrayList<ProfileLoadCommentResponse>();
	
	
	
	
	public boolean isProfileIdValid() {
		return isProfileIdValid;
	}
	public void setProfileIdValid(boolean isProfileIdValid) {
		this.isProfileIdValid = isProfileIdValid;
	}
	public Long getProfileId() {
		return profileId;
	}
	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<ProfileLoadCommentResponse> getCommentResponse() {
		return commentResponse;
	}
	public void setCommentResponse(List<ProfileLoadCommentResponse> commentResponse) {
		this.commentResponse = commentResponse;
	}
	
	
}
