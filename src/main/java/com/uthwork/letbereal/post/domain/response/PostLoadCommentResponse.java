package com.uthwork.letbereal.post.domain.response;

import java.util.Date;

public class PostLoadCommentResponse {
	private String comment;
	private Date createdTime;
	private Long userId;
	private String userName;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getComment() {
		return comment;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
