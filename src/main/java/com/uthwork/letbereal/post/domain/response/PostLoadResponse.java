package com.uthwork.letbereal.post.domain.response;

import java.util.ArrayList;
import java.util.List;

import com.uthwork.letbereal.uma.domain.response.UserDetail;

public class PostLoadResponse {

	private Long userId;
	boolean isPostIdValid;
	private Long postId;
	private String imageUrl;
	private String description;
	
	private UserDetail userDetail;
	private List<PostLoadCommentResponse> commentResponse= new ArrayList<PostLoadCommentResponse>();
	private Float avgRating;
	
	public Long getPostId() {
		return postId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public String getDescription() {
		return description;
	}
	public List<PostLoadCommentResponse> getCommentResponse() {
		return commentResponse;
	}
	public void setPostId(Long postId) {
		this.postId = postId;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setCommentResponse(List<PostLoadCommentResponse> commentResponse) {
		this.commentResponse = commentResponse;
	}
	public boolean isPostIdValid() {
		return isPostIdValid;
	}
	public void setPostIdValid(boolean isPostIdValid) {
		this.isPostIdValid = isPostIdValid;
	}

	public Float getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Float avgRating) {
		this.avgRating = avgRating;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public UserDetail getUserDetail() {
		return userDetail;
	}
	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}
	
	
	
}
