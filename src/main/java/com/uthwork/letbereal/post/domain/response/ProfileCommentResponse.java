package com.uthwork.letbereal.post.domain.response;

public class ProfileCommentResponse {

	 boolean isprofileId;
	 boolean iscomment;
	 boolean iscommentVisibility;
	 boolean iscommentType;
	 private String permmisionError;
	 
    public boolean isIsprofileId() {
		return isprofileId;
	}
	public void setIsprofileId(boolean isprofileId) {
		this.isprofileId = isprofileId;
	}
	public boolean isIscomment() {
		return iscomment;
	}
	public void setIscomment(boolean iscomment) {
		this.iscomment = iscomment;
	}
	public boolean isIscommentVisibility() {
		return iscommentVisibility;
	}
	public void setIscommentVisibility(boolean iscommentVisibility) {
		this.iscommentVisibility = iscommentVisibility;
	}
	public boolean isIscommentType() {
		return iscommentType;
	}
	public void setIscommentType(boolean iscommentType) {
		this.iscommentType = iscommentType;
	}
	public String getPermmisionError() {
		return permmisionError;
	}
	public void setPermmisionError(String permmisionError) {
		this.permmisionError = permmisionError;
	}
		 
}
