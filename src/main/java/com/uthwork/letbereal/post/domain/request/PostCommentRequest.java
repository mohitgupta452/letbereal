package com.uthwork.letbereal.post.domain.request;


import org.hibernate.validator.constraints.NotEmpty;

import com.uthwork.letbereal.post.domain.PostCommentVisibilityType;

import javax.validation.constraints.NotNull;

public class PostCommentRequest {

	private long commentBy;
	@NotNull
	private long createdBy;
	@NotNull
	private long postId;
	@NotEmpty
	private String comment;
	@NotNull
	private PostCommentVisibilityType postCommentVisibilityType;
	/*@NotEmpty
	private String commentType;*/

	public long getPostId() {
		return postId;
	}
	public void setPostId(long postId) {
		this.postId = postId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public PostCommentVisibilityType getPostCommentVisibilityType() {
		return postCommentVisibilityType;
	}

	public void setPostCommentVisibilityType(PostCommentVisibilityType postCommentVisibilityType) {
		this.postCommentVisibilityType = postCommentVisibilityType;
	}
	
	public long getCommentBy() {
		return commentBy;
	}
	public void setCommentBy(long commentBy) {
		this.commentBy = commentBy;
	}
	public long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	 
	
}
