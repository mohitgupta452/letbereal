package com.uthwork.letbereal.post.domain.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class ProfileCommentRequest1 {
	@NotNull
	private long profileId;
	@NotBlank
	private String comment;
	@NotBlank
	private String commentType;
	@NotEmpty
	private CommentVisibility commentVisibility;
	public long getProfileId() {
		return profileId;
	}
	public void setProfileId(long profileId) {
		this.profileId = profileId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCommentType() {
		return commentType;
	}
	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}
	public CommentVisibility getCommentVisibility() {
		return commentVisibility;
	}
	public void setCommentVisibility(CommentVisibility commentVisibility) {
		this.commentVisibility = commentVisibility;
	}
	
	
}
