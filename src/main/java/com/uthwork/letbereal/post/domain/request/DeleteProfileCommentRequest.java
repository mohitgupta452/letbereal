package com.uthwork.letbereal.post.domain.request;

import javax.validation.constraints.NotNull;

public class DeleteProfileCommentRequest {


    @NotNull
    private Long profileId;
    private Long userId;

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
	
	
}
