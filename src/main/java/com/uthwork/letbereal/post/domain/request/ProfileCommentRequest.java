package com.uthwork.letbereal.post.domain.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class ProfileCommentRequest {
	
	private long commentBy;
	@NotNull
	private long profileId;
	@NotEmpty
	private String comment;
	@NotNull
	private CommentVisibility commentVisibility;
	@NotEmpty
	private String commentType;

	public CommentVisibility getCommentVisibility() {
		return commentVisibility;
	}
	public void setCommentVisibility(CommentVisibility commentVisibility) {
		this.commentVisibility = commentVisibility;
	}
	public long getProfileId() {
		return profileId;
	}
	public void setProfileId(long profileId) {
		this.profileId = profileId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getCommentType() {
		return commentType;
	}
	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}
	public long getCommentBy() {
		return commentBy;
	}
	public void setCommentBy(long commentBy) {
		this.commentBy = commentBy;
	}
	
}
