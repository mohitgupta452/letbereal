package com.uthwork.letbereal.post.domain.response;

public class PostResponse {

	boolean isPostValid;
	boolean imageUrl;
	boolean title;
	
	
	public boolean isImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(boolean imageUrl) {
		this.imageUrl = imageUrl;
	}
	public boolean isTitle() {
		return title;
	}
	public void setTitle(boolean title) {
		this.title = title;
	}

	public boolean isPostValid() {
		return isPostValid;
	}

	public void setPostValid(boolean postValid) {
		isPostValid = postValid;
	}
}
