package com.uthwork.letbereal.post.domain.request;

import org.hibernate.validator.constraints.NotEmpty;

import com.uthwork.letbereal.post.domain.PostVisibilityType;

import javax.validation.constraints.NotNull;

public class PostRequest {

	@NotNull
	private long postId;
	@NotNull
	private Long imageUrlId;
	private String description;
	@NotEmpty
	private String title;
	@NotNull
	private PostVisibilityType postVisibilityType;



	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public Long getImageUrlId() {
		return imageUrlId;
	}

	public void setImageUrlId(Long imageUrlId) {
		this.imageUrlId = imageUrlId;
	}

	public long getPostId() {
		return postId;
	}

	public void setPostId(long postId) {
		this.postId = postId;
	}

	public PostVisibilityType getPostVisibilityType() {
		return postVisibilityType;
	}

	public void setPostVisibilityType(PostVisibilityType postVisibilityType) {
		this.postVisibilityType = postVisibilityType;
	}
}
