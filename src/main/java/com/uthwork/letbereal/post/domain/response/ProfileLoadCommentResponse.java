package com.uthwork.letbereal.post.domain.response;

import java.util.Date;

public class ProfileLoadCommentResponse {

	private Long userId;
	private String comment;
	private Date createdTime;
	private String userName;
	private String date;
	private String time;
	
	public String getComment() {
		return comment;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
