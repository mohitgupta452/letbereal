package com.uthwork.letbereal.post.domain.response;

public class PostCommentResponse {

	 boolean  isCreatedBy;
	 boolean isPosstValid;
	 boolean iscomment;
	 boolean isPostCommentVisibilityType;
	 private String permmisionError;
	public boolean isIscomment() {
		return iscomment;
	}
	public void setIscomment(boolean iscomment) {
		this.iscomment = iscomment;
	}

	public boolean isPostCommentVisibilityType() {
		return isPostCommentVisibilityType;
	}

	public void setPostCommentVisibilityType(boolean postCommentVisibilityType) {
		isPostCommentVisibilityType = postCommentVisibilityType;
	}

	public boolean isPosstValid() {
		return isPosstValid;
	}

	public void setPosstValid(boolean posstValid) {
		isPosstValid = posstValid;
	}
	public boolean isCreatedBy() {
		return isCreatedBy;
	}
	public void setCreatedBy(boolean isCreatedBy) {
		this.isCreatedBy = isCreatedBy;
	}
	public String getPermmisionError() {
		return permmisionError;
	}
	public void setPermmisionError(String permmisionError) {
		this.permmisionError = permmisionError;
	}
		
}
