package com.uthwork.letbereal.post.service.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uthwork.letbereal.model.Post;
import com.uthwork.letbereal.model.PostComment;
import com.uthwork.letbereal.model.PostCommentId;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.FollowersRepository;
import com.uthwork.letbereal.persistence.repository.PostCommentRepository;
import com.uthwork.letbereal.persistence.repository.PostRatingRepository;
import com.uthwork.letbereal.persistence.repository.PostRepository;
import com.uthwork.letbereal.persistence.repository.UserRepository;
import com.uthwork.letbereal.post.domain.request.DeletePostCommentRequest;
import com.uthwork.letbereal.post.domain.request.PostCommentRequest;
import com.uthwork.letbereal.post.domain.request.PostRequest;
import com.uthwork.letbereal.post.domain.response.PostCommentResponseById;
import com.uthwork.letbereal.post.domain.response.PostLoadCommentResponse;
import com.uthwork.letbereal.post.domain.response.PostLoadResponse;
import com.uthwork.letbereal.post.domain.response.PostLoadResponseById;
import com.uthwork.letbereal.post.domain.response.PostResponse;
import com.uthwork.letbereal.post.domain.response.Response;
import com.uthwork.letbereal.post.service.PostCommentService;
import com.uthwork.letbereal.security.UserSession;
import com.uthwork.letbereal.uma.domain.response.UserDetail;
import com.uthwork.letbereal.utilities.AES;

import java.util.Collections;

@Service("postCommentService")
public class PostCommentServiceImpl implements PostCommentService {

	@Autowired
	PostCommentRepository postCommentRepository;

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private PostRatingRepository postRatingRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	FollowersRepository followersRepository;

	@Override
	public Post savePostDetails(PostRequest postRequest, Authentication principal) {
		User user= new User();
		UserSession us=(UserSession) principal.getPrincipal();
		user.setUserId(us.getProfileId());
		Post post=new Post();
		post.setUser(user);
		post.setImageUrlId(postRequest.getImageUrlId());
		post.setDescription(postRequest.getDescription());
		post.setPostVisibility(postRequest.getPostVisibilityType().name());
		post.setTitle(postRequest.getTitle());
		post.setCreatedTime(new Date());
		Post postResponse=postRepository.save(post);
		return postResponse;
	}

	@Override
	public PostComment savePostCommentDetails(PostCommentRequest postRequest) {
		 
		PostComment postComment=new PostComment();
		PostCommentId commentId=new PostCommentId();
		commentId.setPostId(postRequest.getPostId());
		commentId.setUserId(postRequest.getCommentBy());

		String privateKey=AES.generateString();

		postComment.setComment(AES.encrypt(postRequest.getComment(), privateKey));
		postComment.setField2(privateKey);	
		
//		postComment.setComment(postRequest.getComment());
		
		postComment.setCommentVisibility(postRequest.getPostCommentVisibilityType().name());
		postComment.setId(commentId);
		postComment.setCreatedTime(new Date());
		PostComment postResponse= postCommentRepository.save(postComment);

		return postResponse;
	}

	
	
	
	
	
	
	
	@Override
	public List<PostLoadResponse> getAllPostDetailsList(long userId) {
		//List<List<PostLoadResponse>> list= new ArrayList<>();
		List<PostLoadResponse> list= new ArrayList<>();
		List<PostLoadResponse> list1=null,list2=null,list3=null;
		//List<Post> postLoadList=postRepository.findAll();
		
		//pappu pandit
		
		List<Post> userPost=postRepository.findByUserPostId(userId);
		list1=getPost(userPost);
		
		for(int i=0;i<list1.size();i++)
		{
			list.add(list1.get(i));
		}
		
		List<User> userFollowersList = followersRepository.getUserFollowersListByUserID(userId);
		List<User> userFollowingList = followersRepository.getFollowingsList(userId);
		
		System.out.println("userFollowersList"+userFollowersList);
		System.out.println("userFollowingList"+userFollowingList);
		
		for(User userFollower:userFollowersList)
		{
			List<Post> postsOfFollower=postRepository.findByUserPostId(userFollower.getUserId());
			list2=getPost(postsOfFollower);
			for(int i=0;i<list2.size();i++)
			{
				list.add(list2.get(i));
			}
		}
		
		for(User userFollowing:userFollowingList)
		{
			List<Post> postsOfFollower=postRepository.findByUserPostId(userFollowing.getUserId());
			list3=getPost(postsOfFollower);
			for(int i=0;i<list3.size();i++)
			{
				list.add(list3.get(i));
			}
		}
	
		//pappu pandit
			
		
		/*for (Post post : postLoadList) {
			System.out.println("post== : "+post.getUser().getUserId());
			
			User user= userRepository.findOne(post.getUser().getUserId());
			
			UserDetail userDetail= new UserDetail();
			userDetail.setFirstName(user.getFirstName());
			userDetail.setUserId(user.getUserId());
			userDetail.setImageUrl(user.getImageUrl());
			
			PostLoadResponse postLoadResponse=new PostLoadResponse(); 
			postLoadResponse.setUserDetail(userDetail);
						
			postLoadResponse.setDescription(post.getDescription());
			postLoadResponse.setImageUrl(post.getImageUrl());
			postLoadResponse.setPostId(post.getPostId());
			List<PostLoadCommentResponse> listComment= new ArrayList<>();
			for(PostComment postComment:post.getPostComments()){
				PostLoadCommentResponse postLoadComment= new PostLoadCommentResponse();
				postLoadComment.setUserId(postComment.getUser().getUserId());
				
				
				System.out.println("postComment.getCommentType()"+postComment.getCommentType());
				if(postComment.getCommentType()!=null)
				{
					if(postComment.getCommentType().equals("Negative")){
						User commentByuser= userRepository.findOne(postComment.getUser().getUserId());
						String lastName=commentByuser.getLastName()!=null?commentByuser.getLastName():"";
							postLoadComment.setUserName(commentByuser.getFirstName()+" "+lastName);
					}
				}
				postLoadComment.setComment(postComment.getComment());
				postLoadComment.setCreatedTime(postComment.getCreatedTime());
				listComment.add(postLoadComment);
			}
			Float avgRating=postRatingRepository.findAvgPostRatingByPostId(post.getPostId());
			postLoadResponse.setAvgRating(avgRating);
			postLoadResponse.setCommentResponse(listComment);
			list.add(postLoadResponse);
		}*/
		return list;
	}

	
	@Override
	public List<PostLoadResponse> getPostDetailsList(Long user_id) {
		List<PostLoadResponse> list= new ArrayList<>();
		
		List<Post> postLoadList=postRepository.findByUserPostId(user_id);
		for (Post post : postLoadList) {
			PostLoadResponse postLoadResponse=new PostLoadResponse();
			postLoadResponse.setUserId(user_id);
			postLoadResponse.setDescription(post.getDescription());
			postLoadResponse.setImageUrl(post.getImageUrl());
			postLoadResponse.setPostId(post.getPostId());
			List<PostLoadCommentResponse> listComment= new ArrayList<>();
			for(PostComment postComment:post.getPostComments()){
				PostLoadCommentResponse postLoadComment= new PostLoadCommentResponse();
				postLoadComment.setUserId(postComment.getUser().getUserId());
				//postLoadComment.setComment(postComment.getComment());
				postLoadComment.setComment(AES.decrypt(postComment.getComment(), postComment.getField2()));
				postLoadComment.setCreatedTime(postComment.getCreatedTime());
				listComment.add(postLoadComment);
			}
			Float avgRating=postRatingRepository.findAvgPostRatingByPostId(post.getPostId());
			postLoadResponse.setAvgRating(avgRating);
			postLoadResponse.setCommentResponse(listComment);
			list.add(postLoadResponse);
		}
		return list;
	}

	@Override
	public PostLoadResponseById getPostDetailById(Long user_id,long postId) {

		Post post=postRepository.findOne(postId);
		PostLoadResponseById postLoadResponse=new PostLoadResponseById();
		postLoadResponse.setUserId(user_id);
		postLoadResponse.setDescription(post.getDescription());
		postLoadResponse.setImageUrl(post.getImageUrl());
		postLoadResponse.setPostId(post.getPostId());
		List<PostCommentResponseById> listComment= new ArrayList<>();
		for(PostComment postComment:post.getPostComments()){
			PostCommentResponseById postLoadCommentById= new PostCommentResponseById();
			
			postLoadCommentById.setComment(AES.decrypt(postComment.getComment(), postComment.getField2()));
			//postLoadCommentById.setComment(postComment.getComment());
			postLoadCommentById.setCreatedTime(postComment.getCreatedTime());
			listComment.add(postLoadCommentById);
		}
		Float avgRating=postRatingRepository.findAvgPostRatingByPostId(post.getPostId());
		postLoadResponse.setAvgRating(avgRating);
		postLoadResponse.setCommentResponse(listComment);
		return postLoadResponse;
	}

	@Override
	public PostComment updatePostCommentDetails(PostCommentRequest postRequest){

		PostCommentId commentId=new PostCommentId();
		commentId.setPostId(postRequest.getPostId());
		commentId.setUserId(postRequest.getCommentBy());

		PostComment existUser=postCommentRepository.findOne(commentId);
		//existUser.setComment(postRequest.getComment());

		String privateKey=AES.generateString();

		existUser.setComment(AES.encrypt(postRequest.getComment(), privateKey));
		existUser.setField2(privateKey);	
		existUser.setCommentVisibility(postRequest.getPostCommentVisibilityType().name());
		existUser.setId(commentId);
		existUser.setUpdateTime(new Date());
		PostComment postResponse= postCommentRepository.save(existUser);

		return postResponse;

	}

	@Override
	public Post updatePostDetails(PostRequest postRequest, Authentication principal){
		User user= new User();
		UserSession us=(UserSession) principal.getPrincipal();
		user.setUserId(us.getProfileId());
		Post post=new Post();
		post.setUser(user);
		Post existUser=postRepository.findOne(postRequest.getPostId());
		existUser.setImageUrlId(postRequest.getImageUrlId());
		existUser.setDescription(postRequest.getDescription());
		existUser.setTitle(postRequest.getTitle());
		existUser.setUpdateTime(new Date());
		existUser.setPostVisibility(postRequest.getPostVisibilityType().name());
		Post postResponse=postRepository.save(existUser);
		return postResponse;


	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public Response deletePostDetails(Long postId, Authentication principal){

		Response response= new Response();
		int reating=postRatingRepository.deletePostRatingByPostId(postId);
		int comment=postCommentRepository.deletePostCommentByPostId(postId);
		int post=postRepository.deletePostByPostId(postId);
		if(reating==0&&comment==0&&post==0){
			response.setValid(false);
		}
		else{
			response.setValid(true);
		}

		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public int deletePostComment(DeletePostCommentRequest commentRequest) {
		return postCommentRepository.deletePostComment(commentRequest.getPostId(),commentRequest.getUserId());
	}
	
	
	public List<PostLoadResponse> getPost(List<Post> postsOfFollower)
	{
		List<PostLoadResponse> list= new ArrayList<>();
		
		Collections.sort(postsOfFollower);
		
		for (Post post : postsOfFollower) {
			System.out.println("post== : "+post.getUser().getUserId());
			
			User user= userRepository.findOne(post.getUser().getUserId());
			
			UserDetail userDetail= new UserDetail();
			userDetail.setFirstName(user.getFirstName());
			userDetail.setUserId(user.getUserId());
			userDetail.setImageUrl(user.getImageUrl());
			
			PostLoadResponse postLoadResponse=new PostLoadResponse(); 
			postLoadResponse.setUserDetail(userDetail);
						
			postLoadResponse.setDescription(post.getDescription());
			postLoadResponse.setImageUrl(post.getImageUrl());
			postLoadResponse.setPostId(post.getPostId());
			List<PostLoadCommentResponse> listComment= new ArrayList<>();
			for(PostComment postComment:post.getPostComments()){
				PostLoadCommentResponse postLoadComment= new PostLoadCommentResponse();
				postLoadComment.setUserId(postComment.getUser().getUserId());
				
				
				System.out.println("postComment.getCommentType()"+postComment.getCommentType());
				if(postComment.getCommentType()!=null)
				{
					if(postComment.getCommentType().equals("Negative")){
						User commentByuser= userRepository.findOne(postComment.getUser().getUserId());
						String lastName=commentByuser.getLastName()!=null?commentByuser.getLastName():"";
							postLoadComment.setUserName(commentByuser.getFirstName()+" "+lastName);
					}
				}
				postLoadComment.setComment(postComment.getComment());
				postLoadComment.setCreatedTime(postComment.getCreatedTime());
				listComment.add(postLoadComment);
			}
			Float avgRating=postRatingRepository.findAvgPostRatingByPostId(post.getPostId());
			postLoadResponse.setAvgRating(avgRating);
			postLoadResponse.setCommentResponse(listComment);
			list.add(postLoadResponse);
		}
		return list;
	}
}
