package com.uthwork.letbereal.post.service;

import java.util.List;

import org.springframework.security.core.Authentication;

import com.uthwork.letbereal.model.ProfileComment;
import com.uthwork.letbereal.post.domain.request.DeleteProfileCommentRequest;
import com.uthwork.letbereal.post.domain.request.ProfileCommentRequest;
import com.uthwork.letbereal.post.domain.response.ProfileLoadResponse;

public interface ProfileCommentService {

	ProfileComment saveProfileCommentDetails(ProfileCommentRequest profileRequest);

	ProfileComment updateProfileCommentDetails(ProfileCommentRequest profileRequest);

	ProfileLoadResponse getListCommentsByUser(Authentication principal);

	ProfileLoadResponse getCommentByProfileId(Long profileId);

	int deleteProfileComment(DeleteProfileCommentRequest commentRequest);



}
