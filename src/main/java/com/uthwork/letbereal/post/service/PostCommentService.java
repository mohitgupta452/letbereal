package com.uthwork.letbereal.post.service;

import java.util.List;

import org.springframework.security.core.Authentication;

import com.uthwork.letbereal.model.Post;
import com.uthwork.letbereal.model.PostComment;
import com.uthwork.letbereal.post.domain.request.DeletePostCommentRequest;
import com.uthwork.letbereal.post.domain.request.PostCommentRequest;
import com.uthwork.letbereal.post.domain.request.PostRequest;
import com.uthwork.letbereal.post.domain.response.PostLoadResponse;
import com.uthwork.letbereal.post.domain.response.PostLoadResponseById;
import com.uthwork.letbereal.post.domain.response.PostResponse;
import com.uthwork.letbereal.post.domain.response.Response;

public interface PostCommentService {

	PostComment savePostCommentDetails(PostCommentRequest postRequest);
	public Post savePostDetails(PostRequest postRequest, Authentication principal);

    List<PostLoadResponse> getPostDetailsList(Long user_id);

    PostLoadResponseById getPostDetailById(Long user_id,long postId);

    PostComment updatePostCommentDetails(PostCommentRequest postRequest);
	Post updatePostDetails(PostRequest postRequest, Authentication principal);
	public Response deletePostDetails(Long postId, Authentication principal);
	
	int deletePostComment(DeletePostCommentRequest commentRequest);
	
	List<PostLoadResponse> getAllPostDetailsList(long user_id);
}
