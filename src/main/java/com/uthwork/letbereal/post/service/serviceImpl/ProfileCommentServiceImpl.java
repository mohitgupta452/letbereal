package com.uthwork.letbereal.post.service.serviceImpl;

import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uthwork.letbereal.model.BadComment;
import com.uthwork.letbereal.model.ProfileComment;
import com.uthwork.letbereal.model.ProfileCommentId;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.BadCommentRepository;
import com.uthwork.letbereal.persistence.repository.ProfileCommentRepository;
import com.uthwork.letbereal.persistence.repository.UserRepository;
import com.uthwork.letbereal.post.domain.request.DeleteProfileCommentRequest;
import com.uthwork.letbereal.post.domain.request.ProfileCommentRequest;
import com.uthwork.letbereal.post.domain.response.ProfileLoadCommentResponse;
import com.uthwork.letbereal.post.domain.response.ProfileLoadResponse;
import com.uthwork.letbereal.post.service.ProfileCommentService;
import com.uthwork.letbereal.security.UserSession;
import com.uthwork.letbereal.utilities.AES;

@Service("profileCommentService")
public class ProfileCommentServiceImpl implements ProfileCommentService {

	@Autowired
	ProfileCommentRepository profileCommentRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BadCommentRepository badCommentRepository;
	
	@Override
	public ProfileComment saveProfileCommentDetails(ProfileCommentRequest profileRequest) {
		ProfileComment profileComment=new ProfileComment();
		ProfileCommentId commentId=new ProfileCommentId();
		
		commentId.setProfileId(profileRequest.getProfileId());
		commentId.setUserId(profileRequest.getCommentBy());
		
		String privateKey=AES.generateString();
		
		profileComment.setComment(AES.encrypt(profileRequest.getComment(), privateKey));
		profileComment.setField2(privateKey);
		//profileComment.setComment(profileRequest.getComment());
		String type=this.commentStatus(profileRequest.getComment());
		
		profileComment.setCommentType(type);
		profileComment.setCommentVisibility(profileRequest.getCommentVisibility().name());
		profileComment.setId(commentId);
		profileComment.setCreatedTime(new Date());
		
		ProfileComment profileResponse= profileCommentRepository.save(profileComment);
		
		return profileResponse;
	}

	@Override
	public ProfileComment updateProfileCommentDetails(ProfileCommentRequest profileRequest) {
		
		ProfileCommentId commentId=new ProfileCommentId();
		commentId.setProfileId(profileRequest.getProfileId());
		commentId.setUserId(profileRequest.getCommentBy());
		ProfileComment existUser=profileCommentRepository.findOne(commentId);
		
		String privateKey=AES.generateString();

		existUser.setComment(AES.encrypt(profileRequest.getComment(), privateKey));
		existUser.setField2(privateKey);
		//existUser.setComment(profileRequest.getComment());
		
		String type=this.commentStatus(profileRequest.getComment());
		
		existUser.setCommentType(type);
		existUser.setCommentVisibility(profileRequest.getCommentVisibility().name());
		existUser.setId(commentId);
		existUser.setUpdateTime(new Date());
		ProfileComment profileResponse= profileCommentRepository.save(existUser);

		return profileResponse;
	}

	private String commentStatus(String comment) {
		
		List<BadComment> badComments=badCommentRepository.findAll();
		
		for(BadComment badComment:badComments){
			if(comment.contains(badComment.getComment())){
				return "Negative";
			}
		}
		return "Positive";
	}

	@Override
	public ProfileLoadResponse getListCommentsByUser(Authentication principal) {
		
		ProfileLoadResponse profileLoadResponse=new ProfileLoadResponse();
		UserSession us=(UserSession) principal.getPrincipal();
		Long user_id=us.getProfileId();
		User user=(User) userRepository.findOne(user_id);
		profileLoadResponse.setFirstName(user.getFirstName());
		profileLoadResponse.setLastName(user.getLastName());
		profileLoadResponse.setProfileId(user.getUserId());
		profileLoadResponse.setEmail(user.getEmail());
		List<ProfileLoadCommentResponse> listComment= new ArrayList<>();
		for(ProfileComment profileComment:user.getProfileCommentsForUserId()){
			ProfileLoadCommentResponse profileLoadComment= new ProfileLoadCommentResponse();
			profileLoadComment.setUserId(profileComment.getUserByProfileId().getUserId());
			profileLoadComment.setComment(AES.decrypt(profileComment.getComment(), profileComment.getField2()));
			//profileLoadComment.setComment(profileComment.getComment());
			profileLoadComment.setCreatedTime(profileComment.getCreatedTime());
			listComment.add(profileLoadComment);
		}
		profileLoadResponse.setCommentResponse(listComment);
		profileLoadResponse.setProfileIdValid(true);
		return profileLoadResponse;
		
	}
		
		
	

	@Override
	public ProfileLoadResponse getCommentByProfileId(Long profileId) {
		User user=userRepository.findOne(profileId);
		ProfileLoadResponse profileLoadResponse=new ProfileLoadResponse();
		profileLoadResponse.setFirstName(user.getFirstName());
		profileLoadResponse.setLastName(user.getLastName());
		profileLoadResponse.setProfileId(user.getUserId());
		profileLoadResponse.setEmail(user.getEmail());
		List<ProfileLoadCommentResponse> listComment= new ArrayList<>();
		for(ProfileComment profileComment:user.getProfileCommentsForProfileId_1()){
			ProfileLoadCommentResponse profileLoadComment= new ProfileLoadCommentResponse();
			profileLoadComment.setUserId(profileComment.getUserByUserId().getUserId());
			if(profileComment.getCommentType().equals("Negative")){
				User commentByuser= userRepository.findOne(profileComment.getUserByUserId().getUserId());
				String lastName=commentByuser.getLastName()!=null?commentByuser.getLastName():"";
				profileLoadComment.setUserName(commentByuser.getFirstName()+" "+lastName);
			}
			profileLoadComment.setComment(AES.decrypt(profileComment.getComment(), profileComment.getField2()));
			//profileLoadComment.setComment(profileComment.getComment());
			profileLoadComment.setCreatedTime(profileComment.getCreatedTime());
			listComment.add(profileLoadComment);
		}
		profileLoadResponse.setCommentResponse(listComment);
		return profileLoadResponse;
	}

	@Override
	@Transactional
	public int deleteProfileComment(DeleteProfileCommentRequest commentRequest) {
		
		return profileCommentRepository.deleteProfileComment(commentRequest.getProfileId(),commentRequest.getUserId());
	}

}
