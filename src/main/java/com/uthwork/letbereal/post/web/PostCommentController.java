package com.uthwork.letbereal.post.web;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uthwork.letbereal.model.Post;
import com.uthwork.letbereal.model.PostComment;
import com.uthwork.letbereal.post.domain.request.DeletePostCommentRequest;
import com.uthwork.letbereal.post.domain.request.PostCommentRequest;
import com.uthwork.letbereal.post.domain.request.PostRequest;
import com.uthwork.letbereal.post.domain.response.*;
import com.uthwork.letbereal.post.service.PostCommentService;
import com.uthwork.letbereal.security.UserSession;

@RestController
@RequestMapping("/letbereal")
public class PostCommentController {

	 @Autowired
	 PostCommentService postCommentService;
	 
	 /* saveUserPost: This method is used to save new Post. */
	 
	 @CrossOrigin(origins="*")
	 @RequestMapping(value="/saveUserPost", method=RequestMethod.POST)
		public @ResponseBody  PostResponse  savePostDetails(HttpServletResponse httpResponse,@RequestBody @Valid PostRequest postRequest,Authentication principal,BindingResult result) throws UnsupportedEncodingException, JsonProcessingException {

	    	PostResponse postResponse= new PostResponse();
	    	 if(result.hasErrors()){
	    		postResponse= validatePostRequest(postResponse,result);
	             return postResponse;
	         }
	    	 else{
	    		 postResponse.setImageUrl(true);
	    		 postResponse.setTitle(true);
	    		 postResponse.setPostValid(true);
	    		 Post  postData= postCommentService.savePostDetails(postRequest,principal);
			 
	    	 }
			 return postResponse;
		}
	    
	 
	 /* validatePostRequest: This method is used validate Post Request . */
	 
	    private PostResponse validatePostRequest(PostResponse postResponse, BindingResult result) {
	    	
	    	if(result.hasFieldErrors("imageUrl")){
	    		postResponse.setImageUrl(false);
	        } else{
	        	postResponse.setImageUrl(true);
	        }
	     if(result.hasFieldErrors("title")){
	    	 postResponse.setTitle(false);
	        } else{
	        	postResponse.setTitle(true);
	        }
	     
	    	return postResponse;
	    }
	    
	    /* savePostCommentDetails: This method is used save comment on Post. */
	    
	    @CrossOrigin(origins="*")
	    @RequestMapping(value="/savePostCommentDetails", method=RequestMethod.POST)
		public @ResponseBody  PostCommentResponse  savePostCommentDetails(HttpServletResponse httpResponse,@RequestBody @Valid PostCommentRequest postRequest,Authentication principal,BindingResult result) throws UnsupportedEncodingException, JsonProcessingException 
		{
	    	UserSession us=(UserSession) principal.getPrincipal();
			Long user_id=us.getProfileId();
			 postRequest.setCommentBy(user_id);
	    	PostCommentResponse postCommentResponse=new PostCommentResponse();
		if (postRequest.getCreatedBy() == postRequest.getCommentBy()) {
			postCommentResponse.setPermmisionError("Sorry you can not comment on your post.");
			return postCommentResponse;
		} else { 
			postCommentResponse = this.validateCommentRequest(postCommentResponse, result);
			if (result.hasErrors()) {
				
				return postCommentResponse;
			} else {

				PostComment postResponse = postCommentService.savePostCommentDetails(postRequest);
			}
		}
	    	return postCommentResponse;
		}
	    
	    /* validateCommentRequest: This method is used validate Post Comment Request . */
	    
	    private PostCommentResponse validateCommentRequest(PostCommentResponse postResponse, BindingResult result) {
	    	
	    	if(result.hasFieldErrors("comment")){
	    		postResponse.setIscomment(false);
	        } else{
	        	postResponse.setIscomment(true);
	        }
	     if(result.hasFieldErrors("postCommentVisibilityType")){
	    	 postResponse.setPostCommentVisibilityType(false);;
	        } else{
	        	postResponse.setPostCommentVisibilityType(true);
	        }
	        if(result.hasFieldErrors("postId")){
				postResponse.setPosstValid(false);
			} else{
				postResponse.setPosstValid(true);
			}
	        if(result.hasFieldErrors("createdBy")){
				postResponse.setCreatedBy(false);
			} else{
				postResponse.setCreatedBy(true);
			}
	        
	    	return postResponse;
	    
	    }

	    /* getPostDetailsList: This method is used to get All Posts List . */
	    
	    @CrossOrigin(origins="*")
		@RequestMapping(value = "getAllPostDetailsList", method = RequestMethod.GET)
		public @ResponseBody  List<PostLoadResponse>  getAllPostDetailsList(Authentication principal) {	
			
			UserSession us=(UserSession) principal.getPrincipal();
			long user_id=us.getProfileId();
			
			List<PostLoadResponse> postResponseList= postCommentService.getAllPostDetailsList(user_id);
			return postResponseList;
		}
	    
	    /* getPostDetailsList: This method is used to get All Posts List . */
	
	@CrossOrigin(origins="*")    
	@RequestMapping(value = "getUserPostDetailsList", method = RequestMethod.GET)
	public @ResponseBody  List<PostLoadResponse>  getPostDetailsList(Authentication principal) {
		UserSession us=(UserSession) principal.getPrincipal();
		Long user_id=us.getProfileId();
		
		List<PostLoadResponse> postResponseList= postCommentService.getPostDetailsList(user_id);
		return postResponseList;
	}

	 /* getPostDetailById: This method is used to get the complete Description of particular Post . */
    
	@CrossOrigin(origins="*")
	@RequestMapping(value = "getPostDetailById/{postId}", method = RequestMethod.GET)
	public @ResponseBody  PostLoadResponseById  getPostDetailById(@PathVariable("postId") Long postId,Authentication principal) {
		
		PostLoadResponseById postResponse=new PostLoadResponseById();

		UserSession us=(UserSession) principal.getPrincipal();
		Long user_id=us.getProfileId();
		
		if(postId==null){
			postResponse.setPostIdValid(false);
			return postResponse;
		}
		postResponse= postCommentService.getPostDetailById(user_id ,postId);
		postResponse.setPostIdValid(true);
		return postResponse;
	}

	/* updatePostCommentDetails: This method is used to update the comment on this post  . */
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/updatePostCommentDetails", method=RequestMethod.POST)
	public @ResponseBody  PostCommentResponse  updatePostCommentDetails(HttpServletResponse httpResponse, @Valid @RequestBody PostCommentRequest postRequest,Authentication principal,BindingResult result) throws UnsupportedEncodingException, JsonProcessingException
	{

		UserSession us=(UserSession) principal.getPrincipal();
		Long user_id=us.getProfileId();
		 postRequest.setCommentBy(user_id);
    	PostCommentResponse postCommentResponse=new PostCommentResponse();
	if (postRequest.getCreatedBy() == postRequest.getCommentBy()) {
		postCommentResponse.setPermmisionError("Sorry you can not comment on your post.");
		return postCommentResponse;
	} else {
		if (result.hasErrors()) {
			postCommentResponse = this.validateCommentRequest(postCommentResponse, result);
			return postCommentResponse;
		} else {

			PostComment postResponse = postCommentService.updatePostCommentDetails(postRequest);
		}
	}

		return postCommentResponse;
	}
	
	/* updatePostDetails: This method is used to update old post with new words . */
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/updatePostDetails", method=RequestMethod.POST)
	public @ResponseBody  PostResponse  updatePostDetails(@RequestBody @Valid PostRequest postRequest ,Authentication principal,BindingResult result) throws UnsupportedEncodingException, JsonProcessingException
	{

		PostResponse updatePostResponse= new PostResponse();

		 updatePostResponse=this.validatePostRequest(updatePostResponse, result);

		if(result.hasErrors()){
			return updatePostResponse;
		}else{
			updatePostResponse.setImageUrl(true);
			updatePostResponse.setTitle(true);
			updatePostResponse.setPostValid(true);
			postCommentService.updatePostDetails(postRequest,principal);
		}
		return updatePostResponse;
	}

	/* deletePostComment: This method is used to delete comment on particular post. */
	@CrossOrigin(origins="*")
	 @PostMapping("/deletePostComment")
	    public DeletePostCommentResponse deletePostComment(Authentication authentication, @Valid @RequestBody DeletePostCommentRequest commentRequest, BindingResult result){
	        DeletePostCommentResponse response=new DeletePostCommentResponse();
	        if(result.hasFieldErrors("postId")){
	            response.setPostIdValid(false);
	            return response;
	        }
	        response.setPostIdValid(true);
	        UserSession userSession = (UserSession) authentication.getPrincipal();
	        commentRequest.setUserId(userSession.getProfileId());
	        int postComment=postCommentService.deletePostComment(commentRequest);
	        if(postComment==0){
	            response.setPostIdValid(false);
	            return response;
	        }
	        return response;
	    }

	 /* deletePostDetails: This method is used to delete post. */
	 
	@CrossOrigin(origins="*")
	@RequestMapping(value="/deletePostDetails/{postId}", method=RequestMethod.GET)
	public @ResponseBody
	Response deletePostDetails(@RequestBody  @PathVariable Long postId , Authentication principal) throws UnsupportedEncodingException, JsonProcessingException
	{
		return postCommentService.deletePostDetails(postId,principal);
	}

}
