package com.uthwork.letbereal.post.web;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uthwork.letbereal.model.PostComment;
import com.uthwork.letbereal.model.ProfileComment;
import com.uthwork.letbereal.post.domain.request.DeleteProfileCommentRequest;
import com.uthwork.letbereal.post.domain.request.ProfileCommentRequest;
import com.uthwork.letbereal.post.domain.response.DeleteProfileCommentResponse;
import com.uthwork.letbereal.post.domain.response.ProfileCommentResponse;
import com.uthwork.letbereal.post.domain.response.ProfileLoadResponse;
import com.uthwork.letbereal.post.service.ProfileCommentService;
import com.uthwork.letbereal.security.UserSession;

@RestController
@RequestMapping("/letbereal")
public class ProfileCommentController {

	
	@Autowired
	 ProfileCommentService profileCommentService;
	
	/* saveProfileCommentDetails: This method is used to save User profile comments details.  */
	
	@CrossOrigin(origins="*")
	@RequestMapping(value = "/saveProfileCommentDetails", method = RequestMethod.POST)
	public @ResponseBody ProfileCommentResponse saveProfileCommentDetails(HttpServletResponse httpResponse,
			@RequestBody @Valid ProfileCommentRequest profileRequest, Authentication principal, BindingResult result)
			throws UnsupportedEncodingException, JsonProcessingException {

		UserSession us = (UserSession) principal.getPrincipal();
		Long user_id = us.getProfileId();
		profileRequest.setCommentBy(user_id);
		ProfileCommentResponse profileCommentResponse = new ProfileCommentResponse();
		this.setTrueInProfileCommentResponse(profileCommentResponse);
		if (profileRequest.getProfileId() == profileRequest.getCommentBy()) {
			profileCommentResponse.setPermmisionError("Sorry you can not comment yourself.");
			return profileCommentResponse;
		} else {
			profileCommentResponse = this.validateCommentRequest(profileCommentResponse, result);
			if (result.hasErrors()) {
				return profileCommentResponse;
			} else {
				profileCommentService.saveProfileCommentDetails(profileRequest);

			}
		}

		return profileCommentResponse;

	}


	private void setTrueInProfileCommentResponse(ProfileCommentResponse profileCommentResponse) {
		profileCommentResponse.setIscomment(true);
		profileCommentResponse.setIscommentVisibility(true);
		profileCommentResponse.setIscommentType(true);
		profileCommentResponse.setIsprofileId(true);
		profileCommentResponse.setPermmisionError("");
	}


	private ProfileCommentResponse validateCommentRequest(ProfileCommentResponse profileCommentResponse,
			Errors result) {
		
		if (result.hasFieldErrors("comment")) {
			profileCommentResponse.setIscomment(false);
		}
		else if (result.hasFieldErrors("commentVisibility")) {
			profileCommentResponse.setIscommentVisibility(false);
		}
		else if (result.hasFieldErrors("commentType")) {
			profileCommentResponse.setIscommentType(false);
		}
		else if (result.hasFieldErrors("profileId")) {
			profileCommentResponse.setIsprofileId(false);
		}
		return profileCommentResponse;
	}
	
	/* /updateProfileCommentDetails: This method is used to update User profile comments details.  */
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/updateProfileCommentDetails", method=RequestMethod.POST)
	public @ResponseBody  ProfileCommentResponse  updatePostCommentDetails(HttpServletResponse httpResponse,@RequestBody @Valid ProfileCommentRequest profileRequest,Authentication principal,Errors result) throws MethodArgumentNotValidException, UnsupportedEncodingException, JsonProcessingException
	{
		UserSession us = (UserSession) principal.getPrincipal();
		Long user_id = us.getProfileId();
		profileRequest.setCommentBy(user_id);
		ProfileCommentResponse profileCommentResponse = new ProfileCommentResponse();
		
		setTrueInProfileCommentResponse(profileCommentResponse);

		if (profileRequest.getProfileId() == profileRequest.getCommentBy()) {
			profileCommentResponse.setPermmisionError("Sorry you can not comment yourself.");
			return profileCommentResponse;
		} else {
			
			if (result.hasErrors()) {

				profileCommentResponse = validateCommentRequest(profileCommentResponse, result);
				return profileCommentResponse;
			} else {
				profileCommentService.updateProfileCommentDetails(profileRequest);

			}
		}

		return profileCommentResponse;
	}
	
	/* getCommentDetailsList: This method is used to get complete Comment list on self profile. */
	
	@CrossOrigin(origins="*")
	@RequestMapping(value = "/getCommentDetailsList", method = RequestMethod.GET)
	public @ResponseBody ProfileLoadResponse getProfileDetailsList(Authentication principal) {
		UserSession us=(UserSession) principal.getPrincipal();
		Long user_id=us.getProfileId();
		ProfileLoadResponse profileResponseList= this.getPostDetailById(user_id);
		return profileResponseList;
	}
	
	/* getCommentsByProfileId/{profileId}: This method is used to get complete Comment list by Id. */
	
	@CrossOrigin(origins="*")
	@RequestMapping(value = "getCommentsByProfileId/{profileId}", method = RequestMethod.GET)
	public @ResponseBody  ProfileLoadResponse  getPostDetailById(@PathVariable("profileId") Long profileId) {
		ProfileLoadResponse profileLoadResponse=new ProfileLoadResponse();
		if(profileId==null){
			profileLoadResponse.setProfileIdValid(false);
			return profileLoadResponse;
		}
		profileLoadResponse= profileCommentService.getCommentByProfileId(profileId);
		profileLoadResponse.setProfileIdValid(true);
		return profileLoadResponse;
	}
	
	/* getListCommentsByUser: This method is used to get list of comments commented by the user. */
	
	@CrossOrigin(origins="*")
	@RequestMapping(value = "/getListCommentsByUser", method = RequestMethod.GET)
	public @ResponseBody ProfileLoadResponse getListCommentsByUser(Authentication principal) {
		
		ProfileLoadResponse profileResponseList= profileCommentService.getListCommentsByUser(principal);
		return profileResponseList;
	}
	
	/* deleteProfileComment: This method is used to delete comments on profile . */
	
		@CrossOrigin(origins="*")
	    @PostMapping("/deleteProfileComment")
	    public DeleteProfileCommentResponse deleteProfileComment(Authentication authentication, @Valid @RequestBody DeleteProfileCommentRequest commentRequest, BindingResult result){

	        DeleteProfileCommentResponse response=new DeleteProfileCommentResponse();
	        if(result.hasFieldErrors("profileId")){
	            response.setProfileIdValid(false);
	            return response;
	        }
	        response.setProfileIdValid(true);

	        UserSession userSession = (UserSession) authentication.getPrincipal();
	        commentRequest.setUserId(userSession.getProfileId());
	        int profileComment=profileCommentService.deleteProfileComment(commentRequest);
	        if(profileComment==0){
	            response.setProfileIdValid(false);
	            return response;
	        }
	        return response;
	    }
	
}
