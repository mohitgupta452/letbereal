package com.uthwork.letbereal.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Company generated by hbm2java
 */
@Entity
@Table(name = "company", catalog = "letbereal", uniqueConstraints = @UniqueConstraint(columnNames = "company_email_id"))
public class Company implements java.io.Serializable {

	private Long companyId;
	private User user;
	private String companyName;
	private String companySalg;
	private String ownerName;
	private Date companyStartDate;
	private Integer noOfEmployees;
	private String companyContactNo;
	private String companyEmailId;
	private String companyImageUrl;
	private String companyCoverUrl;
	private Integer field1;
	private String field2;
	private Date field3;
	private Set<CompanyReview> companyReviews = new HashSet<CompanyReview>(0);
	private CompanyRating companyRating;
	private Set<CompanyPostRating> companyPostRatings = new HashSet<CompanyPostRating>(0);
	private Set<CompanyPostComment> companyPostComments = new HashSet<CompanyPostComment>(0);
	private Set<CompanyPost> companyPosts = new HashSet<CompanyPost>(0);

	public Company() {
	}

	public Company(User user, Date companyStartDate, String companyEmailId) {
		this.user = user;
		this.companyStartDate = companyStartDate;
		this.companyEmailId = companyEmailId;
	}

	public Company(User user, String companyName, String companySalg, String ownerName, Date companyStartDate,
			Integer noOfEmployees, String companyContactNo, String companyEmailId, String companyImageUrl,
			String companyCoverUrl, Integer field1, String field2, Date field3, Set<CompanyReview> companyReviews,
			CompanyRating companyRating, Set<CompanyPostRating> companyPostRatings,
			Set<CompanyPostComment> companyPostComments, Set<CompanyPost> companyPosts) {
		this.user = user;
		this.companyName = companyName;
		this.companySalg = companySalg;
		this.ownerName = ownerName;
		this.companyStartDate = companyStartDate;
		this.noOfEmployees = noOfEmployees;
		this.companyContactNo = companyContactNo;
		this.companyEmailId = companyEmailId;
		this.companyImageUrl = companyImageUrl;
		this.companyCoverUrl = companyCoverUrl;
		this.field1 = field1;
		this.field2 = field2;
		this.field3 = field3;
		this.companyReviews = companyReviews;
		this.companyRating = companyRating;
		this.companyPostRatings = companyPostRatings;
		this.companyPostComments = companyPostComments;
		this.companyPosts = companyPosts;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "company_id", unique = true, nullable = false)
	public Long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "company_name", length = 250, unique=true,nullable = false)
	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "company_salg", length = 100)
	public String getCompanySalg() {
		return this.companySalg;
	}

	public void setCompanySalg(String companySalg) {
		this.companySalg = companySalg;
	}

	@Column(name = "owner_name", length = 250)
	public String getOwnerName() {
		return this.ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "company_start_date", nullable = false, length = 19)
	public Date getCompanyStartDate() {
		return this.companyStartDate;
	}

	public void setCompanyStartDate(Date companyStartDate) {
		this.companyStartDate = companyStartDate;
	}

	@Column(name = "no_of_employees")
	public Integer getNoOfEmployees() {
		return this.noOfEmployees;
	}

	public void setNoOfEmployees(Integer noOfEmployees) {
		this.noOfEmployees = noOfEmployees;
	}

	@Column(name = "company_contact_no", length = 50)
	public String getCompanyContactNo() {
		return this.companyContactNo;
	}

	public void setCompanyContactNo(String companyContactNo) {
		this.companyContactNo = companyContactNo;
	}

	@Column(name = "company_email_id", unique = true, nullable = false, length = 50)
	public String getCompanyEmailId() {
		return this.companyEmailId;
	}

	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}

	@Column(name = "company_image_url", length = 500)
	public String getCompanyImageUrl() {
		return this.companyImageUrl;
	}

	public void setCompanyImageUrl(String companyImageUrl) {
		this.companyImageUrl = companyImageUrl;
	}

	@Column(name = "company_cover_url", length = 500)
	public String getCompanyCoverUrl() {
		return this.companyCoverUrl;
	}

	public void setCompanyCoverUrl(String companyCoverUrl) {
		this.companyCoverUrl = companyCoverUrl;
	}

	@Column(name = "field1")
	public Integer getField1() {
		return this.field1;
	}

	public void setField1(Integer field1) {
		this.field1 = field1;
	}

	@Column(name = "field2", length = 256)
	public String getField2() {
		return this.field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "field3", length = 19)
	public Date getField3() {
		return this.field3;
	}

	public void setField3(Date field3) {
		this.field3 = field3;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	public Set<CompanyReview> getCompanyReviews() {
		return this.companyReviews;
	}

	public void setCompanyReviews(Set<CompanyReview> companyReviews) {
		this.companyReviews = companyReviews;
	}

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "company")
	public CompanyRating getCompanyRating() {
		return this.companyRating;
	}

	public void setCompanyRating(CompanyRating companyRating) {
		this.companyRating = companyRating;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	public Set<CompanyPostRating> getCompanyPostRatings() {
		return this.companyPostRatings;
	}

	public void setCompanyPostRatings(Set<CompanyPostRating> companyPostRatings) {
		this.companyPostRatings = companyPostRatings;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	public Set<CompanyPostComment> getCompanyPostComments() {
		return this.companyPostComments;
	}

	public void setCompanyPostComments(Set<CompanyPostComment> companyPostComments) {
		this.companyPostComments = companyPostComments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	public Set<CompanyPost> getCompanyPosts() {
		return this.companyPosts;
	}

	public void setCompanyPosts(Set<CompanyPost> companyPosts) {
		this.companyPosts = companyPosts;
	}

}
