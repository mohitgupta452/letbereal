package com.uthwork.letbereal.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * CompanyPostRating generated by hbm2java
 */
@Entity
@Table(name = "company_post_rating", catalog = "letbereal")
public class CompanyPostRating implements java.io.Serializable {

	private long postId;
	private Company company;
	private CompanyPost companyPost;
	private User user;
	private Integer rating;
	private Date createdTime;
	private Date updatedTime;
	private Integer field1;
	private String field2;
	private Date field3;

	public CompanyPostRating() {
	}

	public CompanyPostRating(Company company, CompanyPost companyPost, User user, Date updatedTime) {
		this.company = company;
		this.companyPost = companyPost;
		this.user = user;
		this.updatedTime = updatedTime;
	}

	public CompanyPostRating(Company company, CompanyPost companyPost, User user, Integer rating, Date createdTime,
			Date updatedTime, Integer field1, String field2, Date field3) {
		this.company = company;
		this.companyPost = companyPost;
		this.user = user;
		this.rating = rating;
		this.createdTime = createdTime;
		this.updatedTime = updatedTime;
		this.field1 = field1;
		this.field2 = field2;
		this.field3 = field3;
	}

	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "companyPost"))
	@Id
	@GeneratedValue(generator = "generator")

	@Column(name = "post_id", unique = true, nullable = false)
	public long getPostId() {
		return this.postId;
	}

	public void setPostId(long postId) {
		this.postId = postId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id", nullable = false)
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	public CompanyPost getCompanyPost() {
		return this.companyPost;
	}

	public void setCompanyPost(CompanyPost companyPost) {
		this.companyPost = companyPost;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "rating")
	public Integer getRating() {
		return this.rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_time", length = 19)
	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_time", nullable = false, length = 19)
	public Date getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	@Column(name = "field1")
	public Integer getField1() {
		return this.field1;
	}

	public void setField1(Integer field1) {
		this.field1 = field1;
	}

	@Column(name = "field2", length = 256)
	public String getField2() {
		return this.field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "field3", length = 19)
	public Date getField3() {
		return this.field3;
	}

	public void setField3(Date field3) {
		this.field3 = field3;
	}

}
