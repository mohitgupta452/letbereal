package com.uthwork.letbereal.model;
// Generated Jan 20, 2018 11:34:25 AM by Hibernate Tools 5.1.0.Alpha1

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * PostRatingId generated by hbm2java
 */
@Embeddable
public class PostRatingId implements java.io.Serializable {

	private long postId;
	private long userId;

	public PostRatingId() {
	}

	public PostRatingId(long postId, long userId) {
		this.postId = postId;
		this.userId = userId;
	}

	@Column(name = "POST_ID", nullable = false)
	public long getPostId() {
		return this.postId;
	}

	public void setPostId(long postId) {
		this.postId = postId;
	}

	@Column(name = "USER_ID", nullable = false)
	public long getUserId() {
		return this.userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PostRatingId))
			return false;
		PostRatingId castOther = (PostRatingId) other;

		return (this.getPostId() == castOther.getPostId()) && (this.getUserId() == castOther.getUserId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getPostId();
		result = 37 * result + (int) this.getUserId();
		return result;
	}

}
