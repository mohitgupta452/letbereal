package com.uthwork.letbereal.model;
// Generated Jan 20, 2018 11:34:25 AM by Hibernate Tools 5.1.0.Alpha1

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * UserFollowersId generated by hbm2java
 */
@Embeddable
public class UserFollowersId implements java.io.Serializable {

	private long followersUserId;
	private long followingUserId;

	public UserFollowersId() {
	}

	public UserFollowersId(long followersUserId, long followingUserId) {
		this.followersUserId = followersUserId;
		this.followingUserId = followingUserId;
	}

	@Column(name = "FOLLOWERS_USER_ID", nullable = false)
	public long getFollowersUserId() {
		return this.followersUserId;
	}

	public void setFollowersUserId(long followersUserId) {
		this.followersUserId = followersUserId;
	}

	@Column(name = "FOLLOWING_USER_ID", nullable = false)
	public long getFollowingUserId() {
		return this.followingUserId;
	}

	public void setFollowingUserId(long followingUserId) {
		this.followingUserId = followingUserId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof UserFollowersId))
			return false;
		UserFollowersId castOther = (UserFollowersId) other;

		return (this.getFollowersUserId() == castOther.getFollowersUserId())
				&& (this.getFollowingUserId() == castOther.getFollowingUserId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getFollowersUserId();
		result = 37 * result + (int) this.getFollowingUserId();
		return result;
	}

}
