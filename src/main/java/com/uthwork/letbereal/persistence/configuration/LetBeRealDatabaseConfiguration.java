package com.uthwork.letbereal.persistence.configuration;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;


@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@EnableJpaRepositories(basePackages={"com.uthwork.letbereal.persistence.repository"})
public class LetBeRealDatabaseConfiguration {


    @Autowired
    private Environment env;

    @Autowired
    DatabasePropertyConfiguration databasePropertyConfiguration;


    @Value("${spring.jpa.hibernate.ddl-auto}")
    public String HIBERNATE_HBM2DDl_AUTO;

    @Value("${spring.jpa.hibernate.dialect}")
    public String HIBERNATE_DIALECT;

    @Value("${spring.jpa.hibernate.show-sql}")
    public String HIBERNATE_SHOW_SQL;

    @Bean 
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databasePropertyConfiguration.driver);
        dataSource.setUrl(databasePropertyConfiguration.url);
        dataSource.setUsername(databasePropertyConfiguration.username);
        dataSource.setPassword(databasePropertyConfiguration.password);
        return dataSource;
    }


    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(new String[] { "com.uthwork.letbereal.model" });
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.setJpaProperties(additionalProperties());

        return em;
    }


    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
        return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto",databasePropertyConfiguration.HIBERNATE_HBM2DDl_AUTO);
        properties.setProperty("hibernate.dialect", databasePropertyConfiguration.HIBERNATE_DIALECT);
        return properties;
    }

}
