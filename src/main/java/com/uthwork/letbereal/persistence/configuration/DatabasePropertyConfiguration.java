package com.uthwork.letbereal.persistence.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="spring.datasource")
public class DatabasePropertyConfiguration {

     String driver;
     String url;
     String password;
     String username;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    public String HIBERNATE_HBM2DDl_AUTO;

    @Value("${spring.jpa.hibernate.dialect}")
    public String HIBERNATE_DIALECT;

    @Value("${spring.jpa.hibernate.show-sql}")
    public String HIBERNATE_SHOW_SQL;

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
