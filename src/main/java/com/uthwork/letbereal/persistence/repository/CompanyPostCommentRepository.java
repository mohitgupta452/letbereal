package com.uthwork.letbereal.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.uthwork.letbereal.model.CompanyPostComment;


public interface CompanyPostCommentRepository extends JpaRepository<CompanyPostComment, Long> {

	@Query("select u from CompanyPostComment u where u.commentId=?1")
	CompanyPostComment findByCommentId(Long commentId);
	
	@Query("select u from CompanyPostComment u where u.companyPost.postId=?1")
	List<CompanyPostComment> findAllCompanyPostCommentByPostId(Long postId);
}
