package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.Company;

public interface CompanyRepository extends JpaRepository<Company, Long>  {

	@Query("select u from Company u where u.companyEmailId=?1")
	Company findByCompanyEmailId(String companyEmailId);
	
	@Query("select u from Company u where u.companyName=?1")
	Company findByCompanyName(String companyName);
	
	@Query("select u from Company u where u.companyId=?1")
	Company findByCompanyId(Long companyId);
	
	@Modifying
	@Query("Update Company u set u.companyImageUrl=?1 where u.companyId=?2")
	int updateCompanyProfileImage(String url, long companyId);
	
	@Modifying
	@Query("Update Company u set u.companyCoverUrl=?1 where u.companyId=?2")
	int updateCompanyCoverPhoto(String url, long companyId);
}
