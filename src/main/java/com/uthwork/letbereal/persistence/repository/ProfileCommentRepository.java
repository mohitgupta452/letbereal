package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.ProfileComment;
import com.uthwork.letbereal.model.ProfileCommentId;

import java.util.List;


public interface ProfileCommentRepository extends JpaRepository<ProfileComment,ProfileCommentId> {

    @Query("From ProfileComment u where u.userByUserId.userId=?1")
    List<ProfileComment> findCommentbyProfileId(Long profileId);

    @Modifying
    @Query("DELETE FROM ProfileComment pr where pr.id.profileId=?1 and pr.id.userId=?2")
	int deleteProfileComment(Long profileId, Long userId);


	}
	

