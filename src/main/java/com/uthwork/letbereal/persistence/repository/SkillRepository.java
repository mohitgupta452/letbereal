package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uthwork.letbereal.model.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {

}
