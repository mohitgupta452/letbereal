package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.uthwork.letbereal.model.ProfileRating;
import com.uthwork.letbereal.model.ProfileRatingId;

import java.util.List;

public interface ProfileRatingRepository extends CrudRepository<ProfileRating, ProfileRatingId> {

    @Query("FROM ProfileRating pr where pr.id.profileId=?1")
    List<ProfileRating> getProfileRatingList(long profileId);

    @Query("SElECT ROUND(AVG(pr.rating),1) FROM ProfileRating pr where pr.id.profileId=?1")
    Float findAvgProfileRatingByProfileId(long userId);

    @Modifying
    @Query("DELETE FROM ProfileRating pr where pr.id.profileId=?1 and pr.id.userId=?2")
    int deleteProfileRating(Long profileId, Long userId);

}
