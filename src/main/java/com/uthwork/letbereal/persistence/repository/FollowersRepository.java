package com.uthwork.letbereal.persistence.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.Skill;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.model.UserFollowers;
import com.uthwork.letbereal.model.UserFollowersId;

public interface FollowersRepository extends JpaRepository<UserFollowers, UserFollowersId> {

	
	@Query("SELECT us.userByFollowersUserId FROM UserFollowers us where us.id.followingUserId=?1")
	List<User> getUserFollowersListByUserID(Long userId);
	
	@Query("SELECT us.userByFollowingUserId FROM UserFollowers us where us.id.followersUserId=?1")
	List<User> getFollowingsList(Long userId);
	
	
	@Query("SELECT count(us.userByFollowersUserId) FROM UserFollowers us where us.id.followingUserId=?1")
	long getNumberOfFollowers(long userID);
	
	@Query("SELECT count(us.userByFollowersUserId) FROM UserFollowers us where us.id.followersUserId=?1")
	long getNumberOfFollowings(long userID);
	
	
}
