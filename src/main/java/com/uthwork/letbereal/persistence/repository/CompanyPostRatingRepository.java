package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.CompanyPostRating;

public interface CompanyPostRatingRepository extends JpaRepository<CompanyPostRating, Long> {

	@Query("select u from CompanyPostRating u where u.postId=?1")
	CompanyPostRating findByPostId(Long postId);
}
