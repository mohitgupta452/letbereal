package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.uthwork.letbereal.model.PostRating;
import com.uthwork.letbereal.model.PostRatingId;

import javax.transaction.Transactional;
import java.util.List;

public interface PostRatingRepository extends CrudRepository<PostRating, PostRatingId> {



    @Query("FROM PostRating pr where pr.id.postId IN (SELECT p.postId FROM Post p where p.user.userId=?1)")
    List<PostRating> getPostRatingList(long postId);

    @Query("SElECT AVG(pr.rating) FROM PostRating pr where pr.id.postId=?1")
    Float findAvgPostRatingByPostId(Long postId);

    @Modifying
    @Query("DELETE FROM PostRating pr where pr.id.postId=?1 and pr.id.userId=?2")
    int deletePostRating(Long postId, Long userId);

    
    @Modifying
    @Query("delete From PostRating u where u.post.postId=?1")
    int deletePostRatingByPostId(Long postId);

}
