package com.uthwork.letbereal.persistence.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.Post;
import com.uthwork.letbereal.model.PostComment;
import com.uthwork.letbereal.post.domain.response.PostResponse;

public interface PostRepository extends JpaRepository<Post,Long>{

    @Query("From Post u where u.user.userId=?1")
    List<Post> findByUserPostId(Long userId);


    @Query("From PostComment u where u.post.postId=?1")
    List<PostComment> findCommentbyPostId(Long postId);



	@Modifying
    @Query("delete From Post u where u.postId=?1")
    public int deletePostByPostId(Long postId);
	
	@Query("select u from Post u where u.user.userId=?1")
	public List<Post> findByUserId(Long userId);

}
