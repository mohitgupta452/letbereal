package com.uthwork.letbereal.persistence.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.Skill;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.model.UserSkills;
import com.uthwork.letbereal.model.UserSkillsId;

public interface UserSkillRepository extends JpaRepository<UserSkills, UserSkillsId>{
    
	@Query("SELECT us.skill FROM UserSkills us where us.id.userId=?1")
	List<Skill> getUserSkillsListByUserID(Long userId);
	
	@Transactional
	@Modifying
	@Query("DELETE  FROM UserSkills us where us.id.userId=?1")
	public void deleteUserSkillsListByUserID(Long userId);
	
	@Query("SELECT DISTINCT(us.user) FROM UserSkills us where us.id.skillId in(?1)")
	List<User> getUserListBySkillsIDs(List<Long> skillsList);

}
