package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uthwork.letbereal.model.ImageUrl;

public interface ImageUrlRepository extends JpaRepository<ImageUrl, Long> {

}
