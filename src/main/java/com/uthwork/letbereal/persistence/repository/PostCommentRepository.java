package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.Post;
import com.uthwork.letbereal.model.PostComment;
import com.uthwork.letbereal.model.PostCommentId;

import java.util.List;

import javax.transaction.Transactional;

public interface PostCommentRepository extends JpaRepository<PostComment,PostCommentId> {

	
	@Query("From Post u where u.user.userId=?1 and u.postId=?2")
	Post restrictCommentbyPostedUser(Long userId,Long postId);


	@Modifying
	@Query("delete From PostComment u where u.post.postId=?1")
	public int deletePostCommentByPostId(Long postId);

	
	@Modifying
	@Query("DELETE FROM PostComment pr where pr.id.postId=?1 and pr.id.userId=?2")
	int deletePostComment(Long postId, Long userId);
	
	@Query("select u from PostComment u where u.post.postId=?1")
	public List<PostComment> findCommentByPostId(Long postId);



}
