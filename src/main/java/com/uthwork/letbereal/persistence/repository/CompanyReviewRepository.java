package com.uthwork.letbereal.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.CompanyReview;

public interface CompanyReviewRepository extends JpaRepository<CompanyReview, Long> {

	@Query("select u from CompanyReview u where u.reviewId=?1")
	CompanyReview findReviewByReviewId(Long reviewId);

	@Query("select u from CompanyReview u where u.company.companyId=?1")
	List<CompanyReview> findAllReviewByCompanyId(long companyId);
}
