package com.uthwork.letbereal.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.uthwork.letbereal.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	@Query("From User u where u.email=?1")
	User findByEmail(String email);

	@Modifying
	@Query("Update User u set u.imageUrl=?1 where u.userId=?2")
	int updateImageUrl(String imageUrl, long profileId);

	@Modifying
	@Query("Update User u set u.imageUrlId=?1 where u.userId=?2")
	int updateImageUrlId(long imageUrlId, long profileId);
	
	@Query("FROM User u WHERE u.firstName LIKE CONCAT('%',:firstName,'%')")
	List<User> searchUsersListByName(@Param("firstName") String firstName);

	@Query("From User u where u.userId=?1")
	User findByUserProfileId(Long userId);

	@Query("FROM User u WHERE u.location LIKE CONCAT('%',:location,'%') ")
	List<User> searchUsersListByLocation(@Param("location") String location);

	@Query("FROM User u WHERE u.firstName LIKE  %:name% or u.lastName LIKE %:name% or u.email LIKE %:name%) ")
	List<User> searchUsersListByNameEmail(@Param("name") String name);

	
	@Modifying
	@Query("Update User u set u.imageUrl=?1 where u.userId=?2")
	int updateUserProfileImage(String url, long profileId);
	
	//void updateUserProfileImage(long profileId, String url);
	
}
