package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.uthwork.letbereal.model.CompanyRating;

public interface CompanyRatingRepository extends JpaRepository<CompanyRating, Long> {

	@Query("select u from CompanyRating u where u.companyId=?1")
	CompanyRating findByCompanyId(Long companyId);
	
	@Query("select u from CompanyRating u where u.companyId=?1")
	CompanyRating deleteByCompanyId(Long companyId);
}
