package com.uthwork.letbereal.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uthwork.letbereal.model.BadComment;

public interface BadCommentRepository extends JpaRepository<BadComment, Long> {

}
