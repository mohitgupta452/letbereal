package com.uthwork.letbereal.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.uthwork.letbereal.model.CompanyPost;

public interface CompanyPostRepository extends JpaRepository<CompanyPost, Long> {
	
	@Query("select u from CompanyPost u where u.company.companyId=?1")
	CompanyPost findByCompanyId(Long companyId);
	
	@Query("select u from CompanyPost u where u.postId=?1")
	CompanyPost findByPostId(Long postId);
	
	@Query("select u from CompanyPost u where u.company.companyId=?1")
	List<CompanyPost> findAllCompanyPostByCompanyId(Long companyId);
}


