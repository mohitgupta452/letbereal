package com.uthwork.letbereal.rating.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.uthwork.letbereal.model.PostRating;
import com.uthwork.letbereal.rating.domain.PostRatingInfo;
import com.uthwork.letbereal.rating.domain.request.DeletePostRatingRequest;
import com.uthwork.letbereal.rating.domain.request.PostRatingRequest;
import com.uthwork.letbereal.rating.domain.response.*;
import com.uthwork.letbereal.rating.service.PostRatingService;
import com.uthwork.letbereal.security.UserSession;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/letbereal")
public class PostRatingController {

    @Autowired
    PostRatingService postRatingService;
    
    /* savePostRating: This method is used to rate any post. */
    
    @CrossOrigin(origins="*")
    @PostMapping("/savePostRating")
    public PostRatingResponse saveProfileRating(Authentication authentication, @Valid @RequestBody PostRatingRequest ratingRequest, BindingResult result){
        PostRatingResponse response=new PostRatingResponse();
        if(result.hasFieldErrors("profileId")){
            response.setPostIdValid(false);
        }
        response.setPostIdValid(true);
        if(result.hasFieldErrors("rating")||!(ratingRequest.getRating()>=0&&ratingRequest.getRating()<=5)){
            response.setRatingValid(false);
        }
        response.setRatingValid(true);
        UserSession userSession = (UserSession) authentication.getPrincipal();
        ratingRequest.setUserId(userSession.getProfileId());
        PostRating profileRating=postRatingService.savePostRating(ratingRequest);
        if(profileRating==null){
            response.setRatingValid(false);
            return response;
        }
        return response;
    }

    /* getPostRating/post/{postId}: This method is used to get rating list of selected post. */
    
    @CrossOrigin(origins="*")
    @GetMapping("/getPostRating/post/{postId}")
    public GetPostRatingResponse getProfileRating(Authentication authentication, @PathVariable("postId") Long postId){
        GetPostRatingResponse ratingResponse=new GetPostRatingResponse();
        if(postId==null) {
            ratingResponse.setProfileIdValid(false);
            return ratingResponse;
        }
        ratingResponse.setProfileIdValid(true);
        UserSession userSession= (UserSession) authentication.getPrincipal();
        PostRatingInfo ratingInfo=postRatingService.getPostRating(postId,userSession.getProfileId());
        ratingResponse.setRatingInfo(ratingInfo);
        return  ratingResponse;
    }

    /* getPostRatingList: This method is used to show average rating of every post. 
     *  on selection of any post one can see the descriptive rating of post.
     */
    
    @CrossOrigin(origins="*")
    @GetMapping("/getPostRatingList")
    public GetPostRatingListResponse getProfileRatingList(Authentication authentication){
        GetPostRatingListResponse ratingResponse=new GetPostRatingListResponse();
        UserSession userSession= (UserSession) authentication.getPrincipal();
        List<PostRatingInfo> ratingInfoList=postRatingService.getPostRatingList(userSession.getProfileId());
        ratingResponse.setRatingInfoList(ratingInfoList);
        return  ratingResponse;
    }
    

    /* updatePostRating: This method is used to update rating of post.  */

    @CrossOrigin(origins="*")
    @PostMapping("/updatePostRating")
    public ProfileRatingResponse updateProfileRating(Authentication authentication, @Valid @RequestBody PostRatingRequest ratingRequest, BindingResult result){
        ProfileRatingResponse response=new ProfileRatingResponse();
        if(result.hasFieldErrors("profileId")){
            response.setProfileIdValid(false);
        }
        response.setProfileIdValid(true);
        if(result.hasFieldErrors("rating")||!(ratingRequest.getRating()>=0&&ratingRequest.getRating()<=5)){
            response.setRatingValid(false);
        }
        response.setRatingValid(true);
        UserSession userSession = (UserSession) authentication.getPrincipal();
        ratingRequest.setUserId(userSession.getProfileId());
        PostRating postRating=postRatingService.updatePostRating(ratingRequest);
        if(postRating==null){
            response.setRatingValid(false);
            return response;
        }
        return response;
    }

    /* deletePostRating: This method is used to delete rating of post.  */
    
    @CrossOrigin(origins="*")
    @PostMapping("/deletePostRating")
    public DeletePostRatingResponse deletePostRating(Authentication authentication, @Valid @RequestBody DeletePostRatingRequest ratingRequest, BindingResult result){

        DeletePostRatingResponse response=new DeletePostRatingResponse();
        if(result.hasFieldErrors("postId")){
            response.setPostIdValid(false);
            return response;
        }
        response.setPostIdValid(true);

        UserSession userSession = (UserSession) authentication.getPrincipal();
        ratingRequest.setUserId(userSession.getProfileId());
        int profileRating=postRatingService.deletePostRating(ratingRequest);
        if(profileRating==0){
            response.setPostIdValid(false);
            return response;
        }
        return response;
    }


}
