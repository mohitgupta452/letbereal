package com.uthwork.letbereal.rating.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.uthwork.letbereal.model.ProfileRating;
import com.uthwork.letbereal.rating.domain.ProfileRatingInfo;
import com.uthwork.letbereal.rating.domain.request.DeleteProfileRatingRequest;
import com.uthwork.letbereal.rating.domain.request.ProfileRatingRequest;
import com.uthwork.letbereal.rating.domain.response.DeleteProfileRatingResponse;
import com.uthwork.letbereal.rating.domain.response.GetProfileRatingListResponse;
import com.uthwork.letbereal.rating.domain.response.GetProfileRatingResponse;
import com.uthwork.letbereal.rating.domain.response.ProfileRatingResponse;
import com.uthwork.letbereal.rating.service.ProfileRatingService;
import com.uthwork.letbereal.security.UserSession;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/letbereal")
public class ProfileRatingController {


    @Autowired
    ProfileRatingService profileRatingService;


    /* saveProfileRating: This method is used to save rate others profile.  */
    
    @CrossOrigin(origins="*")
    @PostMapping("/saveProfileRating")
    public ProfileRatingResponse saveProfileRating(Authentication authentication, @Valid @RequestBody ProfileRatingRequest ratingRequest, BindingResult result){
        ProfileRatingResponse response=new ProfileRatingResponse();
        UserSession userSession = (UserSession) authentication.getPrincipal();
        ratingRequest.setUserId(userSession.getProfileId());
        
        if(result.hasFieldErrors("profileId") || ratingRequest.getProfileId().equals(userSession.getProfileId())){
            response.setProfileIdValid(false);
        }
        response.setProfileIdValid(true);
        if(result.hasFieldErrors("rating")||!(ratingRequest.getRating()>=0&&ratingRequest.getRating()<=5)){
            response.setRatingValid(false);
        }
        response.setRatingValid(true);
       
        ProfileRating profileRating=profileRatingService.saveProfileRating(ratingRequest);
        if(profileRating==null){
            response.setRatingValid(false);
            return response;
        }
        return response;
    }

    /* getProfileRating/profile/{profileId}: This method is used to see profile rating by id.  */
    
    @CrossOrigin(origins="*")
    @GetMapping("/getProfileRating/profile/{profileId}")
    public GetProfileRatingResponse getProfileRating(Authentication authentication,@PathVariable("profileId") Long profileId){
        GetProfileRatingResponse ratingResponse=new GetProfileRatingResponse();
        if(profileId==null) {
            ratingResponse.setProfileIdValid(false);
            return ratingResponse;
        }
        ratingResponse.setProfileIdValid(true);
        UserSession userSession= (UserSession) authentication.getPrincipal();
        ProfileRatingInfo ratingInfo=profileRatingService.getProfileRating(profileId,userSession.getProfileId());
        ratingResponse.setRatingInfo(ratingInfo);
        return  ratingResponse;
    }
    
    /* getProfileRatingList: This method is used to see the complete list of ratings. */
    
    @CrossOrigin(origins="*")
    @GetMapping("/getProfileRatingList")
    public GetProfileRatingListResponse getProfileRatingList(Authentication authentication){
        GetProfileRatingListResponse ratingResponse=new GetProfileRatingListResponse();
        UserSession userSession= (UserSession) authentication.getPrincipal();
        List<ProfileRatingInfo> ratingInfoList=profileRatingService.getProfileRatingList(userSession.getProfileId());
        ratingResponse.setRatingInfoList(ratingInfoList);
        return  ratingResponse;
    }

    /* updateProfileRating: This method is used to update rating of profile. */
    
    @CrossOrigin(origins="*")
    @PostMapping("/updateProfileRating")
    public ProfileRatingResponse updateProfileRating(Authentication authentication, @Valid @RequestBody ProfileRatingRequest ratingRequest, BindingResult result){
       
    	ProfileRatingResponse response=new ProfileRatingResponse();
    	  UserSession userSession = (UserSession) authentication.getPrincipal();
          ratingRequest.setUserId(userSession.getProfileId());
        if(result.hasFieldErrors("profileId") || ratingRequest.getProfileId().equals(userSession.getProfileId())){
            response.setProfileIdValid(false);
        }
        response.setProfileIdValid(true);
        if(result.hasFieldErrors("rating")||!(ratingRequest.getRating()>=0&&ratingRequest.getRating()<=5)){
            response.setRatingValid(false);
        }
        response.setRatingValid(true);
      
        ProfileRating profileRating=profileRatingService.updateProfileRating(ratingRequest);
        if(profileRating==null){
            response.setRatingValid(false);
            return response;
        }
        return response;
    }

    /* deleteProfileRating: This method is used to delete rating of profile. */
    
    @CrossOrigin(origins="*")
    @PostMapping("/deleteProfileRating")
    public DeleteProfileRatingResponse deleteProfileRating(Authentication authentication, @Valid @RequestBody DeleteProfileRatingRequest ratingRequest, BindingResult result){

        UserSession userSession = (UserSession) authentication.getPrincipal();
        ratingRequest.setUserId(userSession.getProfileId());
        DeleteProfileRatingResponse response=new DeleteProfileRatingResponse();
        
        if(result.hasFieldErrors("profileId") || ratingRequest.getProfileId().equals(userSession.getProfileId())){
            response.setProfileIdValid(false);
            return response;
        }
        response.setProfileIdValid(true);

        int profileRating=profileRatingService.deleteProfileRating(ratingRequest);
        if(profileRating==0){
            response.setProfileIdValid(false);
            return response;
        }
        return response;
    }



}
