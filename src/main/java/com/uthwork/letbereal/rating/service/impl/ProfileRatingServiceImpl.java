package com.uthwork.letbereal.rating.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uthwork.letbereal.model.ProfileRating;
import com.uthwork.letbereal.model.ProfileRatingId;
import com.uthwork.letbereal.persistence.repository.ProfileRatingRepository;
import com.uthwork.letbereal.rating.domain.ProfileRatingInfo;
import com.uthwork.letbereal.rating.domain.request.DeleteProfileRatingRequest;
import com.uthwork.letbereal.rating.domain.request.ProfileRatingRequest;
import com.uthwork.letbereal.rating.service.ProfileRatingService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProfileRatingServiceImpl implements ProfileRatingService {

    @Autowired
    ProfileRatingRepository profileRatingRepository;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProfileRating saveProfileRating(ProfileRatingRequest ratingRequest) {
        ProfileRatingId id=new ProfileRatingId(ratingRequest.getProfileId(),ratingRequest.getUserId());
        ProfileRating profileRating=new ProfileRating(id,null,null,new Date());
        profileRating.setRating(ratingRequest.getRating());
        profileRating.setCreatedTime(new Date());
        return profileRatingRepository.save(profileRating);
    }

    @Override
    public ProfileRatingInfo getProfileRating(Long profileId, long userId) {
        ProfileRatingId id1 = new ProfileRatingId(profileId,userId);
        ProfileRating profileRating=profileRatingRepository.findOne(id1);
        ProfileRatingInfo ratingInfo=new ProfileRatingInfo();
        ratingInfo.setProfileId(profileRating.getId().getProfileId());
        ratingInfo.setUserId(profileRating.getId().getUserId());
        return ratingInfo;
    }

    @Override
    public List<ProfileRatingInfo> getProfileRatingList(long profileId) {
        List<ProfileRating> profileRatingList=profileRatingRepository.getProfileRatingList(profileId);
        List<ProfileRatingInfo> ratingInfos=new ArrayList<ProfileRatingInfo>();
        for(ProfileRating profileRating:profileRatingList){
            ProfileRatingInfo ratingInfo=new ProfileRatingInfo();
            ratingInfo.setProfileId(profileRating.getId().getProfileId());
            ratingInfo.setUserId(profileRating.getId().getUserId());
            ratingInfo.setRating(profileRating.getRating());
            ratingInfos.add(ratingInfo);
        }
        return ratingInfos;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProfileRating updateProfileRating(ProfileRatingRequest ratingRequest) {
        ProfileRatingId id=new ProfileRatingId(ratingRequest.getProfileId(),ratingRequest.getUserId());
        ProfileRating profileRating=profileRatingRepository.findOne(id);
        profileRating.setRating(ratingRequest.getRating());
        profileRating.setCreatedTime(new Date());
        return profileRatingRepository.save(profileRating);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteProfileRating(DeleteProfileRatingRequest ratingRequest) {
        return profileRatingRepository.deleteProfileRating(ratingRequest.getProfileId(),ratingRequest.getUserId());
    }

}
