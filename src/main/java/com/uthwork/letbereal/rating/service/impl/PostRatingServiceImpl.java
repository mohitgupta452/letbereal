package com.uthwork.letbereal.rating.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uthwork.letbereal.model.PostRating;
import com.uthwork.letbereal.model.PostRatingId;
import com.uthwork.letbereal.persistence.repository.PostRatingRepository;
import com.uthwork.letbereal.rating.domain.PostRatingInfo;
import com.uthwork.letbereal.rating.domain.request.DeletePostRatingRequest;
import com.uthwork.letbereal.rating.domain.request.PostRatingRequest;
import com.uthwork.letbereal.rating.service.PostRatingService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PostRatingServiceImpl implements PostRatingService {

    @Autowired
    PostRatingRepository postRatingRepository;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public PostRating savePostRating(PostRatingRequest ratingRequest) {
    	System.out.println("ratingRequest==="+ratingRequest.getPostId()+"  ,   "+ratingRequest.getUserId());
        PostRatingId id=new PostRatingId(ratingRequest.getPostId(),ratingRequest.getUserId());
        PostRating postRating=new PostRating();
        postRating.setRating(ratingRequest.getRating());
        postRating.setCreatedTime(new Date());
        postRating.setId(id);
        return postRatingRepository.save(postRating);
    }

    @Override
    public PostRatingInfo getPostRating(Long postId, long userId) {

        PostRatingId id1 = new PostRatingId(postId,userId);
        PostRating postRating=postRatingRepository.findOne(id1);
        PostRatingInfo ratingInfo=new PostRatingInfo();
        ratingInfo.setPostId(postRating.getId().getPostId());
        ratingInfo.setUserId(postRating.getId().getUserId());
        ratingInfo.setRating(postRating.getRating());
        return ratingInfo;
    }

    @Override
    public List<PostRatingInfo> getPostRatingList(long postId) {
        List<PostRating> profileRatingList=postRatingRepository.getPostRatingList(postId);
        List<PostRatingInfo> ratingInfos=new ArrayList<PostRatingInfo>();
        for(PostRating postRating:profileRatingList){
            PostRatingInfo ratingInfo=new PostRatingInfo();
            ratingInfo.setPostId(postRating.getId().getPostId());
            ratingInfo.setUserId(postRating.getId().getUserId());
            ratingInfos.add(ratingInfo);
        }
        return ratingInfos;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PostRating updatePostRating(PostRatingRequest ratingRequest) {
        PostRatingId id=new PostRatingId(ratingRequest.getPostId(),ratingRequest.getUserId());
        PostRating postRating=postRatingRepository.findOne(id);
        postRating.setRating(ratingRequest.getRating());
        postRating.setCreatedTime(new Date());
        postRating.setUpdateTime(new Date());
        return postRatingRepository.save(postRating);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deletePostRating(DeletePostRatingRequest ratingRequest) {
        return postRatingRepository.deletePostRating(ratingRequest.getPostId(),ratingRequest.getUserId());
    }

}
