package com.uthwork.letbereal.rating.service;

import java.util.List;

import com.uthwork.letbereal.model.ProfileRating;
import com.uthwork.letbereal.rating.domain.ProfileRatingInfo;
import com.uthwork.letbereal.rating.domain.request.DeleteProfileRatingRequest;
import com.uthwork.letbereal.rating.domain.request.ProfileRatingRequest;

public interface ProfileRatingService {


    ProfileRating saveProfileRating(ProfileRatingRequest ratingRequest);

    ProfileRatingInfo getProfileRating(Long profileId, long id);

    List<ProfileRatingInfo> getProfileRatingList(long profileId);

    ProfileRating updateProfileRating(ProfileRatingRequest ratingRequest);

    int deleteProfileRating(DeleteProfileRatingRequest ratingRequest);
}
