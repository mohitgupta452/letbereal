package com.uthwork.letbereal.rating.service;

import java.util.List;

import com.uthwork.letbereal.model.PostRating;
import com.uthwork.letbereal.rating.domain.PostRatingInfo;
import com.uthwork.letbereal.rating.domain.request.DeletePostRatingRequest;
import com.uthwork.letbereal.rating.domain.request.PostRatingRequest;

public interface PostRatingService {


    PostRating savePostRating(PostRatingRequest ratingRequest);

    PostRatingInfo getPostRating(Long postId, long userId);

    List<PostRatingInfo> getPostRatingList(long profileId);

    PostRating updatePostRating(PostRatingRequest ratingRequest);

    int deletePostRating(DeletePostRatingRequest ratingRequest);
}
