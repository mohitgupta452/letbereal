package com.uthwork.letbereal.rating.domain.request;

import javax.validation.constraints.NotNull;

public class ProfileRatingRequest {

    @NotNull
    private Long profileId;
    @NotNull
    private Integer rating;
    private Long userId;

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
