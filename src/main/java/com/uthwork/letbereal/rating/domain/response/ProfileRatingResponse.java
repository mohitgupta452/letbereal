package com.uthwork.letbereal.rating.domain.response;

public class ProfileRatingResponse {

    private boolean isProfileIdValid;
    private boolean isRatingValid;

    public boolean isProfileIdValid() {
        return isProfileIdValid;
    }

    public void setProfileIdValid(boolean profileIdValid) {
        isProfileIdValid = profileIdValid;
    }

    public boolean isRatingValid() {
        return isRatingValid;
    }

    public void setRatingValid(boolean ratingValid) {
        isRatingValid = ratingValid;
    }
}
