package com.uthwork.letbereal.rating.domain.request;

import javax.validation.constraints.NotNull;

public class PostRatingRequest {

    @NotNull
    private Long postId;
    @NotNull
    private Integer rating;
    private Long userId;

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
