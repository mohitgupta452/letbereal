package com.uthwork.letbereal.rating.domain.request;

import javax.validation.constraints.NotNull;

public class DeletePostRatingRequest {

    @NotNull
    private Long postId;
    private Long userId;

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
