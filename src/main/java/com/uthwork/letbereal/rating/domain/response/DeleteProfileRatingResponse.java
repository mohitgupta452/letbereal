package com.uthwork.letbereal.rating.domain.response;

public class DeleteProfileRatingResponse {

    boolean isProfileIdValid;

    public boolean isProfileIdValid() {
        return isProfileIdValid;
    }

    public void setProfileIdValid(boolean profileIdValid) {
        isProfileIdValid = profileIdValid;
    }
}
