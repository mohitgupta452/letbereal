package com.uthwork.letbereal.rating.domain.response;

public class PostRatingResponse {

    boolean isPostIdValid;
    private boolean isRatingValid;

    public boolean isPostIdValid() {
        return isPostIdValid;
    }

    public void setPostIdValid(boolean postIdValid) {
        isPostIdValid = postIdValid;
    }

    public boolean isRatingValid() {
        return isRatingValid;
    }

    public void setRatingValid(boolean ratingValid) {
        isRatingValid = ratingValid;
    }
}
