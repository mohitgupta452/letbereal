package com.uthwork.letbereal.rating.domain.response;

public class DeletePostRatingResponse {

    boolean isPostIdValid;

    public boolean isPostIdValid() {
        return isPostIdValid;
    }

    public void setPostIdValid(boolean postIdValid) {
        isPostIdValid = postIdValid;
    }
}
