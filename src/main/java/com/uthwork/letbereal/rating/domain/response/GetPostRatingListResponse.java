package com.uthwork.letbereal.rating.domain.response;

import java.util.List;

import com.uthwork.letbereal.rating.domain.PostRatingInfo;

public class GetPostRatingListResponse {

    List<PostRatingInfo> ratingInfoList;

    public List<PostRatingInfo> getRatingInfoList() {
        return ratingInfoList;
    }

    public void setRatingInfoList(List<PostRatingInfo> ratingInfoList) {
        this.ratingInfoList = ratingInfoList;
    }
}
