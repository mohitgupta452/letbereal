package com.uthwork.letbereal.rating.domain.response;

import com.uthwork.letbereal.rating.domain.PostRatingInfo;

public class GetPostRatingResponse {

    boolean isProfileIdValid;
    PostRatingInfo ratingInfo;

    public boolean isProfileIdValid() {
        return isProfileIdValid;
    }

    public void setProfileIdValid(boolean profileIdValid) {
        isProfileIdValid = profileIdValid;
    }

    public PostRatingInfo getRatingInfo() {
        return ratingInfo;
    }

    public void setRatingInfo(PostRatingInfo ratingInfo) {
        this.ratingInfo = ratingInfo;
    }
}
