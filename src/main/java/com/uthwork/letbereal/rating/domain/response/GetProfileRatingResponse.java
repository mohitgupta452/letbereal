package com.uthwork.letbereal.rating.domain.response;

import com.uthwork.letbereal.rating.domain.ProfileRatingInfo;

public class GetProfileRatingResponse {

    boolean isProfileIdValid;
    ProfileRatingInfo ratingInfo;

    public boolean isProfileIdValid() {
        return isProfileIdValid;
    }

    public void setProfileIdValid(boolean profileIdValid) {
        isProfileIdValid = profileIdValid;
    }

    public ProfileRatingInfo getRatingInfo() {
        return ratingInfo;
    }

    public void setRatingInfo(ProfileRatingInfo ratingInfo) {
        this.ratingInfo = ratingInfo;
    }
}
