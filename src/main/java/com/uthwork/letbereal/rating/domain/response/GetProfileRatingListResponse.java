package com.uthwork.letbereal.rating.domain.response;

import java.util.List;

import com.uthwork.letbereal.rating.domain.ProfileRatingInfo;

public class GetProfileRatingListResponse {

    List<ProfileRatingInfo> ratingInfoList;

    public List<ProfileRatingInfo> getRatingInfoList() {
        return ratingInfoList;
    }

    public void setRatingInfoList(List<ProfileRatingInfo> ratingInfoList) {
        this.ratingInfoList = ratingInfoList;
    }
}
