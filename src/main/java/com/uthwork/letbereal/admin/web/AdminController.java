package com.uthwork.letbereal.admin.web;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.uthwork.letbereal.admin.serviceImpl.AdminServiceImpl;
import com.uthwork.letbereal.model.Post;
import com.uthwork.letbereal.model.PostComment;
import com.uthwork.letbereal.model.User;

@Controller
@EnableAutoConfiguration
@EnableWebMvc
public class AdminController {
	
	@Autowired
	AdminServiceImpl adminServiceImpl;
	
	@RequestMapping(value = "admin/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }
	

	@RequestMapping(value ={"/","/login"}, method = RequestMethod.GET)
    public String welcome(Model model) {
		return "redirect:/admin/login";
	}
	
	@RequestMapping(value ="admin/home", method = RequestMethod.GET)
    public String home(Model model) {
		List<User> users= adminServiceImpl.getAllUsers();
		model.addAttribute("users",users);
		return "home";
	}
	
	@RequestMapping(value ="admin/getAllPost/{userId}", method = RequestMethod.GET)
    public String getAllPost(Model model, @PathVariable("userId") Long userId) {
		
		System.out.println("userId  "+userId);
		List<Post> listOfPost = adminServiceImpl.getAllPostByUserId(userId);
		model.addAttribute("listOfPost",listOfPost);
		return "userPost";
	}
	
	@RequestMapping(value ="admin/getAllPost/getAllComment/{postId}", method = RequestMethod.GET)
    public String getAllComment(Model model, @PathVariable("postId") Long postId) {
		
		System.out.println("postId  "+postId);
		List<PostComment> listOfComment = adminServiceImpl.getAllCommentByUserId(postId);
		model.addAttribute("listOfComment",listOfComment);
		return "userComment";
	}
}
