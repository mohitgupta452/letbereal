package com.uthwork.letbereal.admin.serviceImpl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.model.Post;
import com.uthwork.letbereal.model.PostComment;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.PostCommentRepository;
import com.uthwork.letbereal.persistence.repository.PostRepository;
import com.uthwork.letbereal.persistence.repository.UserRepository;

@Service
public class AdminServiceImpl {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PostRepository postRepository;
	
	@Autowired
	PostCommentRepository PostCommentRepository;
	
	public List<User> getAllUsers()
	{
		List<User> users= userRepository.findAll();
		Iterator<User> itr=users.iterator();
		while(itr.hasNext())
		{
			User user=itr.next();
			if(user.getUserRole()!=null)
			{
				if(user.getUserRole().equals("admin"))
				{
					itr.remove();
				}
			}
		}
		return users;
	}
	
	public List<Post> getAllPostByUserId(Long userId)
	{
		List<Post> listOfPost=postRepository.findByUserId(userId);
		return listOfPost;
	}
	
	public List<PostComment> getAllCommentByUserId(Long postId)
	{
		List<PostComment> listOfComment = PostCommentRepository.findCommentByPostId(postId);
		return listOfComment;
	}
}
