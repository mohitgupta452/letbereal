package com.uthwork.letbereal.comp.request;

public class CompanyPostRatingRequest {

	private long postId;
	private int rating;
	
	public long getPostId() {
		return postId;
	}
	public void setPostId(long postId) {
		this.postId = postId;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
}
