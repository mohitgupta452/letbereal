package com.uthwork.letbereal.comp.request;

public class CompanyPostRequest {

	private long companyId;
	private long companyPostId;
	private int imageUrlId;
	private String description;
	private String title;
	public long getcompanyId() {
		return companyId;
	}
	public void setcompanyId(long companyId) {
		this.companyId = companyId;
	}
	public Integer getImageUrlId() {
		return imageUrlId;
	}
	public void setImageUrlId(Integer imageUrlId) {
		this.imageUrlId = imageUrlId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getcompanyPostId() {
		return companyPostId;
	}
	public void setcompanyPostId(long companyPostId) {
		this.companyPostId = companyPostId;
	}
	
	
}
