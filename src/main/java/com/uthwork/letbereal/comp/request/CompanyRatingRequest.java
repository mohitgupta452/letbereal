package com.uthwork.letbereal.comp.request;

public class CompanyRatingRequest {

	private String companyName;
	private int rating;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	
}
