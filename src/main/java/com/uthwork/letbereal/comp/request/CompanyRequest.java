package com.uthwork.letbereal.comp.request;

import java.util.Date;

public class CompanyRequest {
	
	private Long companyId;
	private String companyName;
	private String companySalg;
	private String ownerName;
	private String companyStartDate;
	private Integer noOfEmployees;
	private String companyContactNo;
	private String companyEmailId;
	private String companyImageUrl;
	private String companyCoverUrl;
	
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanySalg() {
		return companySalg;
	}
	public void setCompanySalg(String companySalg) {
		this.companySalg = companySalg;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getCompanyStartDate() {
		return companyStartDate;
	}
	public void setCompanyStartDate(String companyStartDate) {
		this.companyStartDate = companyStartDate;
	}
	public Integer getNoOfEmployees() {
		return noOfEmployees;
	}
	public void setNoOfEmployees(Integer noOfEmployees) {
		this.noOfEmployees = noOfEmployees;
	}
	public String getCompanyContactNo() {
		return companyContactNo;
	}
	public void setCompanyContactNo(String companyContactNo) {
		this.companyContactNo = companyContactNo;
	}
	public String getCompanyEmailId() {
		return companyEmailId;
	}
	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}
	public String getCompanyImageUrl() {
		return companyImageUrl;
	}
	public void setCompanyImageUrl(String companyImageUrl) {
		this.companyImageUrl = companyImageUrl;
	}
	public String getCompanyCoverUrl() {
		return companyCoverUrl;
	}
	public void setCompanyCoverUrl(String companyCoverUrl) {
		this.companyCoverUrl = companyCoverUrl;
	}
	
	
}
