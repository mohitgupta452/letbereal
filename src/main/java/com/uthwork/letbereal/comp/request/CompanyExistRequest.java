package com.uthwork.letbereal.comp.request;

public class CompanyExistRequest {

	private String companyEmailId;

	public String getCompanyEmailId() {
		return companyEmailId;
	}

	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}
}
