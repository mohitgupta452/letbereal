package com.uthwork.letbereal.comp.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uthwork.letbereal.comp.request.CompanyPostCommentRequest;
import com.uthwork.letbereal.comp.response.CompanyPostCommentResponse;
import com.uthwork.letbereal.comp.serviceImpl.CompanyPostCommentServiceImpl;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.CompanyPost;
import com.uthwork.letbereal.model.CompanyPostComment;
import com.uthwork.letbereal.persistence.repository.CompanyPostCommentRepository;
import com.uthwork.letbereal.persistence.repository.CompanyPostRepository;
import com.uthwork.letbereal.security.UserSession;

@RestController
@RequestMapping(value = "/letbereal/")
public class CompanyPostCommentController {
	
	@Autowired
	CompanyPostCommentServiceImpl companyPostCommentServiceImpl;
	
	@Autowired
	CompanyPostRepository companyPostRepository;
	
	@Autowired
	CompanyPostCommentRepository companyPostCommentRepository;

	@CrossOrigin(origins="*")
	@RequestMapping(value="saveCompanyPostComment",method=RequestMethod.POST)
	public @ResponseBody CompanyPostCommentResponse saveCompanyPostComment(Authentication principal, @RequestBody CompanyPostCommentRequest companyPostCommentRequest)
	{
		CompanyPost companyPost = companyPostRepository.findByPostId(companyPostCommentRequest.getPostId());
		CompanyPostCommentResponse companyPostCommentResponse= new CompanyPostCommentResponse();
		if(companyPost==null)
		{
			companyPostCommentResponse.setPostValid(false);
			companyPostCommentResponse.setCommentValid(false);
			companyPostCommentResponse.setMessage("Company Post with the given postId does not exist");
		}
		else
		{
			Company company=companyPost.getCompany();
			companyPostCommentResponse=companyPostCommentServiceImpl.saveCompanyPostComment(principal,company, companyPostCommentRequest);
		}
		return companyPostCommentResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="editCompanyPostComment",method=RequestMethod.POST)
	public @ResponseBody CompanyPostCommentResponse editCompanyPostComment(Authentication principal, @RequestBody CompanyPostCommentRequest companyPostCommentRequest)
	{
		CompanyPostComment companyPostComment=companyPostCommentRepository.findByCommentId(companyPostCommentRequest.getCommentId());
		CompanyPostCommentResponse companyPostCommentResponse=new CompanyPostCommentResponse();
		if(companyPostComment==null)
		{
			companyPostCommentResponse.setCommentValid(false);
			companyPostCommentResponse.setPostValid(false);
			companyPostCommentResponse.setMessage("Comment for the given commentId does not exist");
		}
		else
			companyPostCommentResponse=companyPostCommentServiceImpl.editCompanyPostComment(companyPostComment,companyPostCommentRequest);
		return companyPostCommentResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="deleteCompanyPostComment/{commentId}",method=RequestMethod.GET)
	public @ResponseBody CompanyPostCommentResponse deleteCommentByCommentId(@PathVariable("commentId") long commentId, Authentication principal)
	{
		UserSession us=(UserSession)principal.getPrincipal();
		CompanyPostComment companyPostComment=companyPostCommentRepository.findByCommentId(commentId);
		CompanyPostCommentResponse companyPostCommentResponse=new CompanyPostCommentResponse();
		if(companyPostComment==null)
			companyPostCommentResponse.setMessage("Comment for given commentId does not exist");
		else
		{
			if(companyPostComment.getUser().getUserId()==us.getProfileId())
			{
				companyPostCommentRepository.delete(companyPostComment);
				companyPostCommentResponse.setMessage("You have successfully deleted the comment");
			}
			else
				companyPostCommentResponse.setMessage("You are not authorize to delete this comment");
		}
		return companyPostCommentResponse;
	}
	
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="getAllCompanyPostComment/{postId}",method=RequestMethod.GET)
	public @ResponseBody List<CompanyPostComment> getAllCommentByPostId(@PathVariable("postId")long postId)
	{
		CompanyPost companyPost = companyPostRepository.findByPostId(postId);
		List<CompanyPostComment> listOfComments=null;
		if(companyPost!=null)
		{
			listOfComments=companyPostCommentRepository.findAllCompanyPostCommentByPostId(postId);
		}
		return listOfComments;
	}
}
