package com.uthwork.letbereal.comp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uthwork.letbereal.comp.request.CompanyPostRatingRequest;
import com.uthwork.letbereal.comp.response.CompanyPostRatingResponse;
import com.uthwork.letbereal.comp.serviceImpl.CompanyPostRatingServiceImpl;
import com.uthwork.letbereal.model.CompanyPost;
import com.uthwork.letbereal.model.CompanyPostRating;
import com.uthwork.letbereal.persistence.repository.CompanyPostRatingRepository;
import com.uthwork.letbereal.persistence.repository.CompanyPostRepository;
import com.uthwork.letbereal.security.UserSession;

@RestController
@RequestMapping(value = "/letbereal/")
public class CompanyPostRatingController {

	
	@Autowired
	CompanyPostRatingServiceImpl companyPostRatingServiceImpl;
	
	@Autowired
	CompanyPostRepository companyPostRepository;
	
	@Autowired
	CompanyPostRatingRepository companyPostRatingRepository;
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="saveCompanyPostRating",method=RequestMethod.POST)
	public @ResponseBody CompanyPostRatingResponse saveCompanyPostRating(Authentication principal, @RequestBody CompanyPostRatingRequest companyPostRatingRequest)
	{
		CompanyPost companyPost=companyPostRepository.findByPostId(companyPostRatingRequest.getPostId());
		CompanyPostRatingResponse companyPostRatingResponse=new CompanyPostRatingResponse();
		if(companyPost==null)
		{
			companyPostRatingResponse.setPostIdValid(false);
			companyPostRatingResponse.setRatingValid(false);
			companyPostRatingResponse.setMessage("Post for the given postId does not exist");
		}
		else
		{
			CompanyPostRating companyPostRating=companyPostRatingRepository.findByPostId(companyPostRatingRequest.getPostId());
			if(companyPostRating==null)
				companyPostRatingResponse=companyPostRatingServiceImpl.saveCompanyPostRating(companyPost,principal,companyPostRatingRequest);
			else
				companyPostRatingResponse=editCompanyPostRating(principal,companyPostRatingRequest);
		}
		return companyPostRatingResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="editCompanyPostRating",method=RequestMethod.POST)
	public @ResponseBody CompanyPostRatingResponse editCompanyPostRating(Authentication principal, @RequestBody CompanyPostRatingRequest companyPostRatingRequest)
	{
		CompanyPostRating companyPostRating=companyPostRatingRepository.findByPostId(companyPostRatingRequest.getPostId());
		CompanyPostRatingResponse companyPostRatingResponse=new CompanyPostRatingResponse();
		if(companyPostRating==null)
		{
			companyPostRatingResponse.setPostIdValid(false);
			companyPostRatingResponse.setRatingValid(false);
			companyPostRatingResponse.setMessage("You have not rate this post before or this post does not exist");
		}
		else
		{
			companyPostRatingResponse=companyPostRatingServiceImpl.editCompanyPostRating(companyPostRating,companyPostRatingRequest);
		}
		return companyPostRatingResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="deleteCompanyPostRating/{postId}",method=RequestMethod.GET)
	public @ResponseBody CompanyPostRatingResponse deleteCompanyPostRating(@PathVariable("postId")long postId, Authentication principal)
	{
		UserSession us=(UserSession)principal.getPrincipal();
		CompanyPostRating companyPostRating=companyPostRatingRepository.findByPostId(postId);
		CompanyPostRatingResponse companyPostRatingResponse=new CompanyPostRatingResponse();
		if(companyPostRating==null)
		{
			companyPostRatingResponse.setPostIdValid(false);
			companyPostRatingResponse.setRatingValid(false);
			companyPostRatingResponse.setMessage("You have not rate this post before or this post does not exist");
		}
		else
		{
			if(companyPostRating.getUser().getUserId()==us.getProfileId())
			{
				companyPostRatingRepository.delete(companyPostRating);
				companyPostRatingResponse.setMessage("You have successfully deleted your rating for this post");
			}
			else
				companyPostRatingResponse.setMessage("You are not authorize to delete this rating");
		}
		return companyPostRatingResponse;
	}
}
