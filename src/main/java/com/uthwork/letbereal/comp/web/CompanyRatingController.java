package com.uthwork.letbereal.comp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uthwork.letbereal.comp.request.CompanyRatingRequest;
import com.uthwork.letbereal.comp.response.CompanyRatingResponse;
import com.uthwork.letbereal.comp.serviceImpl.CompanyRatingServiceImpl;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.CompanyRating;
import com.uthwork.letbereal.persistence.repository.CompanyRatingRepository;
import com.uthwork.letbereal.persistence.repository.CompanyRepository;

@RestController
@RequestMapping(value = "/letbereal/")
public class CompanyRatingController {
	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	CompanyRatingServiceImpl companyRatingServiceImpl;
	
	@Autowired
	CompanyRatingRepository companyRatingRepository;

	@CrossOrigin(origins="*")
	@RequestMapping(value="createCompanyRating",method=RequestMethod.POST)
	public @ResponseBody CompanyRatingResponse createCompanyRating(Authentication principal, @RequestBody CompanyRatingRequest companyRatingRequest)
	{
		Company company=companyRepository.findByCompanyName(companyRatingRequest.getCompanyName());
		CompanyRatingResponse companyRatingResponse= new CompanyRatingResponse();
		if(company==null)
			companyRatingResponse.setMessage("This company does not exist");
		else
		{
			CompanyRating companyRating=companyRatingRepository.findByCompanyId(company.getCompanyId());
			if(companyRating==null)
				companyRatingResponse=companyRatingServiceImpl.craeteCompanyRating(principal,company,companyRatingRequest);
			else
				companyRatingResponse.setMessage("You have already rate this company");
		}
		return companyRatingResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="editCompanyRating",method=RequestMethod.POST)
	public @ResponseBody CompanyRatingResponse editCompanyRating(Authentication principal, @RequestBody CompanyRatingRequest companyRatingRequest)
	{
		Company company=companyRepository.findByCompanyName(companyRatingRequest.getCompanyName());
		CompanyRatingResponse companyRatingResponse= new CompanyRatingResponse();
		if(company==null)
			companyRatingResponse.setMessage("This company does not exist");
		else
			companyRatingResponse=companyRatingServiceImpl.editCompanyRating(principal,company,companyRatingRequest);
		return companyRatingResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="deleteCompanyRating/{companyId}",method=RequestMethod.GET)
	public @ResponseBody CompanyRatingResponse deleteCompanyRating(@PathVariable("companyId")long companyId)
	{
		CompanyRating companyRating=companyRatingRepository.findByCompanyId(companyId);
		CompanyRatingResponse companyRatingResponse= new CompanyRatingResponse();
		if(companyRating==null)
			companyRatingResponse.setMessage("you have not rate this company before or this company does not exist");
		else
		{
			companyRatingRepository.delete(companyRating);
			companyRatingResponse.setMessage("You have succesfully deleted rating for this company");
		}
		return companyRatingResponse;
	}
}
