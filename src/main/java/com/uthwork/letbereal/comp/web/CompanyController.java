package com.uthwork.letbereal.comp.web;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uthwork.letbereal.comp.request.CompanyExistRequest;
import com.uthwork.letbereal.comp.request.CompanyRequest;
import com.uthwork.letbereal.comp.request.CompanyReviewRequest;
import com.uthwork.letbereal.comp.response.CompanyExistResponse;
import com.uthwork.letbereal.comp.response.CompanyResponse;
import com.uthwork.letbereal.comp.response.CompanyReviewResponse;
import com.uthwork.letbereal.comp.serviceImpl.CompanyServiceImpl;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.persistence.repository.CompanyRepository;
import com.uthwork.letbereal.security.UserSession;

@RestController
@RequestMapping(value = "/letbereal/")
public class CompanyController {
	
	@Autowired
	CompanyServiceImpl companyServiceImpl;
	
	@Autowired
	CompanyRepository companyRepository;

	@CrossOrigin(origins="*")
	@RequestMapping(value="createCompany",method=RequestMethod.POST)
	public @ResponseBody CompanyResponse createCompany(Authentication principal,@RequestBody CompanyRequest companyRequest) throws ParseException
	{
		CompanyResponse companyResponse= new CompanyResponse();
		UserSession us = (UserSession) principal.getPrincipal();
		Company company=companyRepository.findByCompanyEmailId(companyRequest.getCompanyEmailId());
		if(company!=null)
		{
			if(company.getCompanyName().equals(companyRequest.getCompanyName()))
			{
				companyResponse.setUserId(us.getProfileId());
				companyResponse.setCompanyId(company.getCompanyId());
				companyResponse.setMessage("Company with the given name is already exist");
			}
			else
			{
				companyResponse.setUserId(us.getProfileId());
				companyResponse.setCompanyId(company.getCompanyId());
				companyResponse.setMessage("Company with the given email id is already exist");
			}
		}
		else
		{
			company = companyRepository.findByCompanyName(companyRequest.getCompanyName());
			if(company!=null)
			{
				companyResponse.setUserId(us.getProfileId());
				companyResponse.setCompanyId(company.getCompanyId());
				companyResponse.setMessage("Company with the given name is already exist");
			}
			else
				companyResponse=companyServiceImpl.createCompany(principal, companyRequest);
		}
		return companyResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="isCompanyExist",method=RequestMethod.POST)
	public @ResponseBody CompanyExistResponse isCompoanyExist(@RequestBody CompanyExistRequest companyExistRequest)
	{
		Company company=companyRepository.findByCompanyEmailId(companyExistRequest.getCompanyEmailId());
		CompanyExistResponse companyExistResponse= new CompanyExistResponse();
		if(company!=null)
			companyExistResponse.setCompanyExist(true);
		return companyExistResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="editCompany",method=RequestMethod.POST)
	public @ResponseBody CompanyResponse editCompany(Authentication principal,@RequestBody CompanyRequest companyRequest) throws ParseException
	{
		Company company=companyRepository.findByCompanyName(companyRequest.getCompanyName());
		CompanyResponse companyResponse=new CompanyResponse();
		if(company==null)
			companyResponse.setMessage("This company does not exist(:");
		else
		{
			companyResponse=companyServiceImpl.editCompany(principal, companyRequest);
		}
		return companyResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="deleteCompany",method=RequestMethod.POST)
	public @ResponseBody CompanyResponse deleteCompany(Authentication principal,@RequestBody CompanyRequest companyRequest)
	{
		
		Company company=companyRepository.findByCompanyName(companyRequest.getCompanyName());
		CompanyResponse companyResponse=new CompanyResponse();
		if(company==null)
			companyResponse.setMessage("This company does not exist(:");
		else
		{
			companyRepository.delete(company);
			companyResponse.setMessage("You have successfully deleted this company");
		}
		return companyResponse;
	}
}
