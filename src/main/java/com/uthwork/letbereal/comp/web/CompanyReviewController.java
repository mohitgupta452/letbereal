package com.uthwork.letbereal.comp.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uthwork.letbereal.comp.request.CompanyReviewRequest;
import com.uthwork.letbereal.comp.response.CompanyReviewResponse;
import com.uthwork.letbereal.comp.serviceImpl.CompanyReviewServiceImpl;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.CompanyReview;
import com.uthwork.letbereal.persistence.repository.CompanyRepository;
import com.uthwork.letbereal.persistence.repository.CompanyReviewRepository;
import com.uthwork.letbereal.security.UserSession;

@RestController
@RequestMapping(value = "/letbereal/")
public class CompanyReviewController {

	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	CompanyReviewServiceImpl companyReviewServiceImpl;
	
	@Autowired
	CompanyReviewRepository companyReviewRepository;
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="createCompanyReview",method=RequestMethod.POST)
	public @ResponseBody CompanyReviewResponse companyReview(Authentication principal,@RequestBody CompanyReviewRequest CompanyReviewRequest)
	{
		Company company=companyRepository.findByCompanyName(CompanyReviewRequest.getCompanyName());
		CompanyReviewResponse companyReviewResponse= new CompanyReviewResponse();
		if(company==null)
		{
			companyReviewResponse.setReviewSubmitted(false);
			companyReviewResponse.setMessgae("This Company does not exist(");
		}
		else
		{
			companyReviewResponse = companyReviewServiceImpl.companyReview(company, principal, CompanyReviewRequest);
		}
		return companyReviewResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="editCompanyReview",method=RequestMethod.POST)
	public @ResponseBody CompanyReviewResponse editReview(Authentication principal,@RequestBody CompanyReviewRequest CompanyReviewRequest)
	{
		UserSession us=(UserSession)principal.getPrincipal();
		CompanyReview companyReview = companyReviewRepository.findReviewByReviewId(CompanyReviewRequest.getReviewId());
		CompanyReviewResponse companyReviewResponse= new CompanyReviewResponse();
		if(companyReview==null)
			companyReviewResponse.setMessgae("This review does not exist (:");
		else
		{
			if(companyReview.getUser().getUserId()!=us.getProfileId())
				companyReviewResponse.setMessgae("You are not authorize to edit this review");
			else
			{
				companyReviewResponse=companyReviewServiceImpl.editReview(principal,CompanyReviewRequest);
			}
		}
		return companyReviewResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="deleteCompanyReview",method=RequestMethod.POST)
	public @ResponseBody CompanyReviewResponse deleteCompanyReview(Authentication principal, @RequestBody CompanyReviewRequest companyReviewRequest)
	{
		CompanyReview companyReview = companyReviewRepository.findReviewByReviewId(companyReviewRequest.getReviewId());
		UserSession us=(UserSession)principal.getPrincipal();
		CompanyReviewResponse companyReviewResponse= new CompanyReviewResponse();
		if(companyReview==null)
		{
			companyReviewResponse.setMessgae("This review does not exist");
		}
		else
		{
			if(companyReview.getUser().getUserId()!=us.getProfileId())
				companyReviewResponse.setMessgae("You are not authorize to delete this review");
			else
			{	
				companyReviewRepository.delete(companyReview);
				companyReviewResponse.setMessgae("review deleted succesfully");
			}
		}
		return companyReviewResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="getAllCompanyReview/{companyId}",method=RequestMethod.GET)
	public @ResponseBody List<CompanyReview> getAllCompanyReview(@PathVariable("companyId")long companyId)
	{
		Company company=companyRepository.findByCompanyId(companyId);
		List<CompanyReview> listOfCompanyReview=null;
		if(company!=null)
			listOfCompanyReview=companyReviewRepository.findAllReviewByCompanyId(companyId);
		return listOfCompanyReview;
	}
}
