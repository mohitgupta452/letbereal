package com.uthwork.letbereal.comp.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uthwork.letbereal.comp.request.CompanyPostRequest;
import com.uthwork.letbereal.comp.response.CompanyPostResponse;
import com.uthwork.letbereal.comp.serviceImpl.CompanyPostServiceImpl;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.CompanyPost;
import com.uthwork.letbereal.persistence.repository.CompanyPostRepository;
import com.uthwork.letbereal.persistence.repository.CompanyRepository;

@RestController
@RequestMapping(value = "/letbereal/")
public class CompanyPostController {

	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	CompanyPostRepository companyPostRepository;
	
	@Autowired
	CompanyPostServiceImpl companyPostServiceImpl;
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="saveCompanyPost",method=RequestMethod.POST)
	public @ResponseBody CompanyPostResponse saveCompanyPost(Authentication principal, @RequestBody CompanyPostRequest companyPostRequest)
	{
		Company company=companyRepository.findByCompanyId(companyPostRequest.getcompanyId());
		CompanyPostResponse CompanyPostResponse=new CompanyPostResponse();
		if(company==null)
		{
			CompanyPostResponse.setPostValid(false);
			CompanyPostResponse.setMessage("This company for given companyId does not exist");
		}
		else
			CompanyPostResponse=companyPostServiceImpl.saveCompanyPost(principal,companyPostRequest);
		return CompanyPostResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="editCompanyPost",method=RequestMethod.POST)
	public @ResponseBody CompanyPostResponse editCompanyPost(Authentication principal, @RequestBody CompanyPostRequest companyPostRequest)
	{
		CompanyPost companyPost=companyPostRepository.findByPostId(companyPostRequest.getcompanyPostId());
		CompanyPostResponse CompanyPostResponse=new CompanyPostResponse();
		if(companyPost==null)
		{
			CompanyPostResponse.setPostValid(false);
			CompanyPostResponse.setMessage("Post for given postId does not exist");
		}
		else
			CompanyPostResponse=companyPostServiceImpl.editCompanyPostByPostId(companyPostRequest);
		return CompanyPostResponse;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="deleteCompanyPost",method=RequestMethod.POST)
	public @ResponseBody CompanyPostResponse deleteCompanyPost(Authentication principal, @RequestBody CompanyPostRequest companyPostRequest)
	{
		CompanyPost companyPost=companyPostRepository.findByPostId(companyPostRequest.getcompanyPostId());
		CompanyPostResponse CompanyPostResponse=new CompanyPostResponse();
		if(companyPost==null)
		{
			CompanyPostResponse.setMessage("Post for given postId does not exist");
		}
		else
		{
			companyPostRepository.delete(companyPost);
			CompanyPostResponse.setMessage("You have successfully deleted this post");
		}
		return CompanyPostResponse;
	}
	
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="getAllCompanyPost/{companyId}",method=RequestMethod.GET)
	public @ResponseBody List<CompanyPost> getAllCompanyPostByCompanyId(@PathVariable("companyId") Long companyId)
	{
		Company company=companyRepository.findByCompanyId(companyId);
		List<CompanyPost> listOfCompanyPost=null;
		if(company!=null)
			listOfCompanyPost= companyPostRepository.findAllCompanyPostByCompanyId(companyId);
		return listOfCompanyPost;
	}
}
