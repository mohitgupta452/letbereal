package com.uthwork.letbereal.comp.serviceImpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.comp.request.CompanyReviewRequest;
import com.uthwork.letbereal.comp.response.CompanyReviewResponse;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.CompanyReview;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.CompanyReviewRepository;
import com.uthwork.letbereal.security.UserSession;

@Service
public class CompanyReviewServiceImpl {
	
	@Autowired
	CompanyReviewRepository companyReviewRepository;

	public CompanyReviewResponse companyReview(Company company, Authentication principal, CompanyReviewRequest companyReviewRequest)
	{
		UserSession us=(UserSession)principal.getPrincipal();
		User user=new User();
		user.setUserId(us.getProfileId());
		CompanyReview newReview= new CompanyReview();
		newReview.setUser(user);
		newReview.setCompany(company);
		newReview.setCreatedTime(new Date());
		newReview.setReview(companyReviewRequest.getReview());
		CompanyReview companyReview = companyReviewRepository.save(newReview);
		CompanyReviewResponse companyReviewResponse=new CompanyReviewResponse();
		companyReviewResponse.setReviewId(companyReview.getReviewId());
		companyReviewResponse.setReviewSubmitted(true);
		companyReviewResponse.setMessgae("Your review has been submitted successfully");
		return companyReviewResponse;
	}

	public CompanyReviewResponse editReview(Authentication principal, CompanyReviewRequest companyReviewRequest)
	{
		CompanyReview companyReview = companyReviewRepository.findReviewByReviewId(companyReviewRequest.getReviewId());
		companyReview.setReview(companyReviewRequest.getReview());
		companyReviewRepository.save(companyReview);
		CompanyReviewResponse companyReviewResponse=new CompanyReviewResponse();
		companyReviewResponse.setReviewId(companyReview.getReviewId());
		companyReviewResponse.setReviewSubmitted(true);
		companyReviewResponse.setMessgae("Your review has been edited successfully");
		return companyReviewResponse;
	}
}
