package com.uthwork.letbereal.comp.serviceImpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.comp.request.CompanyPostCommentRequest;
import com.uthwork.letbereal.comp.response.CompanyPostCommentResponse;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.CompanyPost;
import com.uthwork.letbereal.model.CompanyPostComment;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.CompanyPostCommentRepository;
import com.uthwork.letbereal.security.UserSession;

@Service
public class CompanyPostCommentServiceImpl {
	
	@Autowired
	CompanyPostCommentRepository companyPostCommentRepository;

	public CompanyPostCommentResponse saveCompanyPostComment(Authentication principal, Company company, CompanyPostCommentRequest companyPostCommentRequest)
	{
		UserSession us=(UserSession)principal.getPrincipal();
		User user=new User();
		user.setUserId(us.getProfileId());
		CompanyPost companyPost=new CompanyPost();
		companyPost.setPostId(companyPostCommentRequest.getPostId());
		CompanyPostComment companyPostComment=new CompanyPostComment();
		companyPostComment.setCompanyPost(companyPost);
		companyPostComment.setUser(user);
		companyPostComment.setCompany(company);
		companyPostComment.setComment(companyPostCommentRequest.getComment());
		companyPostComment.setCreatedTime(new Date());
		companyPostComment=companyPostCommentRepository.save(companyPostComment);
		CompanyPostCommentResponse companyPostCommentResponse= new CompanyPostCommentResponse();
		companyPostCommentResponse.setCommentId(companyPostComment.getCommentId());
		companyPostCommentResponse.setPostValid(true);
		companyPostCommentResponse.setCommentValid(true);
		companyPostCommentResponse.setMessage("You have succesfully submited your comment for the given postId");
		return companyPostCommentResponse;
	}
	
	public CompanyPostCommentResponse editCompanyPostComment(CompanyPostComment companyPostComment, CompanyPostCommentRequest companyPostCommentRequest)
	{
		companyPostComment.setComment(companyPostCommentRequest.getComment());
		companyPostComment.setUpdatedTime(new Date());
		companyPostComment=companyPostCommentRepository.save(companyPostComment);
		CompanyPostCommentResponse companyPostCommentResponse= new CompanyPostCommentResponse();
		companyPostCommentResponse.setCommentId(companyPostComment.getCommentId());
		companyPostCommentResponse.setPostValid(true);
		companyPostCommentResponse.setCommentValid(true);
		companyPostCommentResponse.setMessage("You have succesfully edited your comment for the given commentId");
		return companyPostCommentResponse;
	}
}
