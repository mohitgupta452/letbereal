package com.uthwork.letbereal.comp.serviceImpl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uthwork.letbereal.comp.request.CompanyRequest;
import com.uthwork.letbereal.comp.response.CompanyResponse;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.ImageUrl;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.CompanyRepository;
import com.uthwork.letbereal.persistence.repository.ImageUrlRepository;
import com.uthwork.letbereal.security.UserSession;

@Service
public class CompanyServiceImpl {

	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	private ImageUrlRepository imageUrlRepository;
	
	public CompanyResponse createCompany(Authentication principal,CompanyRequest companyRequest) throws ParseException
	{
		UserSession us = (UserSession) principal.getPrincipal();
		User user=new User();
		user.setUserId(us.getProfileId());
		Company newCompany = new Company();
		newCompany.setUser(user);
		newCompany.setCompanyName(companyRequest.getCompanyName());
		newCompany.setCompanySalg(companyRequest.getCompanySalg());
		newCompany.setOwnerName(companyRequest.getOwnerName());
		
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date companyStartDate=format.parse(companyRequest.getCompanyStartDate());
		
		newCompany.setCompanyStartDate(companyStartDate);
		newCompany.setNoOfEmployees(companyRequest.getNoOfEmployees());
		newCompany.setCompanyContactNo(companyRequest.getCompanyContactNo());
		newCompany.setCompanyEmailId(companyRequest.getCompanyEmailId());
		newCompany.setCompanyImageUrl(companyRequest.getCompanyImageUrl());
		newCompany.setCompanyCoverUrl(companyRequest.getCompanyCoverUrl());
		
		Company company=companyRepository.save(newCompany);
		System.out.println("companyId "+company.getCompanyId());
		
		CompanyResponse companyResponse= new CompanyResponse();
		companyResponse.setUserId(us.getProfileId());
		companyResponse.setCompanyId(company.getCompanyId());
		companyResponse.setMessage("Company created successfully");
		return companyResponse;
	}
	
	public CompanyResponse editCompany(Authentication principal, CompanyRequest companyRequest) throws ParseException
	{
		UserSession us = (UserSession) principal.getPrincipal();
		Company company=companyRepository.findByCompanyId(companyRequest.getCompanyId());
		CompanyResponse companyResponse= new CompanyResponse();
		if(company.getUser().getUserId()!=us.getProfileId())
		{
			companyResponse.setMessage("You are not authorize to edit the details of this company");
		}
		else
		{
			company.setCompanyName(companyRequest.getCompanyName());
			company.setCompanySalg(companyRequest.getCompanySalg());
			company.setOwnerName(companyRequest.getOwnerName());
		
			SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			Date companyStartDate=format.parse(companyRequest.getCompanyStartDate());
		
			company.setCompanyStartDate(companyStartDate);
			company.setNoOfEmployees(companyRequest.getNoOfEmployees());
			company.setCompanyContactNo(companyRequest.getCompanyContactNo());
			company.setCompanyEmailId(companyRequest.getCompanyEmailId());
			company.setCompanyImageUrl(companyRequest.getCompanyImageUrl());
			company.setCompanyCoverUrl(companyRequest.getCompanyCoverUrl());
			companyRepository.save(company);
			companyResponse.setUserId(us.getProfileId());
			companyResponse.setCompanyId(company.getCompanyId());
			companyResponse.setMessage("Company details edited successfully :)");
		}
		return companyResponse;
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public Long saveCompanyProfileImage(byte[] file, long profileId, String path) {
		 
		int returnValue = 0;
		if(!file.equals(null)){
			returnValue=companyRepository.updateCompanyProfileImage(path,profileId);
		}
		if(returnValue!=0){
			try {
				BufferedImage img = ImageIO.read(new ByteArrayInputStream(file));
		         ImageIO.write(img, "png", new File(path));
				//fileOutputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
 
		long result = returnValue;
		return result;

	}
	
	@Transactional(rollbackFor = Exception.class)
	public Long saveCompanyCoverPhoto(byte[] file, long profileId, String path) {
		 
		int returnValue = 0;
		if(!file.equals(null)){
			returnValue=companyRepository.updateCompanyCoverPhoto(path,profileId);
		}
		if(returnValue!=0){
			try {
				BufferedImage img = ImageIO.read(new ByteArrayInputStream(file));
		         ImageIO.write(img, "png", new File(path));
				//fileOutputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
 
		long result = returnValue;
		return result;

	}
	
	
	public Long saveCompanyPostImage(byte[] file, long companyId, String path) {

		try {
			BufferedImage img = ImageIO.read(new ByteArrayInputStream(file));
	         ImageIO.write(img, "png", new File(path));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		ImageUrl imageUrl = new ImageUrl();
		imageUrl.setImageUrl(path);
		imageUrl = imageUrlRepository.save(imageUrl);
		return imageUrl.getImageUrlId();

	}
}
