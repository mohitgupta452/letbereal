package com.uthwork.letbereal.comp.serviceImpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.comp.request.CompanyPostRatingRequest;
import com.uthwork.letbereal.comp.response.CompanyPostRatingResponse;
import com.uthwork.letbereal.model.CompanyPost;
import com.uthwork.letbereal.model.CompanyPostRating;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.CompanyPostRatingRepository;
import com.uthwork.letbereal.persistence.repository.CompanyPostRepository;
import com.uthwork.letbereal.security.UserSession;

@Service
public class CompanyPostRatingServiceImpl {

	@Autowired
	CompanyPostRepository companyPostRepository;
	
	@Autowired
	CompanyPostRatingRepository companyPostRatingRepository;
	
	public CompanyPostRatingResponse saveCompanyPostRating(CompanyPost companyPost, Authentication principal,CompanyPostRatingRequest companyPostRatingRequest)
	{
		UserSession us=(UserSession)principal.getPrincipal();
		User user=new User();
		user.setUserId(us.getProfileId());
		CompanyPostRating companyPostRating=new CompanyPostRating();
		companyPostRating.setCompanyPost(companyPost);
		companyPostRating.setUser(user);
		companyPostRating.setCompany(companyPost.getCompany());
		companyPostRating.setRating(companyPostRatingRequest.getRating());
		companyPostRating.setCreatedTime(new Date());
		companyPostRatingRepository.save(companyPostRating);
		CompanyPostRatingResponse companyPostRatingResponse=new CompanyPostRatingResponse();
		companyPostRatingResponse.setPostIdValid(true);
		companyPostRatingResponse.setRatingValid(true);
		companyPostRatingResponse.setMessage("You have successfully rate this post");
		return companyPostRatingResponse;
	}
	
	public CompanyPostRatingResponse editCompanyPostRating(CompanyPostRating companyPostRating, CompanyPostRatingRequest companyPostRatingRequest)
	{
		companyPostRating.setRating(companyPostRatingRequest.getRating());
		companyPostRating.setUpdatedTime(new Date());
		companyPostRatingRepository.save(companyPostRating);
		CompanyPostRatingResponse companyPostRatingResponse=new CompanyPostRatingResponse();
		companyPostRatingResponse.setPostIdValid(true);
		companyPostRatingResponse.setRatingValid(true);
		companyPostRatingResponse.setMessage("You have successfully edited your rating for this post");
		return companyPostRatingResponse;
	}
}
