package com.uthwork.letbereal.comp.serviceImpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.comp.request.CompanyPostRequest;
import com.uthwork.letbereal.comp.response.CompanyPostResponse;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.CompanyPost;
import com.uthwork.letbereal.persistence.repository.CompanyPostRepository;

@Service
public class CompanyPostServiceImpl {
	
	
	@Autowired
	CompanyPostRepository companyPostRepository;

	public CompanyPostResponse saveCompanyPost(Authentication principal, CompanyPostRequest companyPostRequest)
	{
		Company company=new Company();
		company.setCompanyId(companyPostRequest.getcompanyId());
		CompanyPost companyPost=new CompanyPost();
		companyPost.setCompany(company);
		companyPost.setPostTitle(companyPostRequest.getTitle());
		companyPost.setPostDescription(companyPostRequest.getDescription());
		companyPost.setCreatedTime(new Date());
		companyPost.setImageUrlId(companyPostRequest.getImageUrlId());
		companyPost=companyPostRepository.save(companyPost);
		CompanyPostResponse CompanyPostResponse=new CompanyPostResponse();
		CompanyPostResponse.setCompanyPostId(companyPost.getPostId());
		CompanyPostResponse.setPostValid(true);
		CompanyPostResponse.setMessage("Your post has been submitted succesfully");
		return CompanyPostResponse;
	}

	public CompanyPostResponse editCompanyPostByPostId(CompanyPostRequest companyPostRequest)
	{
		CompanyPost companyPost=companyPostRepository.findByPostId(companyPostRequest.getcompanyPostId());
		companyPost.setPostTitle(companyPostRequest.getTitle());
		companyPost.setPostDescription(companyPostRequest.getDescription());
		companyPost.setImageUrlId(companyPostRequest.getImageUrlId());
		CompanyPostResponse companyPostResponse=new CompanyPostResponse();
		companyPostRepository.save(companyPost);
		companyPostResponse.setCompanyPostId(companyPostRequest.getcompanyPostId());
		companyPostResponse.setMessage("You have successfully edited this post");
		companyPostResponse.setPostValid(true);
		return companyPostResponse;
	}
}
