package com.uthwork.letbereal.comp.serviceImpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.comp.request.CompanyRatingRequest;
import com.uthwork.letbereal.comp.response.CompanyRatingResponse;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.CompanyRating;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.CompanyRatingRepository;
import com.uthwork.letbereal.persistence.repository.CompanyRepository;
import com.uthwork.letbereal.security.UserSession;

@Service
public class CompanyRatingServiceImpl {
	
	@Autowired
	CompanyRatingRepository companyRatingRepository;
	
	@Autowired
	CompanyRepository companyRepository;

	public CompanyRatingResponse craeteCompanyRating(Authentication principal, Company company,CompanyRatingRequest companyRatingRequest)
	{
		UserSession us=(UserSession)principal.getPrincipal();
		User user=new User();
		user.setUserId(us.getProfileId());
		CompanyRating newCompanyRating=new CompanyRating();
		newCompanyRating.setCompany(company);
		newCompanyRating.setUser(user);
		newCompanyRating.setRating(companyRatingRequest.getRating());
		newCompanyRating.setCreatedTime(new Date());
		companyRatingRepository.save(newCompanyRating);
		CompanyRatingResponse companyRatingResponse=new CompanyRatingResponse();
		companyRatingResponse.setMessage("You have successfully rate this company");
		return companyRatingResponse;
	}
	
	public CompanyRatingResponse editCompanyRating(Authentication principal, Company company,CompanyRatingRequest companyRatingRequest)
	{
		CompanyRating companyRating=companyRatingRepository.findByCompanyId(company.getCompanyId());
		CompanyRatingResponse companyRatingResponse=new CompanyRatingResponse();
		if(companyRating==null)
			companyRatingResponse.setMessage("You have not rate this company before");
		else
		{
			companyRating.setRating(companyRatingRequest.getRating());
			companyRatingRepository.save(companyRating);
			companyRatingResponse.setMessage("You have successfully edited your rating for this company");
		}
		return companyRatingResponse;
	}
}
