package com.uthwork.letbereal.comp.response;

public class CompanyExistResponse {

	private boolean isCompanyExist;

	public boolean isCompanyExist() {
		return isCompanyExist;
	}

	public void setCompanyExist(boolean isCompanyExist) {
		this.isCompanyExist = isCompanyExist;
	}
	
	
}
