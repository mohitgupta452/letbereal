package com.uthwork.letbereal.comp.response;

public class CompanyReviewResponse {
	private Long reviewId;
	private boolean isReviewSubmitted;
	private String messgae;

	public Long getReviewId() {
		return reviewId;
	}

	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}

	public boolean isReviewSubmitted() {
		return isReviewSubmitted;
	}

	public void setReviewSubmitted(boolean isReviewSubmitted) {
		this.isReviewSubmitted = isReviewSubmitted;
	}

	public String getMessgae() {
		return messgae;
	}

	public void setMessgae(String messgae) {
		this.messgae = messgae;
	}

}
