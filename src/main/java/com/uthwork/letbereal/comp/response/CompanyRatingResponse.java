package com.uthwork.letbereal.comp.response;

public class CompanyRatingResponse {

	private long companyId;
	private String message;
	
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
