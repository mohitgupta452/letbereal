package com.uthwork.letbereal.comp.response;

public class CompanyPostRatingResponse {

	private boolean isPostIdValid;
    private boolean isRatingValid;
    private String message;
    
	public boolean isPostIdValid() {
		return isPostIdValid;
	}
	public void setPostIdValid(boolean isPostIdValid) {
		this.isPostIdValid = isPostIdValid;
	}
	public boolean isRatingValid() {
		return isRatingValid;
	}
	public void setRatingValid(boolean isRatingValid) {
		this.isRatingValid = isRatingValid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
