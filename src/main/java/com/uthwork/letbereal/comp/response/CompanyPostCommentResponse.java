package com.uthwork.letbereal.comp.response;

public class CompanyPostCommentResponse {

	private long commentId;
	private boolean isPostValid;
	private boolean isCommentValid;
	private String message;
	
	public long getCommentId() {
		return commentId;
	}
	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}
	public boolean isPostValid() {
		return isPostValid;
	}
	public void setPostValid(boolean isPostValid) {
		this.isPostValid = isPostValid;
	}
	public boolean isCommentValid() {
		return isCommentValid;
	}
	public void setCommentValid(boolean isCommentValid) {
		this.isCommentValid = isCommentValid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
