package com.uthwork.letbereal.comp.response;

public class CompanyPostResponse {

	private Long companyPostId;
	private boolean isPostValid;
	private String message;
	
	public Long getCompanyPostId() {
		return companyPostId;
	}
	public void setCompanyPostId(Long companyPostId) {
		this.companyPostId = companyPostId;
	}
	public boolean isPostValid() {
		return isPostValid;
	}
	public void setPostValid(boolean isPostValid) {
		this.isPostValid = isPostValid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
