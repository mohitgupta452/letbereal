package com.uthwork.letbereal.followers.service;

import java.util.List;

import com.uthwork.letbereal.followers.domain.request.FollowersRequest;
import com.uthwork.letbereal.followers.domain.response.FollowersResponse;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.model.UserFollowers;
import com.uthwork.letbereal.uma.domain.response.UserDetail;

public interface FollowersService {

public UserFollowers saveFollower(FollowersRequest followersRequest);

public List<UserDetail> getUserFollowersListByUserID(long userID);

public UserFollowers deleteFollower(FollowersRequest followersRequest);

public long getNumberOfFollowers(long userID);

public List<UserDetail> getFollowingsList(long userID);

public long getNumberOfFollowings(long userID);

}
