package com.uthwork.letbereal.followers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.followers.domain.request.FollowersRequest;
import com.uthwork.letbereal.followers.domain.response.FollowersResponse;
import com.uthwork.letbereal.followers.service.FollowersService;
import com.uthwork.letbereal.model.Skill;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.model.UserFollowers;
import com.uthwork.letbereal.model.UserFollowersId;
import com.uthwork.letbereal.persistence.repository.FollowersRepository;
import com.uthwork.letbereal.uma.domain.response.UserDetail;

@Service("followersService")
public class FollowersServiceImpl implements FollowersService{
@Autowired
FollowersRepository followersRepository;
	
	@Override
	public UserFollowers saveFollower(FollowersRequest followersRequest) {
		
		UserFollowers userFollower=new UserFollowers();
		UserFollowersId userFollowersId=new UserFollowersId();
		userFollowersId.setFollowersUserId(followersRequest.getFollowersUserId());
		userFollowersId.setFollowingUserId(followersRequest.getFollowingUserId());
		System.out.println("follower=="+userFollowersId.getFollowersUserId());
		System.out.println("following =="+userFollowersId.getFollowingUserId());
		userFollower.setId(userFollowersId);
		
		userFollower= followersRepository.save(userFollower);
		
		return userFollower;
	}

	@Override
	public List<UserDetail> getUserFollowersListByUserID(long userID) {
		List<UserDetail> usersDetailList=new ArrayList<UserDetail>();
		List<User> userResponseList=new ArrayList<User>();
		
		userResponseList=followersRepository.getUserFollowersListByUserID(userID);
		if(userResponseList.size()>0){
			for(User user:userResponseList){
				UserDetail userDetail= new UserDetail();
				userDetail.setUserId(user.getUserId());
				userDetail.setEmail(user.getEmail());

				userDetail.setFirstName(user.getFirstName());
				userDetail.setLastName(user.getLastName());
				userDetail.setGender(user.getGender());
				userDetail.setTelephone(user.getTelephone());
				userDetail.setFacebookId(user.getFacebookId());
				userDetail.setGooglePlusId(user.getGooglePlusId());
				userDetail.setFacebookImageUrl(user.getFacebookImageUrl());
				userDetail.setImageUrl(user.getImageUrl());
				userDetail.setLocation(user.getLocation());
				userDetail.setProfileVisibility(user.getProfileVisibility());
				userDetail.setPublIcKey(user.getPublIcKey());
				userDetail.setPrivateKey(user.getPrivateKey());
				usersDetailList.add(userDetail);
			}
		}

		return usersDetailList;
	}

	@Override
	public UserFollowers deleteFollower(FollowersRequest followersRequest) {
		
		UserFollowers userFollower=new UserFollowers();
		UserFollowersId userFollowersId=new UserFollowersId();
		userFollowersId.setFollowersUserId(followersRequest.getFollowersUserId());
		userFollowersId.setFollowingUserId(followersRequest.getFollowingUserId());
		userFollower.setId(userFollowersId);
		
		followersRepository.delete(userFollowersId);;
		
		return userFollower;
	}

	@Override
	public long getNumberOfFollowers(long userID) {
		long count=followersRepository.getNumberOfFollowers(userID);
		return count;
	}

	@Override
	public List<UserDetail> getFollowingsList(long userID) {

			List<UserDetail> usersDetailList=new ArrayList<UserDetail>();
			List<User> userResponseList=new ArrayList<User>();
			userResponseList=followersRepository.getFollowingsList(userID);
			 
			if(userResponseList.size()>0){
				for(User user:userResponseList){
					UserDetail userDetail= new UserDetail();
					userDetail.setUserId(user.getUserId());
					userDetail.setEmail(user.getEmail());

					userDetail.setFirstName(user.getFirstName());
					userDetail.setLastName(user.getLastName());
					userDetail.setGender(user.getGender());
					userDetail.setTelephone(user.getTelephone());
					userDetail.setFacebookId(user.getFacebookId());
					userDetail.setGooglePlusId(user.getGooglePlusId());
					userDetail.setFacebookImageUrl(user.getFacebookImageUrl());
					userDetail.setImageUrl(user.getImageUrl());
					userDetail.setLocation(user.getLocation());
					userDetail.setProfileVisibility(user.getProfileVisibility());
					userDetail.setPublIcKey(user.getPublIcKey());
					userDetail.setPrivateKey(user.getPrivateKey());
					usersDetailList.add(userDetail);
				}
			}

			return usersDetailList;
		}

	@Override
	public long getNumberOfFollowings(long userID) {
		long count=followersRepository.getNumberOfFollowings(userID);
		return count;
	}
 

}
