package com.uthwork.letbereal.followers.domain.response;

public class FollowersResponse {
	
	boolean isfollowingUser_IdValid;

	public boolean isIsfollowingUser_IdValid() {
		return isfollowingUser_IdValid;
	}

	public void setIsfollowingUser_IdValid(boolean isfollowingUser_IdValid) {
		this.isfollowingUser_IdValid = isfollowingUser_IdValid;
	}
	
}
