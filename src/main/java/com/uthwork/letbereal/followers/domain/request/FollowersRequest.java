package com.uthwork.letbereal.followers.domain.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class FollowersRequest {
	
	private long followersUserId;
	@NotNull
	private long followingUserId;
	
	public long getFollowersUserId() {
		return followersUserId;
	}
	public void setFollowersUserId(long followersUserId) {
		this.followersUserId = followersUserId;
	}
	public long getFollowingUserId() {
		return followingUserId;
	}
	public void setFollowingUserId(long followingUserId) {
		this.followingUserId = followingUserId;
	}

}
