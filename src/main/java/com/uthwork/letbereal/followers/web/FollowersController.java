package com.uthwork.letbereal.followers.web;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uthwork.letbereal.followers.domain.request.FollowersRequest;
import com.uthwork.letbereal.followers.domain.response.FollowersResponse;
import com.uthwork.letbereal.followers.service.FollowersService;
import com.uthwork.letbereal.model.UserFollowers;
import com.uthwork.letbereal.security.UserSession;
import com.uthwork.letbereal.uma.domain.response.UserDetail;
import com.uthwork.letbereal.utilities.Count;

@RestController
@RequestMapping("/letbereal/")
public class FollowersController {

	@Autowired
	FollowersService followersService;

	/* saveFollower: This method is used to save follower detail. */
	@CrossOrigin(origins="*")
	@RequestMapping(value = "saveFollower", method = RequestMethod.POST)
	public FollowersResponse saveUserDetails(Authentication authentication, HttpServletResponse httpResponse,
			@RequestBody @Valid FollowersRequest followersRequest, BindingResult result)
					throws UnsupportedEncodingException, JsonProcessingException {
		
		FollowersResponse followingResponse = new FollowersResponse();
		
		 followingResponse.setIsfollowingUser_IdValid(true);
		 
		UserSession us = (UserSession) authentication.getPrincipal();
		Long userID = us.getProfileId();
		followersRequest.setFollowersUserId(userID);

		if (result.hasErrors() || followersRequest.getFollowingUserId() == userID) {

			if (result.hasFieldErrors("followingUserId") || followersRequest.getFollowingUserId() == userID) {
				followingResponse.setIsfollowingUser_IdValid(false);
				;

			} else {
				followingResponse.setIsfollowingUser_IdValid(true);
			}
			return followingResponse;

		} else {
			followersService.saveFollower(followersRequest);
		}

		return followingResponse;

	}

	/* deleteFollower: This method is used to delete follower detail. */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "deleteFollower", method = RequestMethod.POST)
	public FollowersResponse deleteFollower(Authentication authentication, HttpServletResponse httpResponse,
			@RequestBody @Valid FollowersRequest followersRequest, BindingResult result)
					throws UnsupportedEncodingException, JsonProcessingException {
		FollowersResponse followingResponse = new FollowersResponse();
		followingResponse.setIsfollowingUser_IdValid(true);
		UserSession us = (UserSession) authentication.getPrincipal();
		Long userID = us.getProfileId();
		followersRequest.setFollowersUserId(userID);
		if (result.hasErrors() || followersRequest.getFollowingUserId() == userID) {

			if (result.hasFieldErrors("followingUserId") || followersRequest.getFollowingUserId() == userID) {
				followingResponse.setIsfollowingUser_IdValid(false);
				;

			} else {
				followingResponse.setIsfollowingUser_IdValid(true);
			}
			return followingResponse;

		} else {
			followersService.deleteFollower(followersRequest);
		}

		return followingResponse;

	}

	/*
	 * getFollowersList: This method is used see the list all followers of this
	 * user.
	 */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "getFollowersList", method = RequestMethod.POST)
	public @ResponseBody List<UserDetail> getUserFollowersListByUserID(Authentication principal) {

		UserSession us = (UserSession) principal.getPrincipal();
		long userID = us.getProfileId();
		List<UserDetail> userResponse = followersService.getUserFollowersListByUserID(userID);
		return userResponse;
	}

	/*
	 * getFollowingsList: This method is used see the list all followers who is
	 * following by this user.
	 */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "getFollowingsList", method = RequestMethod.POST)
	public @ResponseBody List<UserDetail> getFollowingsList(Authentication principal) {

		UserSession us = (UserSession) principal.getPrincipal();
		long userID = us.getProfileId();
		List<UserDetail> userResponse = followersService.getFollowingsList(userID);
		return userResponse;
	}

	/*
	 * getNumberOfFollowers: This method is used see number of all followers who
	 * is follows this user.
	 */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "getNumberOfFollowers", method = RequestMethod.POST)
	public @ResponseBody Count getNumberOfFollowers(Authentication principal) {
		UserSession us = (UserSession) principal.getPrincipal();
		long userID = us.getProfileId();
		long numberOfcount = followersService.getNumberOfFollowers(userID);
		Count coun = new Count();
		coun.setCount(numberOfcount);
		return coun;
	}

	/*
	 * getNumberOfFollowings: This method is used see number of all followers
	 * who is following by this user.
	 */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "getNumberOfFollowings", method = RequestMethod.POST)
	public @ResponseBody Count getNumberOfFollowings(Authentication principal) {
		UserSession us = (UserSession) principal.getPrincipal();
		long userID = us.getProfileId();
		long numberOfcount = followersService.getNumberOfFollowings(userID);
		Count coun = new Count();
		coun.setCount(numberOfcount);
		return coun;
	}

}
