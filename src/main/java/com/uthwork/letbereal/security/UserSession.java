package com.uthwork.letbereal.security;

public class UserSession {

	private long profileId;

	public UserSession(long talentId) {
		this.profileId = talentId;
	}
	
	/** Deserialization constructor. */
	@SuppressWarnings("unused")
	private UserSession() {
	}
	
	public long getProfileId() {
		return profileId;
	}

}
