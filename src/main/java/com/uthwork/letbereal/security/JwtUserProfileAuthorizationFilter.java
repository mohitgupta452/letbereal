package com.uthwork.letbereal.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uthwork.letbereal.configuration.JwtConfiguration;

import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class JwtUserProfileAuthorizationFilter extends BasicAuthenticationFilter {

	JwtConfiguration jwtConfiguration;
	
    public JwtUserProfileAuthorizationFilter(JwtConfiguration jwtConfiguration) {
    	super(new AuthenticationManager() {
			public Authentication authenticate(Authentication authentication) throws AuthenticationException {
				return null;
			}
		});
        this.jwtConfiguration = jwtConfiguration;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(jwtConfiguration.headerName);
        if (header == null) {
            chain.doFilter(req, res);
            return;
        }
        UsernamePasswordAuthenticationToken authentication = getAuthentication(header);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserSession userSession = (UserSession) authentication.getPrincipal();
        // Refreshes the JWT token.
        JwtUserProfileAuthenticationFilter.addTokenToResponse(jwtConfiguration, res, userSession);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) throws IOException {
    	String subjectJson = Jwts.parser()
    			.setSigningKey(jwtConfiguration.secret.getBytes())
    			.parseClaimsJws(token)
    			.getBody()
    			.getSubject();
    	
    	UserSession userSession = new ObjectMapper().readValue(subjectJson, UserSession.class);
    	if (userSession != null) {
    		return new UsernamePasswordAuthenticationToken(userSession, null, Collections.emptyList());
    	}
    	return null;
    }
    
}