package com.uthwork.letbereal.security;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uthwork.letbereal.configuration.JwtConfiguration;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.security.UserProfileDetailsService.UserProfile;
import com.uthwork.letbereal.uma.domain.response.UserResponse;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUserProfileAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	public static class LoginProfileRequest {

		private String email;
		private String password;

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

	AuthenticationManager authenticationManager;
	JwtConfiguration jwtConfiguration;

	public JwtUserProfileAuthenticationFilter(AuthenticationManager authenticationManager,
											  JwtConfiguration jwtConfiguration) {
		this.authenticationManager = authenticationManager;
		this.jwtConfiguration = jwtConfiguration;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req,
			HttpServletResponse res) throws AuthenticationException {
		try {
			LoginProfileRequest loginRequest = new ObjectMapper()
					.readValue(req.getInputStream(), LoginProfileRequest.class);

			return authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							loginRequest.getEmail(),
							loginRequest.getPassword(),
							Collections.emptyList())
					);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req,
			HttpServletResponse res,
			FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		    User user = ((UserProfile) auth.getPrincipal()).getUser();
		UserSession userSession = new UserSession(user.getUserId());
		addTokenToResponse(jwtConfiguration, res, userSession);
	}

	public static void addTokenToResponse(JwtConfiguration jwtConfiguration, HttpServletResponse res, UserSession userSession) throws JsonProcessingException {
		LocalDateTime expirationTime = LocalDateTime.now().plusMinutes(jwtConfiguration.expirationTimeMin);
		String token = Jwts.builder()
				.setSubject(new ObjectMapper().writeValueAsString(userSession))
				.setExpiration(Date.from(expirationTime.atZone(ZoneId.systemDefault()).toInstant()))
				.signWith(SignatureAlgorithm.HS512, jwtConfiguration.secret.getBytes())
				.compact();
		res.addHeader(jwtConfiguration.headerName, token);
		String userId=String.valueOf(userSession.getProfileId());
		res.addHeader("userId", userId);
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Access-Control-Allow-Methods", "POST,PUT,GET,OPTIONS,DELETE");
		res.setHeader("Access-Control-Allow-Headers","Authorization,Content-Type,userId");
	}

}