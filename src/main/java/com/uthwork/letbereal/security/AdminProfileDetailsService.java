package com.uthwork.letbereal.security;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.UserRepository;
import com.uthwork.letbereal.security.UserProfileDetailsService.UserProfile;

@Service
public class AdminProfileDetailsService implements UserDetailsService {
	
		@SuppressWarnings("serial")
		public static class AdminProfile extends org.springframework.security.core.userdetails.User
		{
			private User user;
		
			public AdminProfile(User talent) {
				super(talent.getEmail(), talent.getPassword(), Arrays.asList(new SimpleGrantedAuthority(talent.getUserRole())));
				this.user = talent;
			}
			
			public User getUser() {
				return user;
			}

			public void setUser(User user) {
				this.user = user;
			}
		}
	
		@Autowired
		UserRepository userRepository;

		@Override
		public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
			User talent = userRepository.findByEmail(username);
			if (talent == null)
				throw new UsernameNotFoundException(username);		
			return new UserProfile(talent);
		}
}
