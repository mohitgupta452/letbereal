package com.uthwork.letbereal.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.UserRepository;

import java.util.Collections;

@Service
public class UserProfileDetailsService implements UserDetailsService {

	@SuppressWarnings("serial")
	public static class UserProfile extends org.springframework.security.core.userdetails.User {
		
		private User user;
		
		public UserProfile(User talent) {
			super(talent.getEmail(), talent.getPassword(), Collections.emptyList());
			this.user = talent;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
	}
	
	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User talent = userRepository.findByEmail(username);
		if (talent == null)
			throw new UsernameNotFoundException(username);		
		return new UserProfile(talent);
	}
}
