package com.uthwork.letbereal.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.uthwork.letbereal.configuration.JwtConfiguration;


@Configuration
public class SecurityConfig {
	
		@Autowired
		private AdminProfileDetailsService adminProfileDetailsService;
		
		
		@Configuration
		//@Order(1)
		public class AdminSecurityConfig extends WebSecurityConfigurerAdapter 
		{
				@Override
		        protected void configure(HttpSecurity http) throws Exception {
		         
		            http
		                .antMatcher("/admin/**")
		                
		                .authorizeRequests()
		                    .anyRequest().hasRole("admin").anyRequest()
		                    .authenticated()
		                    .and()
		                .formLogin()
		                    .loginPage("/admin/login")
		                    .defaultSuccessUrl("/admin/home")
		                    .permitAll()
		                    .and()
		                .logout()
		                    .logoutUrl("/special/logout")
		                    .permitAll();
		        
				}
		}
		
		@Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception 
		{
				BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();
				auth
                	.userDetailsService(adminProfileDetailsService).passwordEncoder(bCryptPasswordEncoder);
        }
		
		
		@Configuration
		@Order(1)
		public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
			
			  
			
			    @Bean
			    public PasswordEncoder passwordEncoder() {
			    return new BCryptPasswordEncoder();
				}
			
				@Autowired
				JwtConfiguration jwtConfiguration;
				
				@Autowired
				PasswordEncoder passwordEncoder;
				
				@Autowired
		        UserProfileDetailsService talentUserDetailsService;
				
				protected void configure(HttpSecurity http) throws Exception {
					http
					.antMatcher("/letbereal/**")
					.csrf().disable()
					.authorizeRequests()
					.antMatchers("/letbereal/saveUserDetails","/letbereal/isUserExist").permitAll()
					.anyRequest().authenticated()
					.and()
					.addFilter(authenticationFilter())
					.addFilter(new JwtUserProfileAuthorizationFilter(jwtConfiguration))
		            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
				}
		
				protected JwtUserProfileAuthenticationFilter authenticationFilter() throws Exception {
					JwtUserProfileAuthenticationFilter authenticationFilter = new JwtUserProfileAuthenticationFilter(authenticationManager(), jwtConfiguration);
					authenticationFilter.setFilterProcessesUrl("/letbereal/login");
					return authenticationFilter;
				}
				
				@Override
				protected void configure(AuthenticationManagerBuilder auth) throws Exception {
					auth.userDetailsService(talentUserDetailsService).passwordEncoder(passwordEncoder);
				}
				
				@Bean
				public AuthenticationManager authenticationManagerBean() throws Exception {
				    return super.authenticationManagerBean();
				}
		
		}

}