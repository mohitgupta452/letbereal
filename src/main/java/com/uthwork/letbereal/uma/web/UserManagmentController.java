package com.uthwork.letbereal.uma.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uthwork.letbereal.comp.serviceImpl.CompanyServiceImpl;
import com.uthwork.letbereal.configuration.JwtConfiguration;
import com.uthwork.letbereal.model.Company;
import com.uthwork.letbereal.model.ImageUrl;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.persistence.repository.CompanyRepository;
import com.uthwork.letbereal.security.JwtUserProfileAuthenticationFilter;
import com.uthwork.letbereal.security.UserSession;
import com.uthwork.letbereal.uma.domain.UploadImageType;
import com.uthwork.letbereal.uma.domain.request.SearchUserRequest;
import com.uthwork.letbereal.uma.domain.request.UserExistRequest;
import com.uthwork.letbereal.uma.domain.request.UserImageUploadRequest;
import com.uthwork.letbereal.uma.domain.request.UserRequest;
import com.uthwork.letbereal.uma.domain.response.SearchUserResponse;
import com.uthwork.letbereal.uma.domain.response.SkillsResponse;
import com.uthwork.letbereal.uma.domain.response.UserDetail;
import com.uthwork.letbereal.uma.domain.response.UserExistResponse;
import com.uthwork.letbereal.uma.domain.response.UserImageUploadResponse;
import com.uthwork.letbereal.uma.domain.response.UserProfileUploadResponse;
import com.uthwork.letbereal.uma.domain.response.UserResponse;
import com.uthwork.letbereal.uma.service.UserManagmentService;

@RestController
@RequestMapping(value = "/letbereal/")
public class UserManagmentController {

	@Autowired
	private UserManagmentService userManagmentService;

	@Autowired
	JwtConfiguration jwtConfiguration;
	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	CompanyServiceImpl companyServiceImpl;

	@Autowired
	private  Environment env;
	private static final String USER_PROFILE_IMAGE_DIR = "userprofile";
	private static final String POST_IMAGE_DIR = "post";
	private static final String IMAGE_TYPE_PNG = ".png";
	private static final String FILE_SEPARATOR = "/";
	private static final String COMPANY_PROFILE_IMAGE_DIR="companyProfile";
	private static final String COMPANY_COVER_IMAGE_DIR="companyCoverPhoto";
	private static final String COMPANY_POST_IMAGE_DIR="companyPost";

	/* saveUserDetails: This method is used to save User Detail only once */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "saveUserDetails", method = RequestMethod.POST)
	public @ResponseBody UserResponse saveUserDetails(HttpServletResponse httpResponse,
			@RequestBody @Valid UserRequest userRequest, BindingResult result)
			throws UnsupportedEncodingException, JsonProcessingException {
		UserResponse userResponse = new UserResponse();
		setTrueInUserResponse(userResponse);
		User userDetail = new User();

		if (result.hasErrors()) {
			userResponse = validateUserRequest(userResponse, result);
			return userResponse;
		} else {
			userDetail = userManagmentService.saveUserDetails(userRequest);
		}
		login(userDetail.getUserId(), httpResponse);
		userResponse.setUserId(userDetail.getUserId());
		return userResponse;
	}

	/*
	 * updateUserDetail: This method is used to Update User Detail after login
	 */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "updateUserDetail", method = RequestMethod.POST)
	public @ResponseBody UserDetail updateUserDetail(@RequestBody UserDetail userRequest, Authentication principal)
			throws UnsupportedEncodingException {

		UserSession us = (UserSession) principal.getPrincipal();
		userRequest.setUserId(us.getProfileId());

		UserDetail userResponse = userManagmentService.updateUserDetail(userRequest);

		return userResponse;
	}

	/*
	 * getUserDetailsList: This method is used to get all users list for "Admin"
	 */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "getUserDetailsList", method = RequestMethod.GET)
	public @ResponseBody List<SearchUserResponse> getUserDetailsList() {

		List<SearchUserResponse> userResponseList = userManagmentService.getUserDetailsList();

		return userResponseList;
	}
	
	
	/*
	 * getOtherUserDetailsByID: This method is used to get other User detail to dashboard
	 */
	@CrossOrigin(origins="*")
	@RequestMapping(value = "getOtherUserDetailsByID/{userId}", method = RequestMethod.GET)
	public @ResponseBody UserDetail getOtherUserDetailsByID(@PathVariable("userId") Long userId,Authentication principal) {
		 UserDetail userDetial = new UserDetail();
		userDetial.setUserId(userId);
		UserDetail userResponse = userManagmentService.getUserDetailsByID(userDetial);
		return userResponse;
	}
	
	/*
	 * getUserDetailsByID: This method is used to get User detail to dashboard
	 */
	@CrossOrigin(origins="*")
	@RequestMapping(value = "getUserDetailsByID", method = RequestMethod.POST)
	public @ResponseBody UserDetail getUserDetailsByID(Authentication principal) {
		UserDetail userDetial = new UserDetail();
		UserSession us = (UserSession) principal.getPrincipal();
		userDetial.setUserId(us.getProfileId());
		UserDetail userResponse = userManagmentService.getUserDetailsByID(userDetial);
		return userResponse;
	}

	/* searchUsersList: This method is used to search other user. */

	@CrossOrigin(origins="*")
	@RequestMapping(value = "searchUsersList", method = RequestMethod.POST)
	public @ResponseBody List<SearchUserResponse> searchUsersList(@RequestBody SearchUserRequest searchUserRequest) {

		List<SearchUserResponse> serachUserResponseList = userManagmentService.searchUsersList(searchUserRequest);

		return serachUserResponseList;
	}

	private UserResponse login(long talentId, HttpServletResponse httpResponse) throws JsonProcessingException {
		UserSession talentSession = new UserSession(talentId);
		JwtUserProfileAuthenticationFilter.addTokenToResponse(jwtConfiguration, httpResponse, talentSession);
		UserResponse userResponse=new UserResponse();
		userResponse.setUserId(talentId);
		return userResponse;
	}

	private void setTrueInUserResponse(UserResponse userResponse) {
		userResponse.setFacebook_IdValid(true);
		userResponse.setEmailValid(true);
		userResponse.setNameValid(true);
		userResponse.setImageUrlValid(true);
		userResponse.setGenderValid(true);

	}

	/*
	 * validateUserRequest: This method is used to validate user Request at the
	 * time of saving data.
	 */

	private UserResponse validateUserRequest(UserResponse userResponse, BindingResult result) {

		if (result.hasFieldErrors("facebook_id")) {
			userResponse.setFacebook_IdValid(false);
		}
		if (result.hasFieldErrors("email")) {
			userResponse.setEmailValid(false);
		}
		if (result.hasFieldErrors("name")) {
			userResponse.setNameValid(false);
		}
		if (result.hasFieldErrors("imageUrl")) {
			userResponse.setImageUrlValid(false);
		}
		if (result.hasFieldErrors("gender")) {
			userResponse.setGenderValid(false);
		}
		return userResponse;
	}

	/*
	 * isUserExist: This method is used to test user availability in our
	 * database at the time of saving new user data.
	 */

	@CrossOrigin(origins="*")
	@PostMapping("isUserExist")
	@ResponseBody
	public UserExistResponse isUserExist(@RequestBody @Valid UserExistRequest userExistRequest, BindingResult result) {
		UserExistResponse userExistResponse = new UserExistResponse();
		if (result.hasErrors()) {
			userExistResponse.setEmailValid(false);
			return userExistResponse;
		}
		userExistResponse.setEmailValid(true);
		userExistResponse.setUserExist(userManagmentService.isUserExist(userExistRequest.getEmail()));
		return userExistResponse;
	}

	/* uploadPhoto: This method is used to upload a new image. */


	
	@CrossOrigin(origins="*")
	@PostMapping("/uploadPhoto")
	public UserProfileUploadResponse uploadProfilePhoto(@RequestParam("file") MultipartFile file,
			Authentication authentication, @RequestParam("imageType") UploadImageType imageType,
			HttpServletRequest request) {
		UserProfileUploadResponse response = new UserProfileUploadResponse();
		if (file.isEmpty()) {
			response.setPhotoValid(false);
			return response;
		}
		response.setPhotoValid(true);
		if (imageType == null || imageType.name() == "") {
			response.setImageTypeValid(false);
			return response;
		}
		response.setImageTypeValid(true);
		UserSession userSession = (UserSession) authentication.getPrincipal();
		String path = env.getProperty("letbereal.image.user.profile");
		System.out.println("path==" + path);
		Long imageUrlId = null;
		
		if (UploadImageType.UserImage.equals(imageType)) {
			path = path + FILE_SEPARATOR + USER_PROFILE_IMAGE_DIR + FILE_SEPARATOR + USER_PROFILE_IMAGE_DIR + "_"
					+ IMAGE_TYPE_PNG;
			imageUrlId = userManagmentService.uploadUserPhoto(file, userSession.getProfileId(), path);
		} else if (UploadImageType.PostImage.equals(imageType)) {
			path = path + FILE_SEPARATOR + POST_IMAGE_DIR + FILE_SEPARATOR + POST_IMAGE_DIR + "_" + IMAGE_TYPE_PNG;
			imageUrlId = userManagmentService.uploadPostPhoto(file, userSession.getProfileId(), path);
		} else {
			response.setPhotoValid(false);
			return response;
		}

		if (imageUrlId == null) {
			response.setPhotoValid(false);
		}
		response.setImageUrlId(imageUrlId);
		return response;
	}

	/*
	 * image/view/{imageUrlId}: This method is used to view image by image id.
	 */

	@CrossOrigin(origins="*")
	@GetMapping("/image/view/{imageUrlId}")
	public void showImage(@PathVariable("imageUrlId") Long imageUrlId, HttpServletResponse response)
			throws IOException {
		ImageUrl imageUrl = userManagmentService.loadImageUrl(imageUrlId);
		response.setContentType(MediaType.IMAGE_PNG_VALUE);
		ServletOutputStream out;
		FileInputStream fin;
		BufferedInputStream bin;
		out = response.getOutputStream();
		BufferedOutputStream bout = new BufferedOutputStream(out);
		try {
			fin = new FileInputStream(imageUrl.getImageUrl());
			bin = new BufferedInputStream(fin);
			int ch = 0;
			;
			while ((ch = bin.read()) != -1) {
				bout.write(ch);
			}
			bin.close();
			fin.close();
			bout.close();
			out.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			bout.close();
			out.close();
		}
	}

	/* saveUserSkills: This method is used to save or update Skills. */

	@CrossOrigin(origins="*")
	@PostMapping(value = "saveUserSkills")
	public @ResponseBody List<Long> saveUserSkills(Authentication authentication, @RequestBody List<Long> userSkills)
			throws UnsupportedEncodingException {

		UserSession userSession = (UserSession) authentication.getPrincipal();

		long userID = userSession.getProfileId();
		List<Long> userSkillsResponse = userManagmentService.saveUserSkills(userSkills, userID);

		return userSkillsResponse;
	}

	/* getAllSkillsList: This method is used to get all list of skills . */

	@CrossOrigin(origins="*")
	@PostMapping(value = "getAllSkillsList")
	public @ResponseBody List<SkillsResponse> getAllSkillsList(Authentication principal) {
		List<SkillsResponse> getAllSkillsList = userManagmentService.getAllSkillsList();
		return getAllSkillsList;
	}
	/*file upload in base64 String  Start*/

	@CrossOrigin(origins="*")
	@RequestMapping(value = "uploadImage", method = RequestMethod.POST)
	public @ResponseBody UserImageUploadResponse uploadImage(@RequestBody UserImageUploadRequest userImageUploadRequest, Authentication principal)
			throws UnsupportedEncodingException, IOException {
			UserImageUploadResponse userImageUploadResponse=new UserImageUploadResponse();
			UserSession userSession = (UserSession) principal.getPrincipal();
		long userID =userSession.getProfileId();
		Long companyId=(long) 0;
		if(userImageUploadRequest.getCompanyName()!=null)
		{
			Company company=companyRepository.findByCompanyName(userImageUploadRequest.getCompanyName());
			if(company!=null)
				companyId=company.getCompanyId();
		}
		 userImageUploadResponse= uploadImage(userImageUploadRequest,userID,companyId);
		return userImageUploadResponse;
	}

	public static byte[] convertToImg(String base64) throws IOException  
    {  
         return Base64.decodeBase64(base64);  
    }  
 
    public  UserImageUploadResponse uploadImage(UserImageUploadRequest userImageUploadRequest ,long userID,long companyId)throws IOException{

    	UserImageUploadResponse response = new UserImageUploadResponse();
 		String base64=userImageUploadRequest.getImageString();
 		
 		byte[] file=convertToImg(base64);
 		
 		String path = env.getProperty("letbereal.image.user.profile");
 		
 		Long imageUrlId = null;
 		
 		if (UploadImageType.UserImage.equals(userImageUploadRequest.getImageType()))  {
 			
 			path = path + FILE_SEPARATOR + USER_PROFILE_IMAGE_DIR + FILE_SEPARATOR + USER_PROFILE_IMAGE_DIR + "_" + userID + "_"
 					+ IMAGE_TYPE_PNG;
 			imageUrlId = userManagmentService.saveUserProfileImage(file, userID, path);
 		} else if (UploadImageType.PostImage.equals(userImageUploadRequest.getImageType()))  {
 			
 			path = path + FILE_SEPARATOR + POST_IMAGE_DIR + FILE_SEPARATOR + POST_IMAGE_DIR + "_"+ userID +"_"+System.currentTimeMillis()+"_" + IMAGE_TYPE_PNG;
 			System.out.println("path from uploadImage()"+path+"userId"+userID+"file"+file);
 			imageUrlId = userManagmentService.saveUserPostImage(file, userID, path);
 		}
 		else if(UploadImageType.CompanyImage.equals(userImageUploadRequest.getImageType()))
 		{
 			path = path + FILE_SEPARATOR + COMPANY_PROFILE_IMAGE_DIR + FILE_SEPARATOR + COMPANY_PROFILE_IMAGE_DIR + "_" + companyId + "_"
 					+ IMAGE_TYPE_PNG;
 			imageUrlId=companyServiceImpl.saveCompanyProfileImage(file, companyId, path);
 		}
 		else if(UploadImageType.CompanyPostImage.equals(userImageUploadRequest.getImageType()))
 		{
 			path = path + FILE_SEPARATOR + COMPANY_POST_IMAGE_DIR + FILE_SEPARATOR + COMPANY_POST_IMAGE_DIR + "_"+ companyId +"_"+System.currentTimeMillis()+"_" + IMAGE_TYPE_PNG;
 			imageUrlId = companyServiceImpl.saveCompanyPostImage(file, companyId, path);
 		}
 		else if(UploadImageType.CompanyCoverPhoto.equals(userImageUploadRequest.getImageType()))
 		{
 			path = path + FILE_SEPARATOR + COMPANY_COVER_IMAGE_DIR + FILE_SEPARATOR + COMPANY_COVER_IMAGE_DIR + "_"+ companyId+"_" + IMAGE_TYPE_PNG;
 			imageUrlId = companyServiceImpl.saveCompanyCoverPhoto(file, companyId, path);
 		}
 	
 		response.setImageID(imageUrlId);
 	
 		return response; 
     }
    /*file upload in base64 String  END */
}