package com.uthwork.letbereal.uma.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.uthwork.letbereal.model.ImageUrl;
import com.uthwork.letbereal.model.Skill;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.model.UserSkills;
import com.uthwork.letbereal.model.UserSkillsId;
import com.uthwork.letbereal.persistence.repository.ImageUrlRepository;
import com.uthwork.letbereal.persistence.repository.ProfileRatingRepository;
import com.uthwork.letbereal.persistence.repository.SkillRepository;
import com.uthwork.letbereal.persistence.repository.UserRepository;
import com.uthwork.letbereal.persistence.repository.UserSkillRepository;
import com.uthwork.letbereal.uma.domain.request.SearchUserRequest;
import com.uthwork.letbereal.uma.domain.request.UserRequest;
import com.uthwork.letbereal.uma.domain.response.SearchUserResponse;
import com.uthwork.letbereal.uma.domain.response.SkillsResponse;
import com.uthwork.letbereal.uma.domain.response.UserDetail;
import com.uthwork.letbereal.uma.service.UserManagmentService;

 

@Service("userManagmentService")
public class UserManagmentServiceImpl implements UserManagmentService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserSkillRepository userSkillRepository;

	@Autowired
	SkillRepository skillRepository;
	@Autowired
	ProfileRatingRepository profileRatingRepository;

	@Autowired
	Environment env;

	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Autowired
	private ImageUrlRepository imageUrlRepository;

	@Override
	public User saveUserDetails(UserRequest userRequest) {

		
		
		
		User user = new User();
		user.setEmail(userRequest.getEmail());
		String inputPassword = userRequest.getFacebook_id();
		user.setPassword(passwordEncoder.encode(inputPassword));
		user.setFacebookId(passwordEncoder.encode(inputPassword));
		user.setImageUrl(userRequest.getImageUrl());
		String fullName = userRequest.getName();
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(fullName);
		boolean found = matcher.find();
		if(found==true){
			String[] name = fullName.split(" ");
			user.setFirstName(name[0]);
			user.setLastName(name[1]);
		}
		else{
			user.setFirstName(fullName);
		}

		user.setGender(userRequest.getGender());
		User userResponse = userRepository.save(user);
		return userResponse;
	}

	@Override
	public UserDetail updateUserDetail(UserDetail userDetailReq) {

		User existingUser = userRepository.findOne(userDetailReq.getUserId());

		existingUser.setFirstName(userDetailReq.getFirstName());
		existingUser.setLastName(userDetailReq.getLastName());
		existingUser.setGender(userDetailReq.getGender());
		existingUser.setTelephone(userDetailReq.getTelephone());
		existingUser.setFacebookImageUrl(userDetailReq.getFacebookImageUrl());
		existingUser.setImageUrl(userDetailReq.getImageUrl());
		existingUser.setImageUrlId(userDetailReq.getImageUrlID());
		existingUser.setLocation(userDetailReq.getLocation());
		existingUser.setProfileVisibility(userDetailReq.getProfileVisibility());
		existingUser.setPublIcKey(userDetailReq.getPublIcKey());
		existingUser.setPrivateKey(userDetailReq.getPrivateKey());

		existingUser = userRepository.save(existingUser);
		return userDetailReq;
	}

	@Override
	public UserDetail getUserDetailsByID(UserDetail userDetail) {

		User userResponse = userRepository.findOne(userDetail.getUserId());
		List<Skill> userSkillsList = userSkillRepository.getUserSkillsListByUserID(userResponse.getUserId());
		float rate =0;
		try{
			rate= profileRatingRepository.findAvgProfileRatingByProfileId(userDetail.getUserId());
} catch (NullPointerException npe){
	System.out.println(npe);
}
		
		finally{
		userDetail.setEmail(userResponse.getEmail());

		userDetail.setFirstName(userResponse.getFirstName());
		userDetail.setLastName(userResponse.getLastName());
		userDetail.setGender(userResponse.getGender());
		userDetail.setTelephone(userResponse.getTelephone());
		userDetail.setFacebookId(userResponse.getFacebookId());
		userDetail.setGooglePlusId(userResponse.getGooglePlusId());
		userDetail.setFacebookImageUrl(userResponse.getFacebookImageUrl());
		userDetail.setImageUrl(userResponse.getImageUrl());
		userDetail.setLocation(userResponse.getLocation());
		userDetail.setProfileVisibility(userResponse.getProfileVisibility());
		userDetail.setPublIcKey(userResponse.getPublIcKey());
		userDetail.setPrivateKey(userResponse.getPrivateKey());
		userDetail.setAvgRating(rate);
		List<SkillsResponse> skillsList = new ArrayList<>();
		for (Skill skill : userSkillsList) {
			SkillsResponse skillsResponse = new SkillsResponse();
			skillsResponse.setSkillId(skill.getSkillId());
			skillsResponse.setSkillDsc(skill.getSkillDsc());
			skillsResponse.setSkillCode(skill.getSkillCode());
			skillsResponse.setSkillLevel(skill.getSkillLevel());
			skillsList.add(skillsResponse);
		}
		userDetail.setSkills(skillsList);
		}
		return userDetail;
	}

	public List<SearchUserResponse> searchUsersList(SearchUserRequest searchUserRequest) {
		List<SearchUserResponse> searchUserResponseList = new ArrayList<SearchUserResponse>();

		List<User> userResponseList = null;

		if (searchUserRequest.getSearchBy().equals(searchUserRequest.getSearchBy().Skills)) {

			List<Long> skillsList = new ArrayList<Long>();
			skillsList.addAll(searchUserRequest.getSkillsList());
			userResponseList = userSkillRepository.getUserListBySkillsIDs(skillsList);

		}

		if (searchUserRequest.getSearchBy().equals(searchUserRequest.getSearchBy().Name)) {
			String name = searchUserRequest.getName();
			userResponseList = userRepository.searchUsersListByNameEmail(name);
		}
		if (searchUserRequest.getSearchBy().equals(searchUserRequest.getSearchBy().Location)) {
			String location = searchUserRequest.getLocation();
			userResponseList = userRepository.searchUsersListByLocation(location);
		}

		if (userResponseList.size() > 0) {
			for (User userResponse : userResponseList) {
				
				SearchUserResponse searchUserResponse = new SearchUserResponse();
				searchUserResponse.setUserId(userResponse.getUserId());
				searchUserResponse.setEmail(userResponse.getEmail());
				searchUserResponse.setFirstName(userResponse.getFirstName());
				searchUserResponse.setLastName(userResponse.getLastName());
				searchUserResponse.setGender(userResponse.getGender());
				searchUserResponse.setFacebookImageUrl(userResponse.getFacebookImageUrl());
				searchUserResponse.setImageUrl(userResponse.getImageUrl());
				searchUserResponse.setLocation(userResponse.getLocation());
				searchUserResponseList.add(searchUserResponse);

			}
		}
		return searchUserResponseList;
	}

	@Override
	public List<SearchUserResponse> getUserDetailsList() {
		List<User> userList = userRepository.findAll();
		List<SearchUserResponse> searchUserResponseList = new ArrayList<SearchUserResponse>();

		if (userList.size() > 0) {
			for (User userResponse : userList) {

				SearchUserResponse searchUserResponse = new SearchUserResponse();
				searchUserResponse.setUserId(userResponse.getUserId());
				searchUserResponse.setEmail(userResponse.getEmail());
				searchUserResponse.setFirstName(userResponse.getFirstName());
				searchUserResponse.setLastName(userResponse.getLastName());
				searchUserResponse.setGender(userResponse.getGender());
				searchUserResponse.setFacebookImageUrl(userResponse.getFacebookImageUrl());
				searchUserResponse.setImageUrl(userResponse.getImageUrl());
				searchUserResponse.setLocation(userResponse.getLocation());

				searchUserResponseList.add(searchUserResponse);

			}
		}
		return searchUserResponseList;
	}

	@Override
	public boolean isUserExist(String email) {
		if (userRepository.findByEmail(email) != null) {
			return true;
		}
		return false;
	}

	@Override
	public UserSkills saveUserSkills(UserSkills userSkills) {
		return userSkillRepository.save(userSkills);
	}

	@Override
	public List<Skill> getUserSkillsListByUserID(Long userId) {
		return userSkillRepository.getUserSkillsListByUserID(userId);
	}

	@Transactional(rollbackFor = Exception.class)
	private ImageUrl uploadPhoto(MultipartFile file, long profileId, String path) {
		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(new File(path));
			fileOutputStream.write(file.getBytes());
			fileOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		ImageUrl imageUrl = new ImageUrl();
		imageUrl.setImageUrl(path);
		System.out.println("path from uploadPhoto()"+path);
		imageUrl = imageUrlRepository.save(imageUrl);
		return imageUrl;
	}

	public ImageUrl loadImageUrl(Long imageUrlId) {
		return imageUrlRepository.findOne(imageUrlId);
	}

	@Transactional(rollbackFor = Exception.class)
	public Long uploadPostPhoto(MultipartFile file, long profileId, String path) {
		ImageUrl imageUrl = uploadPhoto(file, profileId, path);
		return imageUrl.getImageUrlId();
	}

	@Transactional(rollbackFor = Exception.class)
	public Long uploadUserPhoto(MultipartFile file, long profileId, String path) {
		ImageUrl imageUrl = uploadPhoto(file, profileId, path);
		if (imageUrl == null) {
			return null;
		}
		userRepository.updateImageUrlId(imageUrl.getImageUrlId(), profileId);
		return imageUrl.getImageUrlId();
	}

	@Override
	public List<Long> saveUserSkills(List<Long> skillsList, long userID) {
		List<UserSkills> userSkillsList = new ArrayList<UserSkills>();
		User user = new User();
		Skill skill = new Skill();

		if (skillsList.size() > 0) {
			for (int i = 0; i < skillsList.size(); i++) {
				UserSkills userSkill = new UserSkills();
				skill.setSkillId(skillsList.get(i));
				user.setUserId(userID);
				UserSkillsId uskID = new UserSkillsId();
				uskID.setSkillId(skillsList.get(i));
				uskID.setUserId(userID);
				userSkill.setId(uskID);
				userSkillsList.add(userSkill);
			}
		}
		userSkillRepository.deleteUserSkillsListByUserID(userID);
		userSkillRepository.save(userSkillsList);

		return skillsList;
	}

	@Override
	public List<SkillsResponse> getAllSkillsList() {
		List<SkillsResponse> skillsResponseList = new ArrayList<SkillsResponse>();
		List<Skill> skillsList = skillRepository.findAll();

		if (skillsList.size() > 0) {
			for (Skill skill : skillsList) {
				SkillsResponse skillsResponse = new SkillsResponse();
				skillsResponse.setSkillId(skill.getSkillId());
				skillsResponse.setSkillCode(skill.getSkillCode());
				skillsResponse.setSkillDsc(skill.getSkillDsc());
				skillsResponse.setSkillLevel(skill.getSkillLevel());
				skillsResponseList.add(skillsResponse);

			}
		}

		return skillsResponseList;
	}

	 /*file upload in base64 String  Start */
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Long saveUserProfileImage(byte[] file, long profileId, String path) {
		 
		int returnValue = 0;
		if(!file.equals(null)){
			returnValue=userRepository.updateUserProfileImage(path,profileId);
		}
		if(returnValue!=0){
			try {
				BufferedImage img = ImageIO.read(new ByteArrayInputStream(file));
		         ImageIO.write(img, "png", new File(path));
				//fileOutputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
 
		long result = returnValue;
		
		return result;

	}

	@Transactional(rollbackFor = Exception.class)
	private ImageUrl saveUserImage(byte[] file, long profileId, String path) throws InvalidDataAccessApiUsageException {

		try {
			BufferedImage img = ImageIO.read(new ByteArrayInputStream(file));
	         ImageIO.write(img, "png", new File(path));
			//fileOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		ImageUrl imageUrl = new ImageUrl();
		imageUrl.setImageUrl(path);
		imageUrl = imageUrlRepository.save(imageUrl);
		return imageUrl;
	}

	@Override
	public Long saveUserPostImage(byte[] file, long userID, String path) {
		System.out.println("path "+path+" userId "+userID+" file "+file);
		try {
			
			ByteArrayInputStream newFile = new ByteArrayInputStream(file); 			
			BufferedImage img = ImageIO.read(newFile);
			if(img != null){
				
				  ImageIO.write(img, "png", new File(path));
			}
			else{
	         System.out.println(" file "+img);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("rfsdfsdfsd");
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			System.out.println("5234532453245");
			e.printStackTrace();
			return null;
		}
		ImageUrl imageUrl = new ImageUrl();
		imageUrl.setImageUrl(path);
		System.out.println("path from saveUserPostImage()"+path);
		imageUrl = imageUrlRepository.save(imageUrl);
		return imageUrl.getImageUrlId();

	}
	
	
	 /*file upload in base64 String  END */
}
