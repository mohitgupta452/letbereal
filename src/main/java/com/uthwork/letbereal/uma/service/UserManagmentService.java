package com.uthwork.letbereal.uma.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.uthwork.letbereal.model.ImageUrl;
import com.uthwork.letbereal.model.Skill;
import com.uthwork.letbereal.model.User;
import com.uthwork.letbereal.model.UserSkills;
import com.uthwork.letbereal.uma.domain.request.SearchUserRequest;
import com.uthwork.letbereal.uma.domain.request.UserRequest;
import com.uthwork.letbereal.uma.domain.response.SearchUserResponse;
import com.uthwork.letbereal.uma.domain.response.SkillsResponse;
import com.uthwork.letbereal.uma.domain.response.UserDetail;

public interface UserManagmentService {

	public User saveUserDetails(UserRequest userRequest);

	public List<SearchUserResponse> getUserDetailsList();

	public UserDetail getUserDetailsByID(UserDetail userDetial);

	public UserDetail updateUserDetail(UserDetail userRequest);

	boolean isUserExist(String email);

	UserSkills saveUserSkills(UserSkills userSkills);

	public List<Skill> getUserSkillsListByUserID(Long userId);

	Long uploadUserPhoto(MultipartFile file, long profileId, String path);

	Long uploadPostPhoto(MultipartFile file, long profileId, String path);
 
	ImageUrl loadImageUrl(Long imageUrlId);

	List<SkillsResponse> getAllSkillsList();

	List<Long> saveUserSkills(List<Long> userSkills, long userID);

	public List<SearchUserResponse> searchUsersList(SearchUserRequest searchUserRequest);

	public Long saveUserProfileImage(byte[] file, long userID, String path);

	public Long saveUserPostImage(byte[] file, long userID, String path);

}
