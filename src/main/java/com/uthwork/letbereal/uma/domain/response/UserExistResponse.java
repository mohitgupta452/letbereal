package com.uthwork.letbereal.uma.domain.response;

public class UserExistResponse {

    boolean isUserExist;
    boolean isEmailValid;

    public boolean isUserExist() {
        return isUserExist;
    }

    public void setUserExist(boolean userExist) {
        isUserExist = userExist;
    }

    public boolean isEmailValid() {
        return isEmailValid;
    }

    public void setEmailValid(boolean emailValid) {
        isEmailValid = emailValid;
    }
}
