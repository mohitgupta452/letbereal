package com.uthwork.letbereal.uma.domain.response;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class UserResponse {
 
	boolean isEmailValid;
	boolean isFacebook_IdValid;
	boolean isNameValid;
	boolean isImageUrlValid;
	boolean isGenderValid;
	private long userId;
	public boolean isEmailValid() {
		return isEmailValid;
	}
	public void setEmailValid(boolean isEmailValid) {
		this.isEmailValid = isEmailValid;
	}
	public boolean isFacebook_IdValid() {
		return isFacebook_IdValid;
	}
	public void setFacebook_IdValid(boolean isFacebook_IdValid) {
		this.isFacebook_IdValid = isFacebook_IdValid;
	}
	public boolean isNameValid() {
		return isNameValid;
	}
	public void setNameValid(boolean isNameValid) {
		this.isNameValid = isNameValid;
	}
	public boolean isImageUrlValid() {
		return isImageUrlValid;
	}
	public void setImageUrlValid(boolean isImageUrlValid) {
		this.isImageUrlValid = isImageUrlValid;
	}
	public boolean isGenderValid() {
		return isGenderValid;
	}
	public void setGenderValid(boolean isGenderValid) {
		this.isGenderValid = isGenderValid;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
     
	    
}
