package com.uthwork.letbereal.uma.domain;

public enum UploadImageType {

    UserImage,PostImage,CompanyImage,CompanyPostImage,CompanyCoverPhoto;

}
