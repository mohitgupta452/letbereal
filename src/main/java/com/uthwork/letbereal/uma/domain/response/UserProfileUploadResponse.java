package com.uthwork.letbereal.uma.domain.response;

public class UserProfileUploadResponse {

    boolean isPhotoValid;

    boolean isImageTypeValid;

    long imageUrlId;

    public boolean isPhotoValid() {
        return isPhotoValid;
    }

    public void setPhotoValid(boolean photoValid) {
        isPhotoValid = photoValid;
    }

    public boolean isImageTypeValid() {
        return isImageTypeValid;
    }

    public void setImageTypeValid(boolean imageTypeValid) {
        isImageTypeValid = imageTypeValid;
    }

    public long getImageUrlId() {
        return imageUrlId;
    }

    public void setImageUrlId(long imageUrlId) {
        this.imageUrlId = imageUrlId;
    }
}
