package com.uthwork.letbereal.uma.domain.response;

public class UserImageUploadResponse {
	
	private String imageString;
	private long imageID;
	public String getImageString() {
		return imageString;
	}
	public void setImageString(String imageString) {
		this.imageString = imageString;
	}
	public long getImageID() {
		return imageID;
	}
	public void setImageID(long imageID) {
		this.imageID = imageID;
	}

}
