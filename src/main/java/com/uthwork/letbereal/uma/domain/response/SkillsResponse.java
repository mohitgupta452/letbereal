package com.uthwork.letbereal.uma.domain.response;

public class SkillsResponse {
	private Long skillId;
	private String skillLevel;
	private String skillCode;
	private String skillDsc;
	public Long getSkillId() {
		return skillId;
	}
	public void setSkillId(Long skillId) {
		this.skillId = skillId;
	}
	public String getSkillLevel() {
		return skillLevel;
	}
	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}
	public String getSkillCode() {
		return skillCode;
	}
	public void setSkillCode(String skillCode) {
		this.skillCode = skillCode;
	}
	public String getSkillDsc() {
		return skillDsc;
	}
	public void setSkillDsc(String skillDsc) {
		this.skillDsc = skillDsc;
	}
	
}
