package com.uthwork.letbereal.uma.domain.request;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class UserRequest {

@NotBlank
private String facebook_id;
@NotBlank
private String name;
@NotBlank
private String imageUrl;
@NotBlank
@Email
private String email;
@NotBlank
private String gender;

public String getFacebook_id() {
	return facebook_id;
}
public void setFacebook_id(String facebook_id) {
	this.facebook_id = facebook_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getImageUrl() {
	return imageUrl;
}
public void setImageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
}
 
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}

}
