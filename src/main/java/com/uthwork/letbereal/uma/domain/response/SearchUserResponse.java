package com.uthwork.letbereal.uma.domain.response;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class SearchUserResponse {
 
	private Long userId;
	private String firstName;
	private String lastName;
	private String gender;
	private String email;
	private String facebookImageUrl;
	private String imageUrl;
	private String location;
	private String imageUrlID;

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}	
	public String getFacebookImageUrl() {
		return facebookImageUrl;
	}
	public void setFacebookImageUrl(String facebookImageUrl) {
		this.facebookImageUrl = facebookImageUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
	public String getImageUrlID() {
		return imageUrlID;
	}
	public void setImageUrlID(String imageUrlID) {
		this.imageUrlID = imageUrlID;
	}
	
}
