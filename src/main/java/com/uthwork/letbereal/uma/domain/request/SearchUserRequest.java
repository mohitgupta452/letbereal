package com.uthwork.letbereal.uma.domain.request;

import java.util.List;

public class SearchUserRequest {

private String name;
private String email;
private String location;
private List<Long> skillsList;
private UserSearchTypes searchBy;

public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}

public String getLocation() {
	return location;
}
public void setLocation(String location) {
	this.location = location;
}

public List<Long> getSkillsList() {
	return skillsList;
}
public void setSkillsList(List<Long> skillsList) {
	this.skillsList = skillsList;
}
public UserSearchTypes getSearchBy() {
	return searchBy;
}
public void setSearchBy(UserSearchTypes searchBy) {
	this.searchBy = searchBy;
}

}
