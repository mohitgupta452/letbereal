package com.uthwork.letbereal.uma.domain.request;

import com.uthwork.letbereal.uma.domain.UploadImageType;

public class UserImageUploadRequest {
private String imageString;
private long imageID;
private UploadImageType imageType;
private String companyName;
public String getImageString() {
	return imageString;
}
public void setImageString(String imageString) {
	this.imageString = imageString;
}
public long getImageID() {
	return imageID;
}
public void setImageID(long imageID) {
	this.imageID = imageID;
}
public UploadImageType getImageType() {
	return imageType;
}
public void setImageType(UploadImageType imageType) {
	this.imageType = imageType;
}
public String getCompanyName() {
	return companyName;
}
public void setCompanyName(String companyName) {
	this.companyName = companyName;
}

}
