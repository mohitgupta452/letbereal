package com.uthwork.letbereal.uma.domain.response;

import java.util.List;

import com.uthwork.letbereal.uma.domain.response.SkillsResponse;

public class UserDetail {
	private Long userId;
	private String firstName;
	private String lastName;
	private String gender;
	private String email;
	private String telephone;
	private String facebookId;
	private String googlePlusId;
	private String facebookImageUrl;
	private String imageUrl;
	private String location;
	private String profileVisibility;
	private String publIcKey;
	private String privateKey;
	private Integer field1;
	private Long imageUrlID;
	private String field3;
	private List<SkillsResponse> skills;
	private Float avgRating;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getGooglePlusId() {
		return googlePlusId;
	}

	public void setGooglePlusId(String googlePlusId) {
		this.googlePlusId = googlePlusId;
	}

	public String getFacebookImageUrl() {
		return facebookImageUrl;
	}

	public void setFacebookImageUrl(String facebookImageUrl) {
		this.facebookImageUrl = facebookImageUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getProfileVisibility() {
		return profileVisibility;
	}

	public void setProfileVisibility(String profileVisibility) {
		this.profileVisibility = profileVisibility;
	}

	public String getPublIcKey() {
		return publIcKey;
	}

	public void setPublIcKey(String publIcKey) {
		this.publIcKey = publIcKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public Integer getField1() {
		return field1;
	}

	public void setField1(Integer field1) {
		this.field1 = field1;
	}

	public Long getImageUrlID() {
		return imageUrlID;
	}

	public void setImageUrlID(Long imageUrlID) {
		this.imageUrlID = imageUrlID;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public List<SkillsResponse> getSkills() {
		return skills;
	}

	public void setSkills(List<SkillsResponse> skills) {
		this.skills = skills;
	}

	public Float getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Float avgRating) {
		this.avgRating = avgRating;
	}
}
