
<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Admin home</title>
<!-- <link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet"> -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

			<div align="center">
			<h1>Admin Panel</h1>
        <table border="1" cellpadding="5">
            <caption><h2>List of users</h2></caption>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Gender</th>
				<th>Email</th>
				<th>Telephone</th>
				<th>FacebookId</th>
				<th>GooglePlusId</th>
				<th>View Post</th>
			</tr>
            <c:forEach var="user" items="${users}">
                <tr>
                    <td><c:out value="${user.firstName}" /></td>
                    <td><c:out value="${user.lastName}" /></td>
                    <td><c:out value="${user.gender}" /></td>
                    <td><c:out value="${user.email}" /></td>
                    <td><c:out value="${user.telephone}" /></td>
                    <td><c:out value="${user.facebookId}" /></td>
                    <td><c:out value="${user.googlePlusId}" /></td>
                    <%-- <td><a href="admin/getAllPost/${user.userId}">view post</a></td> --%>
                    <td><a href="<c:url value="getAllPost/${user.userId}" />">view post</a></td> 
                </tr>
            </c:forEach>
        </table>
    </div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<!-- <script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script> -->
</body>
</html>