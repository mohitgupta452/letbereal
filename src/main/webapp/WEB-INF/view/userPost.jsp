
<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>user post</title>
</head>
<body>

			<div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of Post</h2></caption>
            <tr>
                <th>Post Id</th>
                <th>User Id</th>
                <th>Image Url</th>
				<th>Title</th>
				<th>Description</th>
				<th>Created Time</th>
				<th>Update Time</th>
				<th>View Comment</th>
			</tr>
            <c:forEach var="listOfPost" items="${listOfPost}">
                <tr>
                    <td><c:out value="${listOfPost.postId}" /></td>
                    <td><c:out value="${listOfPost.user.userId}" /></td>
                    <td><c:out value="${listOfPost.imageUrl}" /></td>
                    <td><c:out value="${listOfPost.title}" /></td>
                    <td><c:out value="${listOfPost.description}" /></td>
                    <td><c:out value="${listOfPost.createdTime}" /></td>
                    <td><c:out value="${listOfPost.updateTime}" /></td>
                    <td><a href="<c:url value="getAllComment/${listOfPost.postId}" />">view comments</a></td> 
                </tr>
            </c:forEach>
        </table>
    </div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<!-- <script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script> -->
</body>
</html>