
CREATE DATABASE IF NOT EXISTS `letbereal`  DEFAULT CHARACTER SET utf8 ;

USE `letbereal`;

DROP TABLE IF EXISTS `image_url`;

CREATE TABLE `image_url` (
  `IMAGE_URL_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `IMAGE_URL` varchar(50) DEFAULT NULL,
  `FIELD1` varchar(256) DEFAULT NULL,
  `FIELD2` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`IMAGE_URL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `POST_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL,
  `IMAGE_URL` varchar(126) DEFAULT NULL,
  `TITLE` varchar(256) DEFAULT NULL,
  `DESCRIPTION` varchar(512) DEFAULT NULL,
  `LOCATION` varchar(126) DEFAULT NULL,
  `lat_lang` varchar(256) DEFAULT NULL,
  `POST_VISIBILITY` varchar(50) DEFAULT NULL,
  `CREATED_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIELD1` int(10) DEFAULT NULL,
  `FIELD2` varchar(128) DEFAULT NULL,
  `FIELD3` varchar(256) DEFAULT NULL,
  `IMAGE_URL_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`POST_ID`),
  FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `post_comment`;

CREATE TABLE `post_comment` (
  `POST_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  `COMMENT` varchar(512) DEFAULT NULL,
  `COMMENT_VISIBILITY` varchar(50) DEFAULT NULL,
  `COMMENT_TYPE` varchar(50) DEFAULT NULL,
  `CREATED_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIELD1` int(10) DEFAULT NULL,
  `FIELD2` varchar(128) DEFAULT NULL,
  `FIELD3` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`POST_ID`,`USER_ID`),
  KEY (`USER_ID`),
  FOREIGN KEY (`POST_ID`) REFERENCES `post` (`POST_ID`),
  FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `post_rating`;

CREATE TABLE `post_rating` (
  `POST_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  `RATING` int(10) DEFAULT NULL,
  `CREATED_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIELD1` int(10) DEFAULT NULL,
  `FIELD2` varchar(128) DEFAULT NULL,
  `FIELD3` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`POST_ID`,`USER_ID`),
  FOREIGN KEY (`POST_ID`) REFERENCES `post` (`POST_ID`),
  FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `profile_comment`;

CREATE TABLE `profile_comment` (
  `PROFILE_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  `COMMENT` varchar(512) DEFAULT NULL,
  `COMMENT_VISIBILITY` varchar(50) DEFAULT NULL,
  `COMMENT_TYPE` varchar(50) DEFAULT NULL,
  `CREATED_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIELD1` int(10) DEFAULT NULL,
  `FIELD2` varchar(128) DEFAULT NULL,
  `FIELD3` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`PROFILE_ID`,`USER_ID`),
  FOREIGN KEY (`PROFILE_ID`) REFERENCES `user` (`USER_ID`),
  FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `profile_rating`;

CREATE TABLE `profile_rating` (
  `PROFILE_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  `RATING` int(10) DEFAULT NULL,
  `CREATED_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIELD1` int(10) DEFAULT NULL,
  `FIELD2` varchar(128) DEFAULT NULL,
  `FIELD3` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`PROFILE_ID`,`USER_ID`),
  CONSTRAINT `profile_rating_ibfk_1` FOREIGN KEY (`PROFILE_ID`) REFERENCES `user` (`USER_ID`),
  CONSTRAINT `profile_rating_ibfk_2` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `skill`;

CREATE TABLE `skill` (
  `SKILL_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SKILL_LEVEL` varchar(50) DEFAULT NULL,
  `SKILL_CODE` varchar(50) DEFAULT NULL,
  `SKILL_DSC` varchar(256) DEFAULT NULL,
  `SKILL_SELECT` varchar(126) DEFAULT NULL,
  `FIELD1` varchar(256) DEFAULT NULL,
  `FIELD2` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`SKILL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `USER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(126) DEFAULT NULL,
  `LAST_NAME` varchar(126) DEFAULT NULL,
  `GENDER` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(126) NOT NULL,
  `TELEPHONE` varchar(12) DEFAULT NULL,
  `FACEBOOK_ID` varchar(100) DEFAULT NULL,
  `GOOGLE_PLUS_ID` varchar(20) DEFAULT NULL,
  `FACEBOOK_IMAGE_URL` varchar(126) DEFAULT NULL,
  `IMAGE_URL` varchar(512) DEFAULT NULL,
  `LOCATION` varchar(126) DEFAULT NULL,
  `PROFILE_VISIBILITY` varchar(50) DEFAULT NULL,
  `PUBlIC_KEY` varchar(128) DEFAULT NULL,
  `PRIVATE_KEY` varchar(128) DEFAULT NULL,
  `FIELD1` int(10) DEFAULT NULL,
  `FIELD2` varchar(128) DEFAULT NULL,
  `FIELD3` varchar(256) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `IMAGE_URL_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_followers`;

CREATE TABLE `user_followers` (
  `FOLLOWERS_USER_ID` bigint(20) NOT NULL,
  `FOLLOWING_USER_ID` bigint(20) NOT NULL,
  `CREATED_TIME` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIELD1` int(10) DEFAULT NULL,
  `FIELD2` varchar(128) DEFAULT NULL,
  `FIELD3` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`FOLLOWERS_USER_ID`,`FOLLOWING_USER_ID`),
  KEY `FOLLOWING_USER_ID` (`FOLLOWING_USER_ID`),
  CONSTRAINT `user_followers_ibfk_1` FOREIGN KEY (`FOLLOWERS_USER_ID`) REFERENCES `user` (`USER_ID`),
  CONSTRAINT `user_followers_ibfk_2` FOREIGN KEY (`FOLLOWING_USER_ID`) REFERENCES `user` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_skills`;

CREATE TABLE `user_skills` (
  `SKILL_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `FIELD1` varchar(256) DEFAULT NULL,
  `FIELD2` varchar(512) DEFAULT NULL,
  KEY `user_skills_ibfk_1` (`SKILL_ID`),
  FOREIGN KEY (`SKILL_ID`) REFERENCES `skill` (`SKILL_ID`),
  FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE bad_comment(
    ID bigint NOT NULL AUTO_INCREMENT,
    COMMENT varchar(255) NOT NULL,
    PRIMARY KEY (ID)
);
